﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.Enuns
{
    public enum Sexo
    {

        Masculino = 1,
        Femenino = 2
    }

    public enum Regiao
    {
        [Description("01ª Região - DF")]
        Regiao_DF = 1,

        [Description("02ª Região - PE")]
        Regiao_PE = 2,

        [Description("03ª Região - BA")]
        Regiao_BA = 3,

        [Description("04ª Região - MG")]
        Regiao_MG = 4,

        [Description("05ª Região - RJ")]
        Regiao_RJ = 5,

        [Description("06ª Região - SP")]
        Regiao_SP = 6,

        [Description("07ª Região - RS")]
        Regiao_RS = 7,

        [Description("08ª Região - PR")]
        Regiao_PR = 8,

        [Description("09ª Região - GO/TO")]
        Regiao_GO_TO = 9,

        [Description("10ª Região - PA/AP")]
        Regiao_AP = 10,

        [Description("11ª Região - CE/PI/MA")]
        Regiao_CE_PI_MA = 11,

        [Description("12ª Região - SC")]
        Regiao_SC = 12,

        [Description("13ª Região - PB")]
        Regiao_PB = 13,

        [Description("14ª Região - MS")]
        Regiao_MS = 14,

        [Description("15ª Região - AL")]
        Regiao_AL = 15,

        [Description("16ª Região - ES")]
        Regiao_ES = 16,

        [Description("17ª Região - RN")]
        Regiao_RN = 17,

        [Description("18ª Região - MT")]
        Regiao_MT = 18,

        [Description("19ª Região - SE")]
        Regiao_SE = 19,

        [Description("20ª Região - AM/AC/RO/RR")]
        Regiao_RR = 20,

        [Description("21ª Região - PI")]
        Regiao_PI = 21,

        [Description("22ª Região - MA")]
        Regiao_MA = 22,

        [Description("23ª Região - TO")]
        Regiao_TO = 23,
    }

    public enum HorarioConsulta
    {

    }

    public enum HorarioTerapia
    {

    }

    public enum Estado
    {
        AC = 1,
        AL = 2,
        AM = 3,
        AP = 4,
        BA = 5,
        CE = 6,
        DF = 7,
        ES = 8,
        GO = 9,
        MA = 10,
        MG = 11,
        MS = 12,
        MT = 13,
        PA = 14,
        PB = 15,
        PE = 16,
        PI = 17,
        PR = 18,
        RJ = 19,
        RN = 20,
        RO = 21,
        RR = 22,
        RS = 23,
        SC = 24,
        SE = 25,
        SP = 26,
        TO = 27
    }

    public enum TipoUsuario
    {
        Administrador = 1,
        Psicologo = 2,
        Atendente = 3
    }

}
