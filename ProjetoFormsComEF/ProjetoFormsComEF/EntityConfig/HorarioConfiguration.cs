﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class HorarioConfiguration : EntityTypeConfiguration<Horario>
    {
        public HorarioConfiguration()
        {
            HasKey(t => t.HorarioId);

            Property(t => t.Descricao)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}
