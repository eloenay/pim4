﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class SalaConfiguration : EntityTypeConfiguration<Sala>
    {
        public SalaConfiguration()
        {
            HasKey(s => s.SalaId);

            Property(s => s.Descricao)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);
        }
    }
}
