﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class TipoTerapiaConfiguration : EntityTypeConfiguration<TipoTerapia>
    {
        public TipoTerapiaConfiguration()
        {
            HasKey(t => t.TipoTerapiaId);

            Property(t => t.Descricao)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}
