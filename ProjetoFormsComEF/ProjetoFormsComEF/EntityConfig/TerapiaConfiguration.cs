﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class TerapiaConfiguration : EntityTypeConfiguration<Terapia>
    {
        public TerapiaConfiguration()
        {
            HasKey(t => t.TerapiaId);
        }
    }
}
