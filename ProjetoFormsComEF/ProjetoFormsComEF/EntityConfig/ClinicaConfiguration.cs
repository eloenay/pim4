﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class ClinicaConfiguration:EntityTypeConfiguration<Clinica>
    {
        public ClinicaConfiguration()
        {
            HasKey(c => c.ClinicaId);

            Property(c => c.Nome)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(c => c.Endereco)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(c => c.Numero)
                .IsRequired();

            Property(c => c.Bairro)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(c => c.Cep)
                .IsRequired()
                .HasMaxLength(9)
                .IsUnicode(false);

            Property(c => c.Estado)
                .IsRequired();

            Property(c => c.Cidade)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            //HasMany(s => s.Salas)
            //    .WithRequired(e => e.Clinicas)
            //    .WillCascadeOnDelete(false);
        }
    }
}
