﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
    public class ConsultaConfiguration : EntityTypeConfiguration<Consulta>
    {
        public ConsultaConfiguration()
        {
            HasKey(c => c.ConsultaId);

            Property(p => p.Valor)
                .IsRequired();

            Property(p => p.HorarioConsulta)
                .IsRequired();

            Property(p => p.DataConsulta)
                .IsRequired();
        }
    }
}
