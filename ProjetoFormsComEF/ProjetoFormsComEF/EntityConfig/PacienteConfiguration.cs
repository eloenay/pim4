﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class PacienteConfiguration : EntityTypeConfiguration<Paciente>
    {
        public PacienteConfiguration()
        {
            HasKey(p => p.PacienteId);

            Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(p => p.DataNascimento)
                .HasColumnType("date")
                .IsRequired();

            Property(p => p.Sexo)
                .IsRequired();

            Property(p => p.Email)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);
        }
    }
}
