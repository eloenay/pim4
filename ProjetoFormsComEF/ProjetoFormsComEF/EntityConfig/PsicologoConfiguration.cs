﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
    class PsicologoConfiguration : EntityTypeConfiguration<Psicologo>
    {
        public PsicologoConfiguration()
        {
            HasKey(p => p.PsicologoId);

            Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(p => p.Cpf)
                .IsRequired()
                .HasMaxLength(14)
                .IsUnicode(false);

            Property(p => p.DataNascimento)
                .IsRequired();

            Property(p => p.NumeroCrp)
                .IsRequired();

            Property(p => p.RegiaoCrp)
                .IsRequired();
        }
    }
}
