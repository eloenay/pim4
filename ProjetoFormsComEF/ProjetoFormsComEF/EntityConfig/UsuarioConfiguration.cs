﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.EntityConfig
{
   public class UsuarioConfiguration : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfiguration()
        {
            HasKey(u => u.UsuarioId);

            Property(u => u.Nome)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            Property(u => u.Login)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            Property(u => u.Senha)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            Property(u => u.ConfirmarSenha)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            Property(u => u.TipoUsuario)
                .IsRequired();
        }
    }
}
