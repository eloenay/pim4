﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.DAO
{
   public class DaoTipoTerapia
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoTipoTerapia()
        {
            _conexaoBd = new ConexaoBd();
        }

        public IEnumerable<TipoTerapia> BuscarTodos()
        {
            return _conexaoBd.TipoTerapias;
        }

        public TipoTerapia BuscarPorId(int? id)
        {
            return _conexaoBd.TipoTerapias.First(x => x.TipoTerapiaId == id);
        }

        public void Save(TipoTerapia tipoTerapia)
        {
            if (tipoTerapia.TipoTerapiaId == 0)
            {
                _conexaoBd.TipoTerapias.Add(tipoTerapia);
            }
            else
            {
                _conexaoBd.Entry(tipoTerapia).State = EntityState.Modified;
            }

            _conexaoBd.SaveChanges();
        }
    }
}
