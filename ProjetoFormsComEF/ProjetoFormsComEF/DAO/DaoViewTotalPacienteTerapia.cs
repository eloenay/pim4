﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.DAO
{
    public class DaoViewTotalPacienteTerapia
    {
        private readonly ViewDB _viewBd;

        public DaoViewTotalPacienteTerapia()
        {
            _viewBd = new ViewDB();
        }

        public IEnumerable<ViewTotalPacienteTerapia> Buscartodos()
        {
            return _viewBd.ViewTotalPacienteTerapia;
        }
    }
}
