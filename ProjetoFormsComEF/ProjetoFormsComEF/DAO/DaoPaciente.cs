﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.DAO
{
   public class DaoPaciente
   {
       private readonly ConexaoBd _conexaoBd;

       public DaoPaciente()
       {
           _conexaoBd = new ConexaoBd();
       }

       public IEnumerable<Paciente> BuscarTodos()
       {
           return _conexaoBd.Pacientes;
       }

       public Paciente BuscarPorId(int? id)
       {
           return _conexaoBd.Pacientes.First(x => x.PacienteId == id);
       }

        public ICollection<Paciente> BuscarPorNome(string nome)
        {
            return _conexaoBd.Pacientes.Where(p => p.Nome.Contains(nome)).ToList();
        }

        public void Save(Paciente paciente)
       {
           if (paciente.PacienteId == 0)
           {
               _conexaoBd.Pacientes.Add(paciente);
           }
           else
           {
               _conexaoBd.Entry(paciente).State = EntityState.Modified;
           }
           _conexaoBd.SaveChanges();
       }

       public void Deletar(Paciente id)
       {
           _conexaoBd.Entry(id).State = EntityState.Deleted;
           _conexaoBd.SaveChanges();
        }
    }
}
