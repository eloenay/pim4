﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.DAO
{
   public class DaoUsuario
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoUsuario()
        {
            _conexaoBd = new ConexaoBd();
        }

        public IEnumerable<Usuario> BuscarPorNome(string nome)
        {
            return _conexaoBd.Usuarios.Where(u => u.Nome.Contains(nome));
        }

        public Usuario BuscarPorId(int? id)
        {
            return _conexaoBd.Usuarios.First(x => x.UsuarioId == id);
        }

        public Usuario BuscarLoginSenha(Usuario registrado)
        {
            return _conexaoBd.Usuarios.FirstOrDefault(u => u.Login == registrado.Login && u.Senha == registrado.Senha);
        }

        public Usuario BuscarLoginExistente(Usuario registrado)
        {
            return _conexaoBd.Usuarios.FirstOrDefault(u => u.Login == registrado.Login);
        }

        public void Save(Usuario usuario)
        {
            if (usuario.UsuarioId == 0)
            {
                _conexaoBd.Usuarios.Add(usuario);
            }
            else
            {
                _conexaoBd.Entry(usuario).State = EntityState.Modified;
            }

            _conexaoBd.SaveChanges();
        }

        public void Deletar(Usuario id)
        {
            _conexaoBd.Entry(id).State = EntityState.Deleted;
            _conexaoBd.SaveChanges();
        }
    }
}
