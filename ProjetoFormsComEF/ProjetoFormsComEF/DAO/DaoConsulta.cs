﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.DAO
{
    public class DaoConsulta
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoConsulta()
        {
            _conexaoBd = new ConexaoBd();
        }

        public IEnumerable<Consulta> Buscartodos()
        {
            return _conexaoBd.Consultas
                .Include(c => c.Paciente)
                .Include(c => c.Psicologo);
        }

        public int ConsultaParaHoje()
        {
            return _conexaoBd.Consultas
                .Count(c => c.DataConsulta.Day == DateTime.Now.Day);
        }

        public IEnumerable<Consulta> BuscarConsultaParaHoje()
        {
            return _conexaoBd.Consultas
                .Where(c => c.DataConsulta.Day == DateTime.Now.Day)
                .Include(c => c.Paciente)
                .Include(c => c.Psicologo);
        }

        public IEnumerable<Consulta> BuscarConsultaParaMes()
        {
            return _conexaoBd.Consultas
                .Where(c => c.DataConsulta.Month == DateTime.Now.Month)
                .Include(c => c.Paciente)
                .Include(c => c.Psicologo);
        }

        public int ConsultaParaMes()
        {
            return _conexaoBd.Consultas
                .Count(c => c.DataConsulta.Month == DateTime.Now.Month);
        }

        public int BuscarPacientesComMaisConsulta()
        {

            return _conexaoBd.Consultas
                    .Include(c => c.Paciente)
                    .Count(c => c.Paciente.PacienteId == c.PacienteId);
                //.Include(c => c.Paciente)
                //.Where(c => c.Paciente.PacienteId == c.PacienteId)
                //.OrderByDescending(c => c.PacienteId).Take(10);

            //var query = from c in _conexaoBd.Consultas
            //    join p in _conexaoBd.Pacientes on c.PacienteId equals p.PacienteId
            //    where p.PacienteId == c.PacienteId
            //    orderby c.PacienteId
            //    group p by p.Nome;

            //select p.Nome, count(c.PacienteId) as 'nº Consultas'
            //from Consultas c join Pacientes p
            //on p.PacienteId = c.PacienteId
            //where p.PacienteId = c.PacienteId
            //group by p.Nome
            //    Order by 'nº Consultas' desc
        }

        public IEnumerable<Consulta> BuscarPorPaciente(string nome)
        {
            return _conexaoBd.Consultas
                .Include(c => c.Paciente)
                .Include(c => c.Psicologo)
                .Where(p => p.Paciente.Nome.Contains(nome));
        }

        public IEnumerable<Consulta> BuscarPorPsicologo(string nome)
        {
            return _conexaoBd.Consultas
                .Include(c => c.Paciente)
                .Include(c => c.Psicologo)
                .Where(p => p.Psicologo.Nome.Contains(nome));
        }

        public Consulta BuscarPorId(int? id)
        {
            return _conexaoBd.Consultas
                .Include(c => c.Paciente)
                .Include(c => c.Psicologo)
                .First(x => x.ConsultaId == id);
        }

        public void Salvar(Consulta consulta)
        {
            _conexaoBd.Psicologos.Attach(consulta.Psicologo);
            _conexaoBd.Pacientes.Attach(consulta.Paciente);
            _conexaoBd.Consultas.Add(consulta);

            _conexaoBd.SaveChanges();
        }

        public void Editar(Consulta consulta)
        {
            _conexaoBd.Entry(consulta).State = EntityState.Modified;
            _conexaoBd.SaveChanges();
        }
    }
}
