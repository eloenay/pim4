﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.DAO
{
   public class DaoSala
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoSala()
        {
            _conexaoBd = new ConexaoBd();
        }

        public IEnumerable<Sala> BuscarTodos()
        {
            return _conexaoBd.Salas;
        }

        public Sala BuscarPorId(int? id)
        {
            return _conexaoBd.Salas.First(x => x.SalaId == id);
        }

        public Sala VerificarSala(Sala sala)
        {
            return _conexaoBd.Salas.FirstOrDefault(x => x.Descricao == sala.Descricao);
        }

        public void Save(Sala sala)
        {
            if (sala.SalaId == 0)
            {
                _conexaoBd.Salas.Add(sala);
            }
            else
            {
                _conexaoBd.Entry(sala).State = EntityState.Modified;
            }

            _conexaoBd.SaveChanges();
        }
    }
}
