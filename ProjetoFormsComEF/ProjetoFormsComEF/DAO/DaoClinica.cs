﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.DAO
{
   public class DaoClinica
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoClinica()
        {
            _conexaoBd = new ConexaoBd();
        }

        public IEnumerable<Clinica> BuscarTodos()
        {
            IEnumerable<Clinica> cl = new List<Clinica>();
            return _conexaoBd.Clinicas.OrderBy(c => c.Nome == cl.ToString());
        }

        public IEnumerable<Clinica> BuscarPorNome(string nome)
        {
            return _conexaoBd.Clinicas.Where(c => c.Nome.Contains(nome));
        }

        public Clinica BuscarPorId(int? id)
        {
            return _conexaoBd.Clinicas.First(x => x.ClinicaId == id);
        }

        public void Save(Clinica consultorio)
        {
            if (consultorio.ClinicaId == 0)
            {
                _conexaoBd.Clinicas.Add(consultorio);
            }
            else
            {
                _conexaoBd.Entry(consultorio).State = EntityState.Modified;
            }
            _conexaoBd.SaveChanges();
        }

        public void Deletar(Clinica clinica)
        {
            _conexaoBd.Entry(clinica).State = EntityState.Deleted;
            _conexaoBd.SaveChanges();
        }
    }
}
