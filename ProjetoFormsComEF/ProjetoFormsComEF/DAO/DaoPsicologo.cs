﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.DAO
{
   public class DaoPsicologo
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoPsicologo()
        {
            _conexaoBd = new ConexaoBd();
        }

        public Psicologo BuscarPorId(int? id)
        {
            return _conexaoBd.Psicologos
                .Include(x => x.Sala)
                .First(x => x.PsicologoId == id);
        }

        public Psicologo VerificarCpf(Psicologo psicologo)
        {
            return _conexaoBd.Psicologos.FirstOrDefault(x => x.Cpf == psicologo.Cpf);
        }

        public IEnumerable<Psicologo> BuscarSala(Sala sala)
        {
            return _conexaoBd.Psicologos.Where(p => p.SalaId == sala.SalaId);
        }

        public IEnumerable<Psicologo> BuscarPorNome(string nome)
        {
            return _conexaoBd.Psicologos
                .Include(p => p.Sala)
                .Where(p => p.Nome.Contains(nome));
        }

        public void Save(Psicologo psicologo)
        {
            if (psicologo.PsicologoId == 0)
            {
                _conexaoBd.Psicologos.Add(psicologo);
            }
            else
            {
                _conexaoBd.Entry(psicologo).State = EntityState.Modified;
            }

            _conexaoBd.SaveChanges();
        }

        public void Deletar(Psicologo psicologo)
        {
            _conexaoBd.Entry(psicologo).State = EntityState.Deleted;
            _conexaoBd.SaveChanges();
        }
    }
}
