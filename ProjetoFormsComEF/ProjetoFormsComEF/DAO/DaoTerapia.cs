﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.DAO
{
    public class DaoTerapia
    {
        private readonly ConexaoBd _conexaoBd;

        public DaoTerapia()
        {
            _conexaoBd = new ConexaoBd();
        }

        public IEnumerable<Terapia> BuscarTodos(string nome)
        {
            return _conexaoBd.Terapias
                .Include(t => t.Consulta.Paciente)
                .Include(t => t.TipoTerapia)
                .Include(t => t.Horario)
                .Where(t => t.Consulta.Paciente.Nome.Contains(nome))
                .ToList()
                .OrderBy(t => t.DataTerapia);
        }

        public IEnumerable<Terapia> BuscarPorPaciente(int consulta)
        {
            return _conexaoBd.Terapias
                .Include(t => t.Consulta.Paciente)
                .Include(t => t.TipoTerapia)
                .Include(t => t.Horario)
                .Where(t => t.ConsultaId == consulta);
        }

        public Terapia BuscarPorId(int? id)
        {
            return _conexaoBd.Terapias
                .Include(t => t.Consulta.Paciente)
                .Include(t => t.Consulta.Psicologo)
                .Include(t => t.TipoTerapia)
                .Include(t => t.Horario)
                .First(x => x.TerapiaId == id);
        }

        public void Salvar(Terapia terapia)
        {
            _conexaoBd.Consultas.Attach(terapia.Consulta);
            _conexaoBd.Terapias.Add(terapia);
            _conexaoBd.SaveChanges();
        }

        public void Editar(Terapia terapia)
        {
            _conexaoBd.Entry(terapia).State = EntityState.Modified;
            _conexaoBd.SaveChanges();
        }
    }
}
