﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.DAO
{
    public class DaoViewTotalTipoTerapia
    {
        private ViewDB _viewDB;

        public DaoViewTotalTipoTerapia()
        {
            _viewDB = new ViewDB();
        }

        public IEnumerable<ViewTotalTipoTerapia> Buscartodos()
        {
            return _viewDB.ViewTotalTipoTerapia;
        }
    }
}
