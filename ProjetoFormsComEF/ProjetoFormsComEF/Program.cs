﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios;
using ProjetoIntegrador4.Formularios.FormsAdicionais;
using ProjetoIntegrador4.Formularios.Login;

namespace ProjetoFormsComEF
{
    static class Program
    {

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new FormPrincipal(new Usuario()));
            Application.Run(new FormSplashEntrada());
        }
    }
}
