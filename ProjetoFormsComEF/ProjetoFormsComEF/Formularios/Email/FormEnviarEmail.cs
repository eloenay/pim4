﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using S22.Imap;


namespace ProjetoIntegrador4.Formularios.Email
{
    public partial class FormEnviarEmail : Form
    {
        public FormEnviarEmail()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                MailMessage message = new MailMessage();

                message.From = new MailAddress(txtDe.Text);
                message.To.Add(txtPara.Text);
                message.Body = txtEmail.Text;
                message.Subject = txtAssunto.Text;
                client.UseDefaultCredentials = false;
                client.EnableSsl = true;

                client.Credentials = new System.Net.NetworkCredential(txtDe.Text, txtSenha.Text);
                client.Send(message);
                message = null;

                MessageBox.Show("Email enviado com sucesso!");

            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }
        }
    }
}
