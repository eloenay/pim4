﻿using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.FormsAdicionais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoIntegrador4.Formularios
{
    public partial class FormTelaInicial : Form
    {
        public FormTelaInicial()
        {
            InitializeComponent();
            ConsultasHoje();
            ConsultasMes();
            TotalTerapiaPaciente();
            TotalTipoTerapia();
        }

        public void ConsultasHoje()
        {
            Consulta consulta = new Consulta();
            btnConsultaHoje.Text = Convert.ToString(consulta.ConsultaParaHoje());
        }

        public void ConsultasMes()
        {
            Consulta consulta = new Consulta();
            btnConsultaMes.Text = Convert.ToString(consulta.ConsultaParaMes());
        }

        public void TotalTerapiaPaciente()
        {
            ViewTotalPacienteTerapia viewTotalPacienteTerapia = new ViewTotalPacienteTerapia();
            dgvTopDezPacientes.DataSource = viewTotalPacienteTerapia.BuscarTodos();
        }

        public void TotalTipoTerapia()
        {
            ViewTotalTipoTerapia viewTotalTipoTerapia = new ViewTotalTipoTerapia();
            dgvTopDezConsultas.DataSource = viewTotalTipoTerapia.BuscarTodos();
        }

        private void btnConsultaHoje_Click(object sender, EventArgs e)
        {
            FormConsultasParaHoje formConsultasParaHoje = new FormConsultasParaHoje();
            formConsultasParaHoje.Show();
        }

        private void btnConsultaMes_Click(object sender, EventArgs e)
        {
            FormConsultaParaMes formConsultaParaMes = new FormConsultaParaMes();
            formConsultaParaMes.Show();
        }
    }
}
