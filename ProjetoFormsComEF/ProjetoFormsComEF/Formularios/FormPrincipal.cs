﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;
using ProjetoIntegrador4.Formularios.Terapias;
using ProjetoIntegrador4.Formularios.Clinicas;
using ProjetoIntegrador4.Formularios.Exame;
using ProjetoIntegrador4.Formularios.Login;
using ProjetoIntegrador4.Formularios.Pacientes;
using ProjetoIntegrador4.Formularios.Psicologos;
using ProjetoIntegrador4.Formularios.Email;

namespace ProjetoIntegrador4.Formularios
{
    public partial class FormPrincipal : Form
    {
        private FormTelaInicial formTelaInicial;
        private FormListaPaciente formPaciente;
        private FormListaClinica formClinica;
        private FormListaAtendimento formAtendimento;
        private FormListaConsulta formConsulta;
        private FormListaPsicologo formPsicologo;
        private FormListaLogin formListaLogin;

        public FormPrincipal(Usuario usuario)
        {
            InitializeComponent();
            NivelDeAcesso(usuario);
            TelaInicial();
        }


        private void TelaInicial()
        {
            formTelaInicial = new FormTelaInicial();
            formTelaInicial.MdiParent = this;
            formTelaInicial.Show();
        }


        //
        //Botões Fechar e Minimizar do Menu topo.
        //
        private void btnX_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        //
        //Botões de acesso aas formularios do sistema fixados no menu lateral.
        //
        private void btnPaciente_Click(object sender, EventArgs e)
        {
            formPaciente = new FormListaPaciente();
            formPaciente.MdiParent = this;
            formPaciente.Show();
        }

        private void btnClinicaLista_Click(object sender, EventArgs e)
        {
            formClinica = new FormListaClinica();
            formClinica.MdiParent = this;
            formClinica.Show();
        }

        private void btnAgendamento_Click(object sender, EventArgs e)
        {
            formAtendimento = new FormListaAtendimento();
            formAtendimento.MdiParent = this;
            formAtendimento.Show();
        }

        private void btnExames_Click(object sender, EventArgs e)
        {
            formConsulta = new FormListaConsulta();
            formConsulta.MdiParent = this;
            formConsulta.Show();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            formTelaInicial = new FormTelaInicial();
            formTelaInicial.MdiParent = this;
            formTelaInicial.Show();
        }

        private void btnPsicologos_Click(object sender, EventArgs e)
        {
            formPsicologo = new FormListaPsicologo();
            formPsicologo.MdiParent = this;
            formPsicologo.Show();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            formListaLogin = new FormListaLogin();
            formListaLogin.MdiParent = this;
            formListaLogin.Show();
        }

        public void NivelDeAcesso(Usuario usuario)
        {
            this.lblTipoUsuario.Text = usuario.TipoUsuario.ToString();
            this.lblNomeUsuario.Text = usuario.Nome;

            if (usuario.TipoUsuario == TipoUsuario.Atendente)
            {
                this.btnClinica.Visible = false;
                this.btnLogin.Visible = false;
                this.btnTerapias.Visible = false;
                this.btnPsicologos.Visible = false;
            }else if (usuario.TipoUsuario == TipoUsuario.Psicologo)
            {
                this.btnClinica.Visible = false;
                this.btnLogin.Visible = false;
                this.btnPacientes.Visible = false;
                this.btnPsicologos.Visible = false;
                this.btnConsultas.Visible = false;
            }

        }

        private void btnSairDoSistema_Click(object sender, EventArgs e)
        {
            FormLogin formLogin = new FormLogin();
            formLogin.Show();
            this.Close();
        }

        private void btnPacientes_MouseHover(object sender, EventArgs e)
        {

        }

        private void btnMensagem_Click(object sender, EventArgs e)
        {
            FormEnviarEmail formEnviarEmail = new FormEnviarEmail();
            formEnviarEmail.MdiParent = this;
            formEnviarEmail.Show();
        }
    }
}
