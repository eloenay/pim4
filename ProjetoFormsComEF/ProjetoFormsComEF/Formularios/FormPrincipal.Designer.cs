﻿namespace ProjetoIntegrador4.Formularios
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTipoUsuario = new System.Windows.Forms.Label();
            this.lblNomeUsuario = new System.Windows.Forms.Label();
            this.btnMinimizar = new System.Windows.Forms.Label();
            this.btnX = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.DetalhesDosBotoes = new System.Windows.Forms.ToolTip(this.components);
            this.btnTerapias = new System.Windows.Forms.PictureBox();
            this.btnPacientes = new System.Windows.Forms.PictureBox();
            this.btnMensagem = new System.Windows.Forms.PictureBox();
            this.btnPsicologos = new System.Windows.Forms.PictureBox();
            this.btnClinica = new System.Windows.Forms.PictureBox();
            this.btnConsultas = new System.Windows.Forms.PictureBox();
            this.btnLogin = new System.Windows.Forms.PictureBox();
            this.btnSairDoSistema = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnTerapias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMensagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPsicologos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSairDoSistema)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.lblTipoUsuario);
            this.panel1.Controls.Add(this.lblNomeUsuario);
            this.panel1.Controls.Add(this.btnMinimizar);
            this.panel1.Controls.Add(this.btnX);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1003, 58);
            this.panel1.TabIndex = 0;
            // 
            // lblTipoUsuario
            // 
            this.lblTipoUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoUsuario.AutoSize = true;
            this.lblTipoUsuario.Font = new System.Drawing.Font("Segoe Print", 10F);
            this.lblTipoUsuario.ForeColor = System.Drawing.Color.White;
            this.lblTipoUsuario.Location = new System.Drawing.Point(701, 7);
            this.lblTipoUsuario.Name = "lblTipoUsuario";
            this.lblTipoUsuario.Size = new System.Drawing.Size(113, 24);
            this.lblTipoUsuario.TabIndex = 20;
            this.lblTipoUsuario.Text = "Administrador";
            this.lblTipoUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNomeUsuario
            // 
            this.lblNomeUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNomeUsuario.AutoSize = true;
            this.lblNomeUsuario.Font = new System.Drawing.Font("Segoe Print", 10F);
            this.lblNomeUsuario.ForeColor = System.Drawing.Color.White;
            this.lblNomeUsuario.Location = new System.Drawing.Point(695, 27);
            this.lblNomeUsuario.Name = "lblNomeUsuario";
            this.lblNomeUsuario.Size = new System.Drawing.Size(120, 24);
            this.lblNomeUsuario.TabIndex = 4;
            this.lblNomeUsuario.Text = "Eloenay Pereira";
            this.lblNomeUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.AutoSize = true;
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.Font = new System.Drawing.Font("Segoe Script", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.ForeColor = System.Drawing.Color.White;
            this.btnMinimizar.Location = new System.Drawing.Point(930, 11);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(26, 31);
            this.btnMinimizar.TabIndex = 17;
            this.btnMinimizar.Text = "-";
            this.DetalhesDosBotoes.SetToolTip(this.btnMinimizar, "Minimizar");
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnX
            // 
            this.btnX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnX.AutoSize = true;
            this.btnX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnX.Font = new System.Drawing.Font("Segoe Script", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnX.ForeColor = System.Drawing.Color.White;
            this.btnX.Location = new System.Drawing.Point(959, 11);
            this.btnX.Name = "btnX";
            this.btnX.Size = new System.Drawing.Size(28, 31);
            this.btnX.TabIndex = 16;
            this.btnX.Text = "X";
            this.DetalhesDosBotoes.SetToolTip(this.btnX, "Fechar");
            this.btnX.Click += new System.EventHandler(this.btnX_Click);
            // 
            // btnHome
            // 
            this.btnHome.AutoSize = true;
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.Font = new System.Drawing.Font("Segoe Script", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Location = new System.Drawing.Point(142, 2);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(141, 55);
            this.btnHome.TabIndex = 15;
            this.btnHome.Text = " SGCP";
            this.DetalhesDosBotoes.SetToolTip(this.btnHome, "Inicio");
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(0, 686);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1003, 106);
            this.panel3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(28, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "© Copyright 2018 - Direitos reservados";
            // 
            // btnTerapias
            // 
            this.btnTerapias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTerapias.Image = ((System.Drawing.Image)(resources.GetObject("btnTerapias.Image")));
            this.btnTerapias.Location = new System.Drawing.Point(18, 280);
            this.btnTerapias.Name = "btnTerapias";
            this.btnTerapias.Size = new System.Drawing.Size(49, 44);
            this.btnTerapias.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnTerapias.TabIndex = 17;
            this.btnTerapias.TabStop = false;
            this.DetalhesDosBotoes.SetToolTip(this.btnTerapias, "Terapias");
            this.btnTerapias.Click += new System.EventHandler(this.btnAgendamento_Click);
            // 
            // btnPacientes
            // 
            this.btnPacientes.AccessibleDescription = "Cadastrar Paciente";
            this.btnPacientes.AccessibleName = "";
            this.btnPacientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPacientes.Image = ((System.Drawing.Image)(resources.GetObject("btnPacientes.Image")));
            this.btnPacientes.Location = new System.Drawing.Point(18, 66);
            this.btnPacientes.Name = "btnPacientes";
            this.btnPacientes.Size = new System.Drawing.Size(49, 43);
            this.btnPacientes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPacientes.TabIndex = 13;
            this.btnPacientes.TabStop = false;
            this.btnPacientes.Tag = "";
            this.DetalhesDosBotoes.SetToolTip(this.btnPacientes, "Pacientes");
            this.btnPacientes.Click += new System.EventHandler(this.btnPaciente_Click);
            this.btnPacientes.MouseHover += new System.EventHandler(this.btnPacientes_MouseHover);
            // 
            // btnMensagem
            // 
            this.btnMensagem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMensagem.Image = ((System.Drawing.Image)(resources.GetObject("btnMensagem.Image")));
            this.btnMensagem.Location = new System.Drawing.Point(18, 346);
            this.btnMensagem.Name = "btnMensagem";
            this.btnMensagem.Size = new System.Drawing.Size(49, 49);
            this.btnMensagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMensagem.TabIndex = 19;
            this.btnMensagem.TabStop = false;
            this.DetalhesDosBotoes.SetToolTip(this.btnMensagem, "Mensagens");
            this.btnMensagem.Click += new System.EventHandler(this.btnMensagem_Click);
            // 
            // btnPsicologos
            // 
            this.btnPsicologos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPsicologos.Image = ((System.Drawing.Image)(resources.GetObject("btnPsicologos.Image")));
            this.btnPsicologos.Location = new System.Drawing.Point(18, 131);
            this.btnPsicologos.Name = "btnPsicologos";
            this.btnPsicologos.Size = new System.Drawing.Size(49, 44);
            this.btnPsicologos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPsicologos.TabIndex = 14;
            this.btnPsicologos.TabStop = false;
            this.DetalhesDosBotoes.SetToolTip(this.btnPsicologos, "Psicologos");
            this.btnPsicologos.Click += new System.EventHandler(this.btnPsicologos_Click);
            // 
            // btnClinica
            // 
            this.btnClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnClinica.Image")));
            this.btnClinica.Location = new System.Drawing.Point(18, 421);
            this.btnClinica.Name = "btnClinica";
            this.btnClinica.Size = new System.Drawing.Size(49, 47);
            this.btnClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnClinica.TabIndex = 18;
            this.btnClinica.TabStop = false;
            this.DetalhesDosBotoes.SetToolTip(this.btnClinica, "Clinicas");
            this.btnClinica.Click += new System.EventHandler(this.btnClinicaLista_Click);
            // 
            // btnConsultas
            // 
            this.btnConsultas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultas.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultas.Image")));
            this.btnConsultas.Location = new System.Drawing.Point(18, 205);
            this.btnConsultas.Name = "btnConsultas";
            this.btnConsultas.Size = new System.Drawing.Size(49, 48);
            this.btnConsultas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnConsultas.TabIndex = 15;
            this.btnConsultas.TabStop = false;
            this.DetalhesDosBotoes.SetToolTip(this.btnConsultas, "Consultas");
            this.btnConsultas.Click += new System.EventHandler(this.btnExames_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.AccessibleDescription = "Login";
            this.btnLogin.AccessibleName = "Login";
            this.btnLogin.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnLogin.Image")));
            this.btnLogin.Location = new System.Drawing.Point(18, 493);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(49, 43);
            this.btnLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnLogin.TabIndex = 20;
            this.btnLogin.TabStop = false;
            this.DetalhesDosBotoes.SetToolTip(this.btnLogin, "Login");
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnSairDoSistema
            // 
            this.btnSairDoSistema.AccessibleDescription = "Cadastrar Paciente";
            this.btnSairDoSistema.AccessibleName = "";
            this.btnSairDoSistema.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSairDoSistema.BackColor = System.Drawing.Color.White;
            this.btnSairDoSistema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSairDoSistema.Image = ((System.Drawing.Image)(resources.GetObject("btnSairDoSistema.Image")));
            this.btnSairDoSistema.Location = new System.Drawing.Point(973, 71);
            this.btnSairDoSistema.Name = "btnSairDoSistema";
            this.btnSairDoSistema.Size = new System.Drawing.Size(16, 24);
            this.btnSairDoSistema.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnSairDoSistema.TabIndex = 21;
            this.btnSairDoSistema.TabStop = false;
            this.btnSairDoSistema.Tag = "";
            this.DetalhesDosBotoes.SetToolTip(this.btnSairDoSistema, "Deslogar do Sistema");
            this.btnSairDoSistema.Click += new System.EventHandler(this.btnSairDoSistema_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.Controls.Add(this.btnLogin);
            this.panel2.Controls.Add(this.btnConsultas);
            this.panel2.Controls.Add(this.btnClinica);
            this.panel2.Controls.Add(this.btnPsicologos);
            this.panel2.Controls.Add(this.btnMensagem);
            this.panel2.Controls.Add(this.btnPacientes);
            this.panel2.Controls.Add(this.btnTerapias);
            this.panel2.Location = new System.Drawing.Point(0, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(89, 649);
            this.panel2.TabIndex = 1;
            // 
            // FormPrincipal
            // 
            this.AccessibleName = "";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PapayaWhip;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1001, 788);
            this.Controls.Add(this.btnSairDoSistema);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "FormPrincipal";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnTerapias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMensagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPsicologos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSairDoSistema)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label btnHome;
        private System.Windows.Forms.Label btnMinimizar;
        private System.Windows.Forms.Label btnX;
        private System.Windows.Forms.ToolTip DetalhesDosBotoes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox btnLogin;
        private System.Windows.Forms.PictureBox btnConsultas;
        private System.Windows.Forms.PictureBox btnClinica;
        private System.Windows.Forms.PictureBox btnPsicologos;
        private System.Windows.Forms.PictureBox btnMensagem;
        private System.Windows.Forms.PictureBox btnPacientes;
        private System.Windows.Forms.PictureBox btnTerapias;
        private System.Windows.Forms.Label lblNomeUsuario;
        private System.Windows.Forms.Label lblTipoUsuario;
        private System.Windows.Forms.PictureBox btnSairDoSistema;
    }
}