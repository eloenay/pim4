﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Consultas
{
    public partial class FormBuscarPsicologoConsulta : Form
    {
        private readonly FormNovaConsulta formNovaConsultas;
        public FormBuscarPsicologoConsulta(FormNovaConsulta formNovaConsulta)
        {
            InitializeComponent();
            formNovaConsultas = formNovaConsulta;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPequisarPsicologo_Click(object sender, EventArgs e)
        {
            Psicologo psicologo = new Psicologo();
            dgvPsicologo.DataSource = psicologo.BuscarPorNome(txtPsicologo.Text);
            if (dgvPsicologo.DataSource == null)
            {
                MessageBox.Show("Nao possui Pacientes Cadastrados!");
            }
        }

        private void dgvPsicologo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int)dgvPsicologo.CurrentRow.Cells["Id"].Value;
            Psicologo psicologo = new Psicologo();
            formNovaConsultas.consulta.Psicologo = psicologo.BuscarPorId(id);
            formNovaConsultas.BuscarPsicologo();
            Close();
        }
    }
}
