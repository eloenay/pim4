﻿using System;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.Consultas;
using ProjetoIntegrador4.Formularios.Terapias;

namespace ProjetoIntegrador4.Formularios.FormsAdicionais
{
    public partial class FormBuscarPaciente : Form
    {
        private readonly FormNovaConsulta formNovaConsultas;

        public FormBuscarPaciente(FormNovaConsulta formNovaConsulta)
        {
            InitializeComponent();
            formNovaConsultas = formNovaConsulta;
        }

        private void btnPequisarPaciente_Click(object sender, EventArgs e)
        {
            Paciente paciente = new Paciente();
            dgvPaciente.DataSource = paciente.BuscarPorNome(txtPaciente.Text);
            if (dgvPaciente.DataSource == null)
            {
                MessageBox.Show("Nao possui Pacientes Cadastrados!");
            }
        }

        private void dgvPaciente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int)dgvPaciente.CurrentRow.Cells["Id"].Value;
            Paciente paciente = new Paciente();
            formNovaConsultas.consulta.Paciente = paciente.BuscarPorId(id);
            formNovaConsultas.BuscarPaciente();
            Close();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
