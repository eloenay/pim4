﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.Terapias;

namespace ProjetoIntegrador4.Formularios.FormsAdicionais
{
    public partial class FormBuscarConsulta : Form
    {
        private FormNovaTerapia formNovaTerapias;

        public FormBuscarConsulta(FormNovaTerapia formNovaTerapia)
        {
            InitializeComponent();
            formNovaTerapias = formNovaTerapia;
        }

        private void btnPequisarPaciente_Click(object sender, EventArgs e)
        {
            Consulta consulta = new Consulta();
            dgvPaciente.DataSource = consulta.BuscarPorPaciente(txtPaciente.Text);
        }

        private void dgvPaciente_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int id = (int)dgvPaciente.CurrentRow.Cells["Id"].Value;
            Consulta consulta = new Consulta();
            formNovaTerapias.terapia.Consulta = consulta.BuscarPorId(id);
            formNovaTerapias.BuscarPacienteConsulta();
            Close();
        }
    }
}
