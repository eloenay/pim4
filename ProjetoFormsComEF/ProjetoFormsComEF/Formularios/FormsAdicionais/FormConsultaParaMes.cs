﻿using ProjetoIntegrador4.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoIntegrador4.Formularios.FormsAdicionais
{
    public partial class FormConsultaParaMes : Form
    {
        public FormConsultaParaMes()
        {
            InitializeComponent();
            BuscarConsultaParaMes();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void BuscarConsultaParaMes()
        {
            Consulta consulta = new Consulta();
            dgvConsultaParaMes.DataSource = consulta.BuscarConsultaParaMes();
        }
    }
}
