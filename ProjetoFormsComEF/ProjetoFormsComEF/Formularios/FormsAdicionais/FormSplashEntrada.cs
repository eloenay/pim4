﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.Login;

namespace ProjetoIntegrador4.Formularios.FormsAdicionais
{
    public partial class FormSplashEntrada : Form
    {
        public FormSplashEntrada()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value = progressBar1.Value + 1;
            }
            else
            {
                this.Visible = false;
                FormLogin telaLogin = new FormLogin();
                telaLogin.Visible = true;
                timer1.Stop();
            }
        }
    }
}
