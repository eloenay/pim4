﻿namespace ProjetoIntegrador4.Formularios.Psicologos
{
    partial class FormDetalhesPsicologo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalhesPsicologo));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDetalhesCpflPsicologo = new System.Windows.Forms.MaskedTextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnFinalizarPsicologo = new System.Windows.Forms.Button();
            this.cbxRegiaoPsicologo = new System.Windows.Forms.ComboBox();
            this.txtDataNascimentoPsicologo = new System.Windows.Forms.DateTimePicker();
            this.btnNovaSalaPsicologo = new System.Windows.Forms.PictureBox();
            this.cbxSalaPsicologo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPequisarPsicologo = new System.Windows.Forms.PictureBox();
            this.txtPsicologo = new System.Windows.Forms.TextBox();
            this.dgvPsicologo = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.lblIdDetalhesPsicologo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDetalhesNumeroCrpPsicologo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDetalhesNomePsicologo = new System.Windows.Forms.TextBox();
            this.btnFechar = new System.Windows.Forms.Label();
            this.btnExcluirPsicologo = new System.Windows.Forms.PictureBox();
            this.btnEditarPsicologo = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaSalaPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarPsicologo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFechar);
            this.groupBox1.Controls.Add(this.btnExcluirPsicologo);
            this.groupBox1.Controls.Add(this.btnEditarPsicologo);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalhes do Psicólogo";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDetalhesCpflPsicologo);
            this.groupBox2.Controls.Add(this.btnCancelar);
            this.groupBox2.Controls.Add(this.btnFinalizarPsicologo);
            this.groupBox2.Controls.Add(this.cbxRegiaoPsicologo);
            this.groupBox2.Controls.Add(this.txtDataNascimentoPsicologo);
            this.groupBox2.Controls.Add(this.btnNovaSalaPsicologo);
            this.groupBox2.Controls.Add(this.cbxSalaPsicologo);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnPequisarPsicologo);
            this.groupBox2.Controls.Add(this.txtPsicologo);
            this.groupBox2.Controls.Add(this.dgvPsicologo);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lblIdDetalhesPsicologo);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtDetalhesNumeroCrpPsicologo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtDetalhesNomePsicologo);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(23, 63);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(927, 397);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Psicólogo";
            // 
            // txtDetalhesCpflPsicologo
            // 
            this.txtDetalhesCpflPsicologo.Enabled = false;
            this.txtDetalhesCpflPsicologo.Location = new System.Drawing.Point(52, 147);
            this.txtDetalhesCpflPsicologo.Mask = "000.000.000-00";
            this.txtDetalhesCpflPsicologo.Name = "txtDetalhesCpflPsicologo";
            this.txtDetalhesCpflPsicologo.Size = new System.Drawing.Size(194, 33);
            this.txtDetalhesCpflPsicologo.TabIndex = 6;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(313, 331);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 46);
            this.btnCancelar.TabIndex = 14;
            this.btnCancelar.Text = "Cancelar";
            this.toolTip1.SetToolTip(this.btnCancelar, "Finalizar Novo Psicólogo");
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnFinalizarPsicologo
            // 
            this.btnFinalizarPsicologo.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarPsicologo.Location = new System.Drawing.Point(81, 331);
            this.btnFinalizarPsicologo.Name = "btnFinalizarPsicologo";
            this.btnFinalizarPsicologo.Size = new System.Drawing.Size(120, 46);
            this.btnFinalizarPsicologo.TabIndex = 13;
            this.btnFinalizarPsicologo.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarPsicologo, "Finalizar Novo Psicólogo");
            this.btnFinalizarPsicologo.UseVisualStyleBackColor = true;
            this.btnFinalizarPsicologo.Visible = false;
            this.btnFinalizarPsicologo.Click += new System.EventHandler(this.btnFinalizarPsicologo_Click);
            // 
            // cbxRegiaoPsicologo
            // 
            this.cbxRegiaoPsicologo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxRegiaoPsicologo.Enabled = false;
            this.cbxRegiaoPsicologo.FormattingEnabled = true;
            this.cbxRegiaoPsicologo.Location = new System.Drawing.Point(85, 267);
            this.cbxRegiaoPsicologo.Name = "cbxRegiaoPsicologo";
            this.cbxRegiaoPsicologo.Size = new System.Drawing.Size(327, 34);
            this.cbxRegiaoPsicologo.TabIndex = 12;
            // 
            // txtDataNascimentoPsicologo
            // 
            this.txtDataNascimentoPsicologo.Enabled = false;
            this.txtDataNascimentoPsicologo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDataNascimentoPsicologo.Location = new System.Drawing.Point(172, 96);
            this.txtDataNascimentoPsicologo.Name = "txtDataNascimentoPsicologo";
            this.txtDataNascimentoPsicologo.Size = new System.Drawing.Size(155, 33);
            this.txtDataNascimentoPsicologo.TabIndex = 3;
            // 
            // btnNovaSalaPsicologo
            // 
            this.btnNovaSalaPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovaSalaPsicologo.BackColor = System.Drawing.Color.Transparent;
            this.btnNovaSalaPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovaSalaPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnNovaSalaPsicologo.Image")));
            this.btnNovaSalaPsicologo.Location = new System.Drawing.Point(484, 153);
            this.btnNovaSalaPsicologo.Name = "btnNovaSalaPsicologo";
            this.btnNovaSalaPsicologo.Size = new System.Drawing.Size(31, 30);
            this.btnNovaSalaPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovaSalaPsicologo.TabIndex = 33;
            this.btnNovaSalaPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovaSalaPsicologo, "Cadastrar Nova Terapia");
            this.btnNovaSalaPsicologo.Visible = false;
            this.btnNovaSalaPsicologo.Click += new System.EventHandler(this.btnNovaSalaPsicologo_Click);
            // 
            // cbxSalaPsicologo
            // 
            this.cbxSalaPsicologo.Enabled = false;
            this.cbxSalaPsicologo.FormattingEnabled = true;
            this.cbxSalaPsicologo.Location = new System.Drawing.Point(299, 150);
            this.cbxSalaPsicologo.Name = "cbxSalaPsicologo";
            this.cbxSalaPsicologo.Size = new System.Drawing.Size(175, 34);
            this.cbxSalaPsicologo.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(526, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 22);
            this.label7.TabIndex = 15;
            this.label7.Text = "Pesquisar:";
            // 
            // btnPequisarPsicologo
            // 
            this.btnPequisarPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPequisarPsicologo.BackColor = System.Drawing.Color.Transparent;
            this.btnPequisarPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPequisarPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnPequisarPsicologo.Image")));
            this.btnPequisarPsicologo.Location = new System.Drawing.Point(884, 24);
            this.btnPequisarPsicologo.Name = "btnPequisarPsicologo";
            this.btnPequisarPsicologo.Size = new System.Drawing.Size(37, 30);
            this.btnPequisarPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPequisarPsicologo.TabIndex = 30;
            this.btnPequisarPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPequisarPsicologo, "Pesquisar Psicólogo");
            this.btnPequisarPsicologo.Click += new System.EventHandler(this.btnPequisarPsicologo_Click);
            // 
            // txtPsicologo
            // 
            this.txtPsicologo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPsicologo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPsicologo.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPsicologo.Location = new System.Drawing.Point(629, 25);
            this.txtPsicologo.Name = "txtPsicologo";
            this.txtPsicologo.Size = new System.Drawing.Size(249, 29);
            this.txtPsicologo.TabIndex = 17;
            // 
            // dgvPsicologo
            // 
            this.dgvPsicologo.AllowUserToAddRows = false;
            this.dgvPsicologo.AllowUserToDeleteRows = false;
            this.dgvPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPsicologo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPsicologo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPsicologo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPsicologo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPsicologo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPsicologo.Location = new System.Drawing.Point(529, 60);
            this.dgvPsicologo.Name = "dgvPsicologo";
            this.dgvPsicologo.ReadOnly = true;
            this.dgvPsicologo.Size = new System.Drawing.Size(392, 331);
            this.dgvPsicologo.TabIndex = 16;
            this.dgvPsicologo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPsicologo_CellClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(252, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 22);
            this.label4.TabIndex = 7;
            this.label4.Text = "Sala:";
            // 
            // lblIdDetalhesPsicologo
            // 
            this.lblIdDetalhesPsicologo.AutoSize = true;
            this.lblIdDetalhesPsicologo.Location = new System.Drawing.Point(413, 99);
            this.lblIdDetalhesPsicologo.Name = "lblIdDetalhesPsicologo";
            this.lblIdDetalhesPsicologo.Size = new System.Drawing.Size(30, 26);
            this.lblIdDetalhesPsicologo.TabIndex = 4;
            this.lblIdDetalhesPsicologo.Text = "Id";
            this.lblIdDetalhesPsicologo.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 212);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 22);
            this.label1.TabIndex = 9;
            this.label1.Text = "Nº Registro do CRP:";
            // 
            // txtDetalhesNumeroCrpPsicologo
            // 
            this.txtDetalhesNumeroCrpPsicologo.Enabled = false;
            this.txtDetalhesNumeroCrpPsicologo.Location = new System.Drawing.Point(196, 209);
            this.txtDetalhesNumeroCrpPsicologo.Name = "txtDetalhesNumeroCrpPsicologo";
            this.txtDetalhesNumeroCrpPsicologo.Size = new System.Drawing.Size(216, 33);
            this.txtDetalhesNumeroCrpPsicologo.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 22);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cpf:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 22);
            this.label5.TabIndex = 11;
            this.label5.Text = "Região:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome:";
            // 
            // txtDetalhesNomePsicologo
            // 
            this.txtDetalhesNomePsicologo.Enabled = false;
            this.txtDetalhesNomePsicologo.Location = new System.Drawing.Point(81, 46);
            this.txtDetalhesNomePsicologo.Name = "txtDetalhesNomePsicologo";
            this.txtDetalhesNomePsicologo.Size = new System.Drawing.Size(425, 33);
            this.txtDetalhesNomePsicologo.TabIndex = 1;
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.AutoSize = true;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(946, -7);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(25, 26);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.Text = "X";
            this.toolTip1.SetToolTip(this.btnFechar, "Fechar");
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnExcluirPsicologo
            // 
            this.btnExcluirPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluirPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirPsicologo.Image")));
            this.btnExcluirPsicologo.Location = new System.Drawing.Point(523, 466);
            this.btnExcluirPsicologo.Name = "btnExcluirPsicologo";
            this.btnExcluirPsicologo.Size = new System.Drawing.Size(69, 59);
            this.btnExcluirPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExcluirPsicologo.TabIndex = 27;
            this.btnExcluirPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnExcluirPsicologo, "Excluir Psicólogo");
            this.btnExcluirPsicologo.Click += new System.EventHandler(this.btnExcluirPsicologo_Click);
            // 
            // btnEditarPsicologo
            // 
            this.btnEditarPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarPsicologo.Image")));
            this.btnEditarPsicologo.Location = new System.Drawing.Point(402, 466);
            this.btnEditarPsicologo.Name = "btnEditarPsicologo";
            this.btnEditarPsicologo.Size = new System.Drawing.Size(69, 59);
            this.btnEditarPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditarPsicologo.TabIndex = 26;
            this.btnEditarPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnEditarPsicologo, "Editar Psicólogo");
            this.btnEditarPsicologo.Click += new System.EventHandler(this.btnEditarPsicologo_Click);
            // 
            // FormDetalhesPsicologo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormDetalhesPsicologo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormDetalhesPsicologo";
            this.Load += new System.EventHandler(this.FormDetalhesPsicologo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaSalaPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarPsicologo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label btnFechar;
        private System.Windows.Forms.PictureBox btnExcluirPsicologo;
        private System.Windows.Forms.PictureBox btnEditarPsicologo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDetalhesNumeroCrpPsicologo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDetalhesNomePsicologo;
        private System.Windows.Forms.Label lblIdDetalhesPsicologo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox btnPequisarPsicologo;
        private System.Windows.Forms.TextBox txtPsicologo;
        private System.Windows.Forms.DataGridView dgvPsicologo;
        private System.Windows.Forms.PictureBox btnNovaSalaPsicologo;
        private System.Windows.Forms.ComboBox cbxSalaPsicologo;
        private System.Windows.Forms.DateTimePicker txtDataNascimentoPsicologo;
        private System.Windows.Forms.ComboBox cbxRegiaoPsicologo;
        private System.Windows.Forms.Button btnFinalizarPsicologo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.MaskedTextBox txtDetalhesCpflPsicologo;
    }
}