﻿namespace ProjetoIntegrador4.Formularios.Psicologos
{
    partial class FormListaPsicologo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaPsicologo));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDetalhesPsicologo = new System.Windows.Forms.PictureBox();
            this.btnNovoPsicologo = new System.Windows.Forms.PictureBox();
            this.btnVoltaPsicologo = new System.Windows.Forms.PictureBox();
            this.btnPequisarPsicologo = new System.Windows.Forms.PictureBox();
            this.txtPsicologo = new System.Windows.Forms.TextBox();
            this.dgvPsicologo = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPsicologo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPsicologo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnDetalhesPsicologo);
            this.groupBox1.Controls.Add(this.btnNovoPsicologo);
            this.groupBox1.Controls.Add(this.btnVoltaPsicologo);
            this.groupBox1.Controls.Add(this.btnPequisarPsicologo);
            this.groupBox1.Controls.Add(this.txtPsicologo);
            this.groupBox1.Controls.Add(this.dgvPsicologo);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Psicólogos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 26);
            this.label2.TabIndex = 27;
            this.label2.Text = "Pesquisar:";
            // 
            // btnDetalhesPsicologo
            // 
            this.btnDetalhesPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalhesPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalhesPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnDetalhesPsicologo.Image")));
            this.btnDetalhesPsicologo.Location = new System.Drawing.Point(473, 466);
            this.btnDetalhesPsicologo.Name = "btnDetalhesPsicologo";
            this.btnDetalhesPsicologo.Size = new System.Drawing.Size(73, 59);
            this.btnDetalhesPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetalhesPsicologo.TabIndex = 26;
            this.btnDetalhesPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnDetalhesPsicologo, "Detalhes do Psicólogo");
            this.btnDetalhesPsicologo.Click += new System.EventHandler(this.btnDetalhesPsicologo_Click);
            // 
            // btnNovoPsicologo
            // 
            this.btnNovoPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovoPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovoPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoPsicologo.Image")));
            this.btnNovoPsicologo.Location = new System.Drawing.Point(919, 62);
            this.btnNovoPsicologo.Name = "btnNovoPsicologo";
            this.btnNovoPsicologo.Size = new System.Drawing.Size(49, 45);
            this.btnNovoPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovoPsicologo.TabIndex = 16;
            this.btnNovoPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovoPsicologo, "Novo Psicólogo");
            this.btnNovoPsicologo.Click += new System.EventHandler(this.btnNovoPsicologo_Click);
            // 
            // btnVoltaPsicologo
            // 
            this.btnVoltaPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltaPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltaPsicologo.Image")));
            this.btnVoltaPsicologo.Location = new System.Drawing.Point(11, 71);
            this.btnVoltaPsicologo.Name = "btnVoltaPsicologo";
            this.btnVoltaPsicologo.Size = new System.Drawing.Size(48, 36);
            this.btnVoltaPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltaPsicologo.TabIndex = 17;
            this.btnVoltaPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnVoltaPsicologo, "Voltar");
            this.btnVoltaPsicologo.Click += new System.EventHandler(this.btnVoltaPsicologo_Click);
            // 
            // btnPequisarPsicologo
            // 
            this.btnPequisarPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPequisarPsicologo.BackColor = System.Drawing.Color.Transparent;
            this.btnPequisarPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPequisarPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnPequisarPsicologo.Image")));
            this.btnPequisarPsicologo.Location = new System.Drawing.Point(919, 154);
            this.btnPequisarPsicologo.Name = "btnPequisarPsicologo";
            this.btnPequisarPsicologo.Size = new System.Drawing.Size(50, 48);
            this.btnPequisarPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPequisarPsicologo.TabIndex = 24;
            this.btnPequisarPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPequisarPsicologo, "Pesquisar Psicólogo");
            this.btnPequisarPsicologo.Click += new System.EventHandler(this.btnPequisarPsicologo_Click);
            // 
            // txtPsicologo
            // 
            this.txtPsicologo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPsicologo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPsicologo.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPsicologo.Location = new System.Drawing.Point(112, 169);
            this.txtPsicologo.Name = "txtPsicologo";
            this.txtPsicologo.Size = new System.Drawing.Size(781, 29);
            this.txtPsicologo.TabIndex = 22;
            // 
            // dgvPsicologo
            // 
            this.dgvPsicologo.AllowUserToAddRows = false;
            this.dgvPsicologo.AllowUserToDeleteRows = false;
            this.dgvPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPsicologo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPsicologo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPsicologo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPsicologo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPsicologo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPsicologo.Location = new System.Drawing.Point(6, 204);
            this.dgvPsicologo.Name = "dgvPsicologo";
            this.dgvPsicologo.ReadOnly = true;
            this.dgvPsicologo.Size = new System.Drawing.Size(962, 256);
            this.dgvPsicologo.TabIndex = 21;
            // 
            // FormListaPsicologo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(205, 90);
            this.Name = "FormListaPsicologo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormListaPsicologo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPsicologo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPsicologo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox btnDetalhesPsicologo;
        private System.Windows.Forms.PictureBox btnNovoPsicologo;
        private System.Windows.Forms.PictureBox btnVoltaPsicologo;
        private System.Windows.Forms.PictureBox btnPequisarPsicologo;
        private System.Windows.Forms.TextBox txtPsicologo;
        private System.Windows.Forms.DataGridView dgvPsicologo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label2;
    }
}