﻿namespace ProjetoIntegrador4.Formularios.Psicologos
{
    partial class FormNovoPsicologo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNovoPsicologo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFinalizarPsicologo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCpfPsicologo = new System.Windows.Forms.MaskedTextBox();
            this.btnNovaSalaPsicologo = new System.Windows.Forms.PictureBox();
            this.cbxSalaPsicologo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumeroCrpPsicologo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxRegiaoPsicologo = new System.Windows.Forms.ComboBox();
            this.txtDataNascimentoPsicologo = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomePsicologo = new System.Windows.Forms.TextBox();
            this.btnFechar = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaSalaPsicologo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.btnFinalizarPsicologo);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFechar);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cadastro de Psicólogos";
            // 
            // btnFinalizarPsicologo
            // 
            this.btnFinalizarPsicologo.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarPsicologo.Location = new System.Drawing.Point(414, 443);
            this.btnFinalizarPsicologo.Name = "btnFinalizarPsicologo";
            this.btnFinalizarPsicologo.Size = new System.Drawing.Size(142, 64);
            this.btnFinalizarPsicologo.TabIndex = 2;
            this.btnFinalizarPsicologo.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarPsicologo, "Finalizar Novo Psicólogo");
            this.btnFinalizarPsicologo.UseVisualStyleBackColor = true;
            this.btnFinalizarPsicologo.Click += new System.EventHandler(this.btnFinalizarPsicologo_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCpfPsicologo);
            this.groupBox2.Controls.Add(this.btnNovaSalaPsicologo);
            this.groupBox2.Controls.Add(this.cbxSalaPsicologo);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtNumeroCrpPsicologo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbxRegiaoPsicologo);
            this.groupBox2.Controls.Add(this.txtDataNascimentoPsicologo);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNomePsicologo);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(76, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(822, 355);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Psicólogo";
            // 
            // txtCpfPsicologo
            // 
            this.txtCpfPsicologo.Location = new System.Drawing.Point(95, 159);
            this.txtCpfPsicologo.Mask = "000.000.000-00";
            this.txtCpfPsicologo.Name = "txtCpfPsicologo";
            this.txtCpfPsicologo.Size = new System.Drawing.Size(211, 33);
            this.txtCpfPsicologo.TabIndex = 5;
            // 
            // btnNovaSalaPsicologo
            // 
            this.btnNovaSalaPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovaSalaPsicologo.BackColor = System.Drawing.Color.Transparent;
            this.btnNovaSalaPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovaSalaPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnNovaSalaPsicologo.Image")));
            this.btnNovaSalaPsicologo.Location = new System.Drawing.Point(594, 162);
            this.btnNovaSalaPsicologo.Name = "btnNovaSalaPsicologo";
            this.btnNovaSalaPsicologo.Size = new System.Drawing.Size(31, 30);
            this.btnNovaSalaPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovaSalaPsicologo.TabIndex = 26;
            this.btnNovaSalaPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovaSalaPsicologo, "Cadastrar Nova Terapia");
            this.btnNovaSalaPsicologo.Click += new System.EventHandler(this.btnNovaSalaPsicologo_Click);
            // 
            // cbxSalaPsicologo
            // 
            this.cbxSalaPsicologo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSalaPsicologo.FormattingEnabled = true;
            this.cbxSalaPsicologo.Location = new System.Drawing.Point(395, 159);
            this.cbxSalaPsicologo.Name = "cbxSalaPsicologo";
            this.cbxSalaPsicologo.Size = new System.Drawing.Size(193, 34);
            this.cbxSalaPsicologo.TabIndex = 7;
            this.cbxSalaPsicologo.Click += new System.EventHandler(this.cbxSalaPsicologo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(334, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sala:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 26);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nº Registro do CRP:";
            // 
            // txtNumeroCrpPsicologo
            // 
            this.txtNumeroCrpPsicologo.Location = new System.Drawing.Point(232, 217);
            this.txtNumeroCrpPsicologo.Name = "txtNumeroCrpPsicologo";
            this.txtNumeroCrpPsicologo.Size = new System.Drawing.Size(216, 33);
            this.txtNumeroCrpPsicologo.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 26);
            this.label6.TabIndex = 4;
            this.label6.Text = "Cpf:";
            // 
            // cbxRegiaoPsicologo
            // 
            this.cbxRegiaoPsicologo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxRegiaoPsicologo.FormattingEnabled = true;
            this.cbxRegiaoPsicologo.Location = new System.Drawing.Point(121, 275);
            this.cbxRegiaoPsicologo.Name = "cbxRegiaoPsicologo";
            this.cbxRegiaoPsicologo.Size = new System.Drawing.Size(327, 34);
            this.cbxRegiaoPsicologo.TabIndex = 11;
            this.cbxRegiaoPsicologo.Click += new System.EventHandler(this.cbxRegiaoPsicologo_Click);
            // 
            // txtDataNascimentoPsicologo
            // 
            this.txtDataNascimentoPsicologo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDataNascimentoPsicologo.Location = new System.Drawing.Point(212, 109);
            this.txtDataNascimentoPsicologo.Name = "txtDataNascimentoPsicologo";
            this.txtDataNascimentoPsicologo.Size = new System.Drawing.Size(155, 33);
            this.txtDataNascimentoPsicologo.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "Região:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome:";
            // 
            // txtNomePsicologo
            // 
            this.txtNomePsicologo.Location = new System.Drawing.Point(117, 54);
            this.txtNomePsicologo.Name = "txtNomePsicologo";
            this.txtNomePsicologo.Size = new System.Drawing.Size(537, 33);
            this.txtNomePsicologo.TabIndex = 1;
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.AutoSize = true;
            this.btnFechar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(946, -7);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(25, 26);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.Text = "X";
            this.toolTip1.SetToolTip(this.btnFechar, "Fechar");
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // FormNovoPsicologo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormNovoPsicologo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormNovoPsicologo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaSalaPsicologo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label btnFechar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnFinalizarPsicologo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumeroCrpPsicologo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxRegiaoPsicologo;
        private System.Windows.Forms.DateTimePicker txtDataNascimentoPsicologo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomePsicologo;
        private System.Windows.Forms.ComboBox cbxSalaPsicologo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox btnNovaSalaPsicologo;
        private System.Windows.Forms.MaskedTextBox txtCpfPsicologo;
    }
}