﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Psicologos
{
    public partial class FormListaPsicologo : Form
    {
        private FormNovoPsicologo formNovoPsicologo;
        private FormDetalhesPsicologo formDetalhesPsicologo;

        public FormListaPsicologo()
        {
            InitializeComponent();
            CarregarLista();
        }

        private void btnVoltaPsicologo_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void CarregarLista()
        {
            Psicologo psicologo = new Psicologo();
            dgvPsicologo.DataSource = psicologo.BuscarPorNome(txtPsicologo.Text);
        }

        private void btnNovoPsicologo_Click(object sender, EventArgs e)
        {
            formNovoPsicologo = new FormNovoPsicologo();
            formNovoPsicologo.Show();
        }

        private void btnDetalhesPsicologo_Click(object sender, EventArgs e)
        {
            formDetalhesPsicologo = new FormDetalhesPsicologo();
            formDetalhesPsicologo.Show();
        }

        private void btnPequisarPsicologo_Click(object sender, EventArgs e)
        {
            CarregarLista();
        }
    }
}
