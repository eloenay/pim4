﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Formularios.Psicologos
{
    public partial class FormDetalhesPsicologo : Form
    {
        public FormDetalhesPsicologo()
        {
            InitializeComponent();
            PreencherComboSala();
        }


        #region Crud

        private void btnFinalizarPsicologo_Click(object sender, EventArgs e)
        {
            try
            {
                Psicologo psicologo = new Psicologo();

                if (txtDetalhesNomePsicologo.Text == String.Empty || txtDetalhesNumeroCrpPsicologo.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    if (txtDetalhesCpflPsicologo.Text.Length < 14)
                    {
                        MessageBox.Show("O CPF precisa ser preechido corretamente");
                    }
                    else
                    {
                        psicologo.PsicologoId = Convert.ToInt32(lblIdDetalhesPsicologo.Text);
                        psicologo.Nome = txtDetalhesNomePsicologo.Text;
                        psicologo.DataNascimento = Convert.ToDateTime(txtDataNascimentoPsicologo.Text);
                        psicologo.Cpf = txtDetalhesCpflPsicologo.Text;
                        psicologo.SalaId = Convert.ToInt32(cbxSalaPsicologo.SelectedValue);
                        psicologo.NumeroCrp = Convert.ToInt32(txtDetalhesNumeroCrpPsicologo.Text);
                        psicologo.RegiaoCrp = (Regiao)cbxRegiaoPsicologo.SelectedValue;

                        psicologo.Salvar(psicologo);

                        MessageBox.Show("Psicólogo alterado com sucesso!!!");
                        dgvPsicologo.DataSource = psicologo.BuscarPorNome(txtPsicologo.Text);

                        VisibleText();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnEditarPsicologo_Click(object sender, EventArgs e)
        {
            txtDetalhesNomePsicologo.Enabled = true;
            txtDataNascimentoPsicologo.Enabled = true;
            txtDetalhesCpflPsicologo.Enabled = true;
            cbxSalaPsicologo.Enabled = true;
            txtDetalhesNumeroCrpPsicologo.Enabled = true;
            cbxRegiaoPsicologo.Enabled = true;
            btnNovaSalaPsicologo.Visible = true;
            btnFinalizarPsicologo.Visible = true;
            btnCancelar.Visible = true;
            btnEditarPsicologo.Visible = false;
            btnExcluirPsicologo.Visible = false;
        }

        private void btnExcluirPsicologo_Click(object sender, EventArgs e)
        {
            if (txtDetalhesNomePsicologo.Text == String.Empty)
            {
                MessageBox.Show("Escolha um Psicólogo para essa função", "Informação");
            }
            else
            {
                Psicologo psicologo = new Psicologo();
                psicologo.PsicologoId = Convert.ToInt32(lblIdDetalhesPsicologo.Text);

                DialogResult dr = MessageBox.Show("Deseja mesmo excluir este psicólogo?",
                    "Excluir", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    psicologo.Deletar(psicologo);
                    MessageBox.Show("Psicólogo excluido com sucesso...");
                    dgvPsicologo.DataSource = psicologo.BuscarPorNome(txtPsicologo.Text);
                    LimparTela();
                }
            }
        }

        private void dgvPsicologo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Psicologo psicologo = new Psicologo();

            int id = (int)dgvPsicologo.CurrentRow.Cells["Id"].Value;
            psicologo = psicologo.BuscarPorId(id);

            lblIdDetalhesPsicologo.Text = psicologo.PsicologoId.ToString();
            txtDetalhesNomePsicologo.Text = psicologo.Nome;
            txtDataNascimentoPsicologo.Text = psicologo.DataNascimento.ToString();
            txtDetalhesCpflPsicologo.Text = psicologo.Cpf;
            cbxSalaPsicologo.Text = psicologo.Sala.Descricao;
            txtDetalhesNumeroCrpPsicologo.Text = psicologo.NumeroCrp.ToString();
            cbxRegiaoPsicologo.Text = psicologo.RegiaoCrp.ToString();
        }

        #endregion

        #region Funcionalidades

        public void PreencherComboSala()
        {
            ConexaoBd cnBd = new ConexaoBd();
            var sala = from salas in cnBd.Salas
                       select salas;
            cbxSalaPsicologo.DataSource = sala.ToList();
            cbxSalaPsicologo.ValueMember = "SalaId";
            cbxSalaPsicologo.DisplayMember = "Descricao";

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void VisibleText()
        {
            txtDetalhesNomePsicologo.Enabled = false;
            txtDataNascimentoPsicologo.Enabled = false;
            txtDetalhesCpflPsicologo.Enabled = false;
            cbxSalaPsicologo.Enabled = false;
            txtDetalhesNumeroCrpPsicologo.Enabled = false;
            cbxRegiaoPsicologo.Enabled = false;
            btnNovaSalaPsicologo.Visible = false;
            btnFinalizarPsicologo.Visible = false;
            btnCancelar.Visible = false;
            btnEditarPsicologo.Visible = true;
            btnExcluirPsicologo.Visible = true;
        }

        public void LimparTela()
        {
            lblIdDetalhesPsicologo.Text = String.Empty;
            txtDetalhesNomePsicologo.Text = String.Empty;
            txtDataNascimentoPsicologo.Text = String.Empty;
            txtDetalhesCpflPsicologo.Text = String.Empty;
            cbxSalaPsicologo.Text = String.Empty;
            txtDetalhesNumeroCrpPsicologo.Text = String.Empty;
            cbxRegiaoPsicologo.Text = String.Empty;
        }

        private void btnPequisarPsicologo_Click(object sender, EventArgs e)
        {
            try
            {
                Psicologo psicologo = new Psicologo();
                dgvPsicologo.DataSource = psicologo.BuscarPorNome(txtPsicologo.Text);

                if (dgvPsicologo.DataSource == null)
                {
                    MessageBox.Show("Não há Psicólogo Cadastrado");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            VisibleText();
            LimparTela();
        }

        #endregion

        #region Preencher Combobox com enum

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void FormDetalhesPsicologo_Load(object sender, EventArgs e)
        {
            cbxRegiaoPsicologo.DataSource = Listar(typeof(Regiao));
            cbxRegiaoPsicologo.DisplayMember = "Value";
            cbxRegiaoPsicologo.ValueMember = "Key";
        }


        #endregion

        private void btnNovaSalaPsicologo_Click(object sender, EventArgs e)
        {
            FormSalaPsicologo formSalaPsicologo = new FormSalaPsicologo();
            formSalaPsicologo.Show();
        }
    }
}
