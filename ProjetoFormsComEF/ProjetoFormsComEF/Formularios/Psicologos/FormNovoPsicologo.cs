﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Formularios.Psicologos
{
    public partial class FormNovoPsicologo : Form
    {
        public FormNovoPsicologo()
        {
            InitializeComponent();
        }

        public void PreencherComboSala()
        {
            ConexaoBd cnBd = new ConexaoBd();
            var sala = from salas in cnBd.Salas
                select salas;
            cbxSalaPsicologo.DataSource = sala.ToList();
            cbxSalaPsicologo.ValueMember = "SalaId";
            cbxSalaPsicologo.DisplayMember = "Descricao";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        #region Crud

        private void btnFinalizarPsicologo_Click(object sender, EventArgs e)
        {
            try
            {
                Psicologo psicologo = new Psicologo();
                if (txtNomePsicologo.Text == String.Empty || txtNumeroCrpPsicologo.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    if (txtCpfPsicologo.Text.Length < 14)
                    {
                        MessageBox.Show("O CPF precisa ser preechido corretamente");
                    }
                    else
                    {
                        psicologo.Nome = txtNomePsicologo.Text;
                        psicologo.DataNascimento = Convert.ToDateTime(txtDataNascimentoPsicologo.Text);
                        psicologo.Cpf = txtCpfPsicologo.Text;
                        if (psicologo.VerificarCpf(psicologo) != null)
                        {
                            MessageBox.Show("Este CPF Já está cadastrado");
                        }
                        else
                        {
                            psicologo.SalaId = Convert.ToInt32(cbxSalaPsicologo.SelectedValue);
                            psicologo.NumeroCrp = Convert.ToInt32(txtNumeroCrpPsicologo.Text);
                            psicologo.RegiaoCrp = (Regiao)cbxRegiaoPsicologo.SelectedValue;

                            psicologo.Salvar(psicologo);

                            MessageBox.Show($"Psicológo ({psicologo.Nome}) Salvo com sucesso!!!");
                            Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        #endregion

        #region Preencher Combobox com enum

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void cbxRegiaoPsicologo_Click(object sender, EventArgs e)
        {
            cbxRegiaoPsicologo.DataSource = Listar(typeof(Regiao));
            cbxRegiaoPsicologo.DisplayMember = "value";
            cbxRegiaoPsicologo.ValueMember = "Key";
        }

        #endregion

        private void btnNovaSalaPsicologo_Click(object sender, EventArgs e)
        {
            FormSalaPsicologo formSalaPsicologo = new FormSalaPsicologo();
            formSalaPsicologo.Show();
        }

        private void cbxSalaPsicologo_Click(object sender, EventArgs e)
        {
            PreencherComboSala();
        }
    }
}
