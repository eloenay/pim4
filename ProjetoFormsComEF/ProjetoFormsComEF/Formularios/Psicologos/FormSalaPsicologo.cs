﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Psicologos
{
    public partial class FormSalaPsicologo : Form
    {
        public FormSalaPsicologo()
        {
            InitializeComponent();
            PreencherComboClinica();
        }

        public void PreencherComboClinica()
        {
            ConexaoBd cnBd = new ConexaoBd();
            var cli = from clinica in cnBd.Clinicas
                select clinica;
            cbxClinicaSala.DataSource = cli.ToList();
            cbxClinicaSala.ValueMember = "ClinicaId";
            cbxClinicaSala.DisplayMember = "Nome";
        }

        private void btnFinalizarUsuairo_Click(object sender, EventArgs e)
        {
            try
            {
                Sala sala = new Sala();
                sala.Descricao = txtDescricaoSala.Text;
                if (txtDescricaoSala.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios...", "Informação");
                }
                else
                {
                    if (sala.VerificarSala(sala) != null)
                    {
                        MessageBox.Show("Esta sala já está cadastrada...", "Informação");
                    }
                    else
                    {
                        sala.ClinicaId = (int)cbxClinicaSala.SelectedValue;
                        sala.Salvar(sala);
                        MessageBox.Show("Sala cadastrada com sucesso...");
                        Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
