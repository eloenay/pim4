﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.DAO;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;


namespace ProjetoIntegrador4.Formularios.Pacientes
{
    public partial class FormNovoPaciente : Form
    {
        private Paciente _pacientes;

        public FormNovoPaciente()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbxPlanoDeSaudePaciente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnFinalizarPaciente_Click(object sender, EventArgs e)
        {
            try
            {
                _pacientes = new Paciente();
                if (txtNomePaciente.Text == String.Empty || txtEmailPaciente.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    _pacientes.Nome = txtNomePaciente.Text;
                    _pacientes.DataNascimento = Convert.ToDateTime(txtDataNascimentoPaciente.Text);
                    _pacientes.Sexo = (Sexo)cbxSexoPaciente.SelectedValue;
                    if (txtEmailPaciente.Text.Contains("@"))
                    {
                        _pacientes.Email = txtEmailPaciente.Text;
                        _pacientes.Salvar(_pacientes);

                        MessageBox.Show($"Paciente ({_pacientes.Nome}) Salvo com sucesso!!!");
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Tipo de e-mail invalido");
                    }
                }

            }
            catch (Exception erro)
            {
                Console.WriteLine(erro.Message);
                throw;
            }
        }

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void cbxSexoPaciente_Click(object sender, EventArgs e)
        {
            cbxSexoPaciente.DataSource = Listar(typeof(Sexo));
            cbxSexoPaciente.DisplayMember = "Value";
            cbxSexoPaciente.ValueMember = "Key";
        }
    }
}
