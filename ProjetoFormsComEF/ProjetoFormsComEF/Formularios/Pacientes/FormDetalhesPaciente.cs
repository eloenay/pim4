﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Formularios.Pacientes
{
    public partial class FormDetalhesPaciente : Form
    {
        public FormDetalhesPaciente()
        {
            InitializeComponent();
        }

        #region Crud

        private void btnEditarFinalizarPaciente_Click(object sender, EventArgs e)
        {
            try
            {
                Paciente pacientes = new Paciente();
                if (txtDetalhesNomePaciente.Text == String.Empty || txtDetalhesEmailPaciente.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    pacientes.PacienteId = Convert.ToInt32(lblDetalhesIdPaciente.Text);
                    pacientes.Nome = txtDetalhesNomePaciente.Text;
                    pacientes.DataNascimento = Convert.ToDateTime(txtDetalhesDataNascimentoPaciente.Text);
                    pacientes.Sexo = (Sexo)cbxDetalhesSexoPaciente.SelectedValue;
                    pacientes.Email = txtDetalhesEmailPaciente.Text;
                    if (txtDetalhesEmailPaciente.Text.Contains("@"))
                    {
                        pacientes.Salvar(pacientes);
                        MessageBox.Show($"Paciente ({pacientes.Nome}) Salvo com sucesso!!!");
                        dgvPaciente.DataSource = pacientes.BuscarPorNome(txtPaciente.Text);
                        TxtVisible();
                        LimparCampos();
                    }
                    else
                    {
                        MessageBox.Show("Tipo de e-mail invalido", "Informação");
                    }
                }
            }
            catch (Exception erro)
            {
                Console.WriteLine(erro.Message);
                throw;
            }
        }

        private void btnExcluirPaciente_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDetalhesNomePaciente.Text == String.Empty)
                {
                    MessageBox.Show("Escolha um paciente para essa função", "Informação");
                }
                else
                {
                    DialogResult rs = MessageBox.Show("Deseja mesmo excluir este paciente?", "Excluir", MessageBoxButtons.YesNo);
                    if (rs == DialogResult.Yes)
                    {
                        Paciente paciente = new Paciente();
                        paciente.PacienteId = Convert.ToInt32(lblDetalhesIdPaciente.Text);
                        paciente.Deletar(paciente);
                        MessageBox.Show("Paciente excluido com sucesso!!!");
                        dgvPaciente.DataSource = paciente.BuscarPorNome(txtPaciente.Text);
                        LimparCampos();
                    }
                }
            }
            catch (Exception erro)


            {
                MessageBox.Show(erro.Message);
            }
        }

        private void dgvPaciente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int)dgvPaciente.CurrentRow.Cells["Id"].Value;
            Paciente paciente = new Paciente();
            paciente = paciente.BuscarPorId(id);

            lblDetalhesIdPaciente.Text = paciente.PacienteId.ToString();
            txtDetalhesNomePaciente.Text = paciente.Nome;
            txtDetalhesDataNascimentoPaciente.Text = paciente.DataNascimento.ToString();
            txtDetalhesEmailPaciente.Text = paciente.Email;
            cbxDetalhesSexoPaciente.Text = paciente.Sexo.ToString();
        }

        #endregion

        #region Funcionalidades

        private void btnEditarPaciente_Click(object sender, EventArgs e)
        {
            txtDetalhesNomePaciente.Enabled = true;
            txtDetalhesDataNascimentoPaciente.Enabled = true;
            txtDetalhesEmailPaciente.Enabled = true;
            cbxDetalhesSexoPaciente.Enabled = true;
            btnEditarFinalizarPaciente.Visible = true;
            btnCancelar.Visible = true;
            btnEditarPaciente.Visible = false;
            btnExcluirPaciente.Visible = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPequisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {
               Paciente paciente = new Paciente();
                dgvPaciente.DataSource = paciente.BuscarPorNome(txtPaciente.Text);
                if (dgvPaciente.DataSource == null)
                {
                    MessageBox.Show("Nao possui Pacientes Cadastrados!");
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
                throw;
            }
        }

        public void TxtVisible()
        {
            txtDetalhesNomePaciente.Enabled = false;
            txtDetalhesDataNascimentoPaciente.Enabled = false;
            txtDetalhesEmailPaciente.Enabled = false;
            cbxDetalhesSexoPaciente.Enabled = false;
            btnEditarFinalizarPaciente.Visible = false;
            btnCancelar.Visible = false;
            btnEditarPaciente.Visible = true;
            btnExcluirPaciente.Visible = true;
        }

        public void LimparCampos()
        {
            lblDetalhesIdPaciente.Text = String.Empty;
            txtDetalhesNomePaciente.Text = String.Empty;
            txtDetalhesDataNascimentoPaciente.Text = String.Empty;
            txtDetalhesEmailPaciente.Text = String.Empty;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            TxtVisible();
            LimparCampos();
        }

        #endregion

        #region Preencher Combobox com enum

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void FormDetalhesPaciente_Load(object sender, EventArgs e)
        {
            cbxDetalhesSexoPaciente.DataSource = Listar(typeof(Sexo));
            cbxDetalhesSexoPaciente.DisplayMember = "Value";
            cbxDetalhesSexoPaciente.ValueMember = "Key";
        }

        #endregion
    }
}
