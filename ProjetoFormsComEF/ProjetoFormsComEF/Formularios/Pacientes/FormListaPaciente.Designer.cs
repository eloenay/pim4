﻿namespace ProjetoIntegrador4.Formularios.Pacientes
{
    partial class FormListaPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaPaciente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblIdPacienteLista = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDetalhesPaciente = new System.Windows.Forms.PictureBox();
            this.btnNovoPaciente = new System.Windows.Forms.PictureBox();
            this.btnVoltaPaciente = new System.Windows.Forms.PictureBox();
            this.btnPequisarPaciente = new System.Windows.Forms.PictureBox();
            this.txtPaciente = new System.Windows.Forms.TextBox();
            this.dgvPaciente = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bancoPIM4DataSet = new ProjetoIntegrador4.BancoPIM4DataSet();
            this.pacientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pacientesTableAdapter = new ProjetoIntegrador4.BancoPIM4DataSetTableAdapters.PacientesTableAdapter();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bancoPIM4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pacientesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.lblIdPacienteLista);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnDetalhesPaciente);
            this.groupBox1.Controls.Add(this.btnNovoPaciente);
            this.groupBox1.Controls.Add(this.btnVoltaPaciente);
            this.groupBox1.Controls.Add(this.btnPequisarPaciente);
            this.groupBox1.Controls.Add(this.txtPaciente);
            this.groupBox1.Controls.Add(this.dgvPaciente);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pacientes";
            // 
            // lblIdPacienteLista
            // 
            this.lblIdPacienteLista.AutoSize = true;
            this.lblIdPacienteLista.Font = new System.Drawing.Font("Palatino Linotype", 10F);
            this.lblIdPacienteLista.Location = new System.Drawing.Point(952, 0);
            this.lblIdPacienteLista.Name = "lblIdPacienteLista";
            this.lblIdPacienteLista.Size = new System.Drawing.Size(22, 19);
            this.lblIdPacienteLista.TabIndex = 28;
            this.lblIdPacienteLista.Text = "Id";
            this.lblIdPacienteLista.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 26);
            this.label2.TabIndex = 27;
            this.label2.Text = "Pesquisar:";
            // 
            // btnDetalhesPaciente
            // 
            this.btnDetalhesPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalhesPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalhesPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnDetalhesPaciente.Image")));
            this.btnDetalhesPaciente.Location = new System.Drawing.Point(473, 466);
            this.btnDetalhesPaciente.Name = "btnDetalhesPaciente";
            this.btnDetalhesPaciente.Size = new System.Drawing.Size(73, 59);
            this.btnDetalhesPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetalhesPaciente.TabIndex = 26;
            this.btnDetalhesPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnDetalhesPaciente, "Detalhes do paciente");
            this.btnDetalhesPaciente.Click += new System.EventHandler(this.btnDetalhesPaciente_Click);
            // 
            // btnNovoPaciente
            // 
            this.btnNovoPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovoPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovoPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoPaciente.Image")));
            this.btnNovoPaciente.Location = new System.Drawing.Point(919, 62);
            this.btnNovoPaciente.Name = "btnNovoPaciente";
            this.btnNovoPaciente.Size = new System.Drawing.Size(49, 45);
            this.btnNovoPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovoPaciente.TabIndex = 16;
            this.btnNovoPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovoPaciente, "Novo Paciente");
            this.btnNovoPaciente.Click += new System.EventHandler(this.btnNovoPaciente_Click);
            // 
            // btnVoltaPaciente
            // 
            this.btnVoltaPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltaPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltaPaciente.Image")));
            this.btnVoltaPaciente.Location = new System.Drawing.Point(11, 71);
            this.btnVoltaPaciente.Name = "btnVoltaPaciente";
            this.btnVoltaPaciente.Size = new System.Drawing.Size(48, 36);
            this.btnVoltaPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltaPaciente.TabIndex = 17;
            this.btnVoltaPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnVoltaPaciente, "Voltar");
            this.btnVoltaPaciente.Click += new System.EventHandler(this.btnVoltaPaciente_Click);
            // 
            // btnPequisarPaciente
            // 
            this.btnPequisarPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPequisarPaciente.BackColor = System.Drawing.Color.Transparent;
            this.btnPequisarPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPequisarPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnPequisarPaciente.Image")));
            this.btnPequisarPaciente.Location = new System.Drawing.Point(919, 154);
            this.btnPequisarPaciente.Name = "btnPequisarPaciente";
            this.btnPequisarPaciente.Size = new System.Drawing.Size(50, 48);
            this.btnPequisarPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPequisarPaciente.TabIndex = 24;
            this.btnPequisarPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPequisarPaciente, "Pesquisar Paciente");
            this.btnPequisarPaciente.Click += new System.EventHandler(this.btnPequisarPaciente_Click);
            // 
            // txtPaciente
            // 
            this.txtPaciente.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPaciente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaciente.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaciente.Location = new System.Drawing.Point(112, 169);
            this.txtPaciente.Name = "txtPaciente";
            this.txtPaciente.Size = new System.Drawing.Size(781, 29);
            this.txtPaciente.TabIndex = 22;
            // 
            // dgvPaciente
            // 
            this.dgvPaciente.AllowUserToAddRows = false;
            this.dgvPaciente.AllowUserToDeleteRows = false;
            this.dgvPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPaciente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPaciente.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPaciente.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPaciente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPaciente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPaciente.Location = new System.Drawing.Point(6, 204);
            this.dgvPaciente.Name = "dgvPaciente";
            this.dgvPaciente.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 50, 5, 50);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPaciente.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPaciente.Size = new System.Drawing.Size(962, 256);
            this.dgvPaciente.TabIndex = 21;
            // 
            // bancoPIM4DataSet
            // 
            this.bancoPIM4DataSet.DataSetName = "BancoPIM4DataSet";
            this.bancoPIM4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pacientesBindingSource
            // 
            this.pacientesBindingSource.DataMember = "Pacientes";
            this.pacientesBindingSource.DataSource = this.bancoPIM4DataSet;
            // 
            // pacientesTableAdapter
            // 
            this.pacientesTableAdapter.ClearBeforeFill = true;
            // 
            // FormListaPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(205, 90);
            this.Name = "FormListaPaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "formPacienteLista";
            this.Load += new System.EventHandler(this.FormListaPaciente_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bancoPIM4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pacientesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox btnPequisarPaciente;
        private System.Windows.Forms.TextBox txtPaciente;
        private System.Windows.Forms.DataGridView dgvPaciente;
        private System.Windows.Forms.PictureBox btnDetalhesPaciente;
        private System.Windows.Forms.PictureBox btnNovoPaciente;
        private System.Windows.Forms.PictureBox btnVoltaPaciente;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblIdPacienteLista;
        private BancoPIM4DataSet bancoPIM4DataSet;
        private System.Windows.Forms.BindingSource pacientesBindingSource;
        private BancoPIM4DataSetTableAdapters.PacientesTableAdapter pacientesTableAdapter;
    }
}