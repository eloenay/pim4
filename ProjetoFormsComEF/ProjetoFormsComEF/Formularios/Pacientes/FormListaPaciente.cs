﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Pacientes
{
    public partial class FormListaPaciente : Form
    {
        private FormNovoPaciente formNovoPaciente;
        private FormDetalhesPaciente formDetalhesPaciente;
        private Paciente paciente;


        public FormListaPaciente()
        {
            InitializeComponent();
        }

        private void btnVoltaPaciente_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNovoPaciente_Click(object sender, EventArgs e)
        {
            formNovoPaciente = new FormNovoPaciente();
            formNovoPaciente.Show();
        }

        private void btnDetalhesPaciente_Click(object sender, EventArgs e)
        {
            formDetalhesPaciente = new FormDetalhesPaciente();
            formDetalhesPaciente.Show();
        }

        private void btnPequisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {
                paciente = new Paciente();
                dgvPaciente.DataSource = paciente.BuscarPorNome(txtPaciente.Text);
                if (dgvPaciente.DataSource == null)
                {
                    MessageBox.Show("Nao possui Pacientes Cadastrados!");
                }
            }
            catch (Exception error)
            {
               MessageBox.Show(error.Message);
                throw;
            }

        }

        private void FormListaPaciente_Load(object sender, EventArgs e)
        {
            try
            {
                paciente = new Paciente();
                dgvPaciente.DataSource = paciente.BuscarPorNome(txtPaciente.Text); ;
                if (dgvPaciente.DataSource == null)
                {
                    MessageBox.Show("Nao possui Pacientes Cadastrados!");
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
