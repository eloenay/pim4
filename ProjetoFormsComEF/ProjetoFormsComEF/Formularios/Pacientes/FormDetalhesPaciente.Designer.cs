﻿namespace ProjetoIntegrador4.Formularios.Pacientes
{
    partial class FormDetalhesPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalhesPaciente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPequisarPaciente = new System.Windows.Forms.PictureBox();
            this.txtPaciente = new System.Windows.Forms.TextBox();
            this.dgvPaciente = new System.Windows.Forms.DataGridView();
            this.btnEditarFinalizarPaciente = new System.Windows.Forms.Button();
            this.txtDetalhesDataNascimentoPaciente = new System.Windows.Forms.DateTimePicker();
            this.cbxDetalhesSexoPaciente = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDetalhesIdPaciente = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDetalhesEmailPaciente = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDetalhesNomePaciente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExcluirPaciente = new System.Windows.Forms.PictureBox();
            this.btnEditarPaciente = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnExcluirPaciente);
            this.groupBox1.Controls.Add(this.btnEditarPaciente);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalhes do Paciente";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCancelar);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnPequisarPaciente);
            this.groupBox2.Controls.Add(this.txtPaciente);
            this.groupBox2.Controls.Add(this.dgvPaciente);
            this.groupBox2.Controls.Add(this.btnEditarFinalizarPaciente);
            this.groupBox2.Controls.Add(this.txtDetalhesDataNascimentoPaciente);
            this.groupBox2.Controls.Add(this.cbxDetalhesSexoPaciente);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lblDetalhesIdPaciente);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtDetalhesEmailPaciente);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtDetalhesNomePaciente);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(22, 63);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(928, 369);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Paciente";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(339, 288);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 46);
            this.btnCancelar.TabIndex = 31;
            this.btnCancelar.Text = "Cancelar";
            this.toolTip1.SetToolTip(this.btnCancelar, "Finalizar Editar Paciente");
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(533, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 26);
            this.label4.TabIndex = 10;
            this.label4.Text = "Pesquisar:";
            // 
            // btnPequisarPaciente
            // 
            this.btnPequisarPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPequisarPaciente.BackColor = System.Drawing.Color.Transparent;
            this.btnPequisarPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPequisarPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnPequisarPaciente.Image")));
            this.btnPequisarPaciente.Location = new System.Drawing.Point(890, 20);
            this.btnPequisarPaciente.Name = "btnPequisarPaciente";
            this.btnPequisarPaciente.Size = new System.Drawing.Size(32, 30);
            this.btnPequisarPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPequisarPaciente.TabIndex = 30;
            this.btnPequisarPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPequisarPaciente, "Pesquisar Paciente");
            this.btnPequisarPaciente.Click += new System.EventHandler(this.btnPequisarPaciente_Click);
            // 
            // txtPaciente
            // 
            this.txtPaciente.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPaciente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaciente.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaciente.Location = new System.Drawing.Point(635, 20);
            this.txtPaciente.Name = "txtPaciente";
            this.txtPaciente.Size = new System.Drawing.Size(249, 29);
            this.txtPaciente.TabIndex = 11;
            // 
            // dgvPaciente
            // 
            this.dgvPaciente.AllowUserToAddRows = false;
            this.dgvPaciente.AllowUserToDeleteRows = false;
            this.dgvPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPaciente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPaciente.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPaciente.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPaciente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPaciente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPaciente.Location = new System.Drawing.Point(534, 55);
            this.dgvPaciente.Name = "dgvPaciente";
            this.dgvPaciente.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 50, 5, 50);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPaciente.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPaciente.Size = new System.Drawing.Size(388, 294);
            this.dgvPaciente.TabIndex = 12;
            this.dgvPaciente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPaciente_CellClick);
            // 
            // btnEditarFinalizarPaciente
            // 
            this.btnEditarFinalizarPaciente.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarFinalizarPaciente.Location = new System.Drawing.Point(117, 288);
            this.btnEditarFinalizarPaciente.Name = "btnEditarFinalizarPaciente";
            this.btnEditarFinalizarPaciente.Size = new System.Drawing.Size(110, 46);
            this.btnEditarFinalizarPaciente.TabIndex = 9;
            this.btnEditarFinalizarPaciente.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnEditarFinalizarPaciente, "Finalizar Editar Paciente");
            this.btnEditarFinalizarPaciente.UseVisualStyleBackColor = true;
            this.btnEditarFinalizarPaciente.Visible = false;
            this.btnEditarFinalizarPaciente.Click += new System.EventHandler(this.btnEditarFinalizarPaciente_Click);
            // 
            // txtDetalhesDataNascimentoPaciente
            // 
            this.txtDetalhesDataNascimentoPaciente.Enabled = false;
            this.txtDetalhesDataNascimentoPaciente.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDetalhesDataNascimentoPaciente.Location = new System.Drawing.Point(208, 97);
            this.txtDetalhesDataNascimentoPaciente.Name = "txtDetalhesDataNascimentoPaciente";
            this.txtDetalhesDataNascimentoPaciente.Size = new System.Drawing.Size(155, 33);
            this.txtDetalhesDataNascimentoPaciente.TabIndex = 4;
            // 
            // cbxDetalhesSexoPaciente
            // 
            this.cbxDetalhesSexoPaciente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDetalhesSexoPaciente.Enabled = false;
            this.cbxDetalhesSexoPaciente.FormattingEnabled = true;
            this.cbxDetalhesSexoPaciente.Location = new System.Drawing.Point(117, 208);
            this.cbxDetalhesSexoPaciente.Name = "cbxDetalhesSexoPaciente";
            this.cbxDetalhesSexoPaciente.Size = new System.Drawing.Size(257, 34);
            this.cbxDetalhesSexoPaciente.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 26);
            this.label5.TabIndex = 7;
            this.label5.Text = "Sexo:";
            // 
            // lblDetalhesIdPaciente
            // 
            this.lblDetalhesIdPaciente.AutoSize = true;
            this.lblDetalhesIdPaciente.Location = new System.Drawing.Point(6, 26);
            this.lblDetalhesIdPaciente.Name = "lblDetalhesIdPaciente";
            this.lblDetalhesIdPaciente.Size = new System.Drawing.Size(30, 26);
            this.lblDetalhesIdPaciente.TabIndex = 0;
            this.lblDetalhesIdPaciente.Text = "Id";
            this.lblDetalhesIdPaciente.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 26);
            this.label6.TabIndex = 5;
            this.label6.Text = "Email:";
            // 
            // txtDetalhesEmailPaciente
            // 
            this.txtDetalhesEmailPaciente.Enabled = false;
            this.txtDetalhesEmailPaciente.Location = new System.Drawing.Point(117, 154);
            this.txtDetalhesEmailPaciente.Name = "txtDetalhesEmailPaciente";
            this.txtDetalhesEmailPaciente.Size = new System.Drawing.Size(389, 33);
            this.txtDetalhesEmailPaciente.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 26);
            this.label3.TabIndex = 3;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome:";
            // 
            // txtDetalhesNomePaciente
            // 
            this.txtDetalhesNomePaciente.Enabled = false;
            this.txtDetalhesNomePaciente.Location = new System.Drawing.Point(117, 51);
            this.txtDetalhesNomePaciente.Name = "txtDetalhesNomePaciente";
            this.txtDetalhesNomePaciente.Size = new System.Drawing.Size(389, 33);
            this.txtDetalhesNomePaciente.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(946, -7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.toolTip1.SetToolTip(this.label1, "Fechar");
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnExcluirPaciente
            // 
            this.btnExcluirPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluirPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirPaciente.Image")));
            this.btnExcluirPaciente.Location = new System.Drawing.Point(523, 449);
            this.btnExcluirPaciente.Name = "btnExcluirPaciente";
            this.btnExcluirPaciente.Size = new System.Drawing.Size(69, 59);
            this.btnExcluirPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExcluirPaciente.TabIndex = 27;
            this.btnExcluirPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnExcluirPaciente, "Excluir Paciente");
            this.btnExcluirPaciente.Click += new System.EventHandler(this.btnExcluirPaciente_Click);
            // 
            // btnEditarPaciente
            // 
            this.btnEditarPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarPaciente.Image")));
            this.btnEditarPaciente.Location = new System.Drawing.Point(402, 449);
            this.btnEditarPaciente.Name = "btnEditarPaciente";
            this.btnEditarPaciente.Size = new System.Drawing.Size(69, 59);
            this.btnEditarPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditarPaciente.TabIndex = 26;
            this.btnEditarPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnEditarPaciente, "Editar Paciente");
            this.btnEditarPaciente.Click += new System.EventHandler(this.btnEditarPaciente_Click);
            // 
            // FormDetalhesPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormDetalhesPaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormDetalhesPaciente";
            this.Load += new System.EventHandler(this.FormDetalhesPaciente_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarPaciente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox btnExcluirPaciente;
        private System.Windows.Forms.PictureBox btnEditarPaciente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDetalhesIdPaciente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDetalhesEmailPaciente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDetalhesNomePaciente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxDetalhesSexoPaciente;
        private System.Windows.Forms.DateTimePicker txtDetalhesDataNascimentoPaciente;
        private System.Windows.Forms.Button btnEditarFinalizarPaciente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox btnPequisarPaciente;
        private System.Windows.Forms.TextBox txtPaciente;
        private System.Windows.Forms.DataGridView dgvPaciente;
        private System.Windows.Forms.Button btnCancelar;
    }
}