﻿namespace ProjetoIntegrador4.Formularios.Login
{
    partial class FormEditarLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFecharEditar = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnEditarFinalizarUsuairo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEditarConfirmarSenhaUsuario = new System.Windows.Forms.TextBox();
            this.txtEditarLoginUsuario = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEditarSenhaUsuario = new System.Windows.Forms.TextBox();
            this.cbxEditarTipoLoginUsuario = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEditarNomeUsuario = new System.Windows.Forms.TextBox();
            this.lblIdEditarUsuario = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.btnEditarFinalizarUsuairo);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFecharEditar);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Editar Usuário";
            // 
            // btnFecharEditar
            // 
            this.btnFecharEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFecharEditar.AutoSize = true;
            this.btnFecharEditar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFecharEditar.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFecharEditar.Location = new System.Drawing.Point(946, -7);
            this.btnFecharEditar.Name = "btnFecharEditar";
            this.btnFecharEditar.Size = new System.Drawing.Size(25, 26);
            this.btnFecharEditar.TabIndex = 29;
            this.btnFecharEditar.Text = "X";
            this.toolTip1.SetToolTip(this.btnFecharEditar, "Fechar");
            this.btnFecharEditar.Click += new System.EventHandler(this.btnFecharEditar_Click);
            // 
            // btnEditarFinalizarUsuairo
            // 
            this.btnEditarFinalizarUsuairo.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarFinalizarUsuairo.Location = new System.Drawing.Point(412, 455);
            this.btnEditarFinalizarUsuairo.Name = "btnEditarFinalizarUsuairo";
            this.btnEditarFinalizarUsuairo.Size = new System.Drawing.Size(142, 64);
            this.btnEditarFinalizarUsuairo.TabIndex = 31;
            this.btnEditarFinalizarUsuairo.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnEditarFinalizarUsuairo, "Finalizar Editar Usuário");
            this.btnEditarFinalizarUsuairo.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblIdEditarUsuario);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtEditarConfirmarSenhaUsuario);
            this.groupBox2.Controls.Add(this.txtEditarLoginUsuario);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtEditarSenhaUsuario);
            this.groupBox2.Controls.Add(this.cbxEditarTipoLoginUsuario);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtEditarNomeUsuario);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(75, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(822, 332);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Usuário";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Confirmar Senha:";
            // 
            // txtEditarConfirmarSenhaUsuario
            // 
            this.txtEditarConfirmarSenhaUsuario.Location = new System.Drawing.Point(212, 213);
            this.txtEditarConfirmarSenhaUsuario.Name = "txtEditarConfirmarSenhaUsuario";
            this.txtEditarConfirmarSenhaUsuario.Size = new System.Drawing.Size(442, 33);
            this.txtEditarConfirmarSenhaUsuario.TabIndex = 7;
            // 
            // txtEditarLoginUsuario
            // 
            this.txtEditarLoginUsuario.Location = new System.Drawing.Point(130, 106);
            this.txtEditarLoginUsuario.Name = "txtEditarLoginUsuario";
            this.txtEditarLoginUsuario.Size = new System.Drawing.Size(524, 33);
            this.txtEditarLoginUsuario.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 26);
            this.label6.TabIndex = 4;
            this.label6.Text = "Senha:";
            // 
            // txtEditarSenhaUsuario
            // 
            this.txtEditarSenhaUsuario.Location = new System.Drawing.Point(117, 160);
            this.txtEditarSenhaUsuario.Name = "txtEditarSenhaUsuario";
            this.txtEditarSenhaUsuario.Size = new System.Drawing.Size(537, 33);
            this.txtEditarSenhaUsuario.TabIndex = 5;
            // 
            // cbxEditarTipoLoginUsuario
            // 
            this.cbxEditarTipoLoginUsuario.FormattingEnabled = true;
            this.cbxEditarTipoLoginUsuario.Location = new System.Drawing.Point(212, 265);
            this.cbxEditarTipoLoginUsuario.Name = "cbxEditarTipoLoginUsuario";
            this.cbxEditarTipoLoginUsuario.Size = new System.Drawing.Size(282, 34);
            this.cbxEditarTipoLoginUsuario.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 268);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Tipo de Usuário:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Usuário:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome:";
            // 
            // txtEditarNomeUsuario
            // 
            this.txtEditarNomeUsuario.Location = new System.Drawing.Point(117, 54);
            this.txtEditarNomeUsuario.Name = "txtEditarNomeUsuario";
            this.txtEditarNomeUsuario.Size = new System.Drawing.Size(537, 33);
            this.txtEditarNomeUsuario.TabIndex = 1;
            // 
            // lblIdEditarUsuario
            // 
            this.lblIdEditarUsuario.AutoSize = true;
            this.lblIdEditarUsuario.Location = new System.Drawing.Point(786, 17);
            this.lblIdEditarUsuario.Name = "lblIdEditarUsuario";
            this.lblIdEditarUsuario.Size = new System.Drawing.Size(30, 26);
            this.lblIdEditarUsuario.TabIndex = 10;
            this.lblIdEditarUsuario.Text = "Id";
            this.lblIdEditarUsuario.Visible = false;
            // 
            // FormEditarLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormEditarLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormEditarLogin";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label btnFecharEditar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnEditarFinalizarUsuairo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEditarConfirmarSenhaUsuario;
        private System.Windows.Forms.TextBox txtEditarLoginUsuario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEditarSenhaUsuario;
        private System.Windows.Forms.ComboBox cbxEditarTipoLoginUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEditarNomeUsuario;
        private System.Windows.Forms.Label lblIdEditarUsuario;
    }
}