﻿using System;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.FormsAdicionais;

namespace ProjetoIntegrador4.Formularios.Login
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnLoginEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();
                if (txtLoginUsuario.Text == String.Empty || txtLoginSenha.Text == String.Empty)
                {
                    MessageBox.Show("Preencha todos os campos!!!");
                }
                else
                {
                    usuario.Login = txtLoginUsuario.Text;
                    usuario.Senha = txtLoginSenha.Text;
                    if (usuario.BuscarLoginSenha(usuario) != null)
                    {
                        usuario = usuario.BuscarLoginSenha(usuario);
                        FormPrincipal FormPrincipal = new FormPrincipal(usuario);
                        FormPrincipal.Show();
                        
                    }
                    else
                    {
                        MessageBox.Show("Login ou senha incorretos!!!");
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnLoginSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
