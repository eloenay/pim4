﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Formularios.Login
{
    public partial class FormDetalhesLogin : Form
    {
        public FormDetalhesLogin()
        {
            InitializeComponent();
        }

        #region Funcionalidades

        private void btnFecharDetalhes_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPesquisarLogin_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            dgvLogin.DataSource = usuario.BuscarTodos(txtBuscaLogin.Text);
        }

        private void dgvLogin_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Usuario usuario = new Usuario();
            int id = (int)dgvLogin.CurrentRow.Cells["Id"].Value;
            usuario = usuario.BuscarPorId(id);

            lblIdDetalhesLogin.Text = usuario.UsuarioId.ToString();
            txtNomeUsuario.Text = usuario.Nome;
            txtLoginUsuario.Text = usuario.Login;
            txtSenhaUsuario.Text = usuario.ConfirmarSenha;
            txtConfirmarSenhaUsuario.Text = usuario.ConfirmarSenha;
            cbxTipoLoginUsuario.Text = usuario.TipoUsuario.ToString();
        }

        public void LimparCampos()
        {
            lblIdDetalhesLogin.Text = String.Empty;
            txtNomeUsuario.Text = String.Empty;
            txtLoginUsuario.Text = String.Empty;
            txtSenhaUsuario.Text = String.Empty;
            txtConfirmarSenhaUsuario.Text = String.Empty;
            cbxTipoLoginUsuario.Text = String.Empty;
        }

        public void VisibleTxt()
        {
            txtNomeUsuario.Enabled = false;
            txtLoginUsuario.Enabled = false;
            txtSenhaUsuario.Enabled = false;
            txtConfirmarSenhaUsuario.Enabled = false;
            cbxTipoLoginUsuario.Enabled = false;
            btnCancelar.Visible = false;
            btnFinalizar.Visible = false;
            btnEditarUsuario.Visible = true;
            btnExcluirUsuario.Visible = true;
        }

        #endregion

        #region Crud

        private void btnEditarUsuario_Click(object sender, EventArgs e)
        {
            txtNomeUsuario.Enabled = true;
            txtLoginUsuario.Enabled = true;
            txtSenhaUsuario.Enabled = true;
            txtConfirmarSenhaUsuario.Enabled = true;
            cbxTipoLoginUsuario.Enabled = true;
            btnCancelar.Visible = true;
            btnFinalizar.Visible = true;
            btnEditarUsuario.Visible = false;
            btnExcluirUsuario.Visible = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            VisibleTxt();
            LimparCampos();
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();

                if (txtNomeUsuario.Text == String.Empty || txtLoginUsuario.Text == String.Empty ||
                    txtSenhaUsuario.Text == String.Empty || txtConfirmarSenhaUsuario.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    usuario.UsuarioId = Convert.ToInt32(lblIdDetalhesLogin.Text);
                    usuario.Senha = txtSenhaUsuario.Text;
                    usuario.Nome = txtNomeUsuario.Text;
                    usuario.Login = txtLoginUsuario.Text;
                    if (usuario.BuscarLoginExistente(usuario) != null)
                    {
                        MessageBox.Show("Este usuário já está cadastrado");
                        txtLoginUsuario.Text = String.Empty;
                    }
                    else
                    {
                        if (txtConfirmarSenhaUsuario.Text != txtSenhaUsuario.Text)
                        {
                            MessageBox.Show("As senhas estão diferentes");
                            txtConfirmarSenhaUsuario.Text = String.Empty;
                        }
                        else
                        {
                            usuario.ConfirmarSenha = txtConfirmarSenhaUsuario.Text;
                            usuario.TipoUsuario = (TipoUsuario)cbxTipoLoginUsuario.SelectedValue;

                            usuario.Salvar(usuario);
                            MessageBox.Show("Usuário Atualizado com sucesso!!!");

                            dgvLogin.DataSource = usuario.BuscarTodos(txtBuscaLogin.Text);
                            VisibleTxt();
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnExcluirUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNomeUsuario.Text == String.Empty)
                {
                    MessageBox.Show("Escolha um Usuário para essa função", "Informação");
                }
                else
                {
                    Usuario usuario = new Usuario();
                    usuario.UsuarioId = Convert.ToInt32(lblIdDetalhesLogin.Text);

                    DialogResult dr = MessageBox.Show("Deseja mesmo excluir o usuário?",
                        "Excluir", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        usuario.Deletar(usuario);
                        MessageBox.Show("Usuário Excluido com Sucesso...");
                        dgvLogin.DataSource = usuario.BuscarTodos(txtBuscaLogin.Text);
                        LimparCampos();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }
        #endregion

        #region Preencher Combobox com enum

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void FormDetalhesLogin_Load(object sender, EventArgs e)
        {
            cbxTipoLoginUsuario.DataSource = Listar(typeof(TipoUsuario));
            cbxTipoLoginUsuario.DisplayMember = "value";
            cbxTipoLoginUsuario.ValueMember = "Key";
        }

        #endregion


    }
}
