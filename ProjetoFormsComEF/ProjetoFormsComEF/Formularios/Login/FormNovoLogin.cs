﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Formularios.Login
{
    public partial class FormNovoLogin : Form
    {
        public FormNovoLogin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnFinalizarUsuairo_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();

                if (txtNomeUsuario.Text == String.Empty || txtLoginUsuario.Text == String.Empty ||
                    txtSenhaUsuario.Text == String.Empty || txtConfirmarSenhaUsuario.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    usuario.Senha = txtSenhaUsuario.Text;
                    usuario.Nome = txtNomeUsuario.Text;
                    usuario.Login = txtLoginUsuario.Text;
                    if (usuario.BuscarLoginExistente(usuario) != null)
                    {
                        MessageBox.Show("Este usuário já está cadastrado");
                        txtLoginUsuario.Text = String.Empty;
                    }
                    else
                    {
                        if (txtConfirmarSenhaUsuario.Text != txtSenhaUsuario.Text)
                        {
                            MessageBox.Show("As senhas estão diferentes");
                            txtConfirmarSenhaUsuario.Text = String.Empty;
                        }
                        else
                        {
                            usuario.ConfirmarSenha = txtConfirmarSenhaUsuario.Text;
                            usuario.TipoUsuario = (TipoUsuario)cbxTipoLoginUsuario.SelectedValue;

                            usuario.Salvar(usuario);
                            MessageBox.Show($"Usuário ({usuario.Nome}) salvo com sucesso!!!");

                            Close();
                        }
                    }
                }        
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        #region Preencher Combobox com enum

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void cbxTipoLoginUsuario_Click(object sender, EventArgs e)
        {
            cbxTipoLoginUsuario.DataSource = Listar(typeof(TipoUsuario));
            cbxTipoLoginUsuario.DisplayMember = "Value";
            cbxTipoLoginUsuario.ValueMember = "Key";
        }
        #endregion


    }
}

