﻿namespace ProjetoIntegrador4.Formularios.Login
{
    partial class FormDetalhesLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalhesLogin));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtConfirmarSenhaUsuario = new System.Windows.Forms.TextBox();
            this.txtLoginUsuario = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSenhaUsuario = new System.Windows.Forms.TextBox();
            this.cbxTipoLoginUsuario = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomeUsuario = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPesquisarLogin = new System.Windows.Forms.PictureBox();
            this.txtBuscaLogin = new System.Windows.Forms.TextBox();
            this.dgvLogin = new System.Windows.Forms.DataGridView();
            this.lblIdDetalhesLogin = new System.Windows.Forms.Label();
            this.btnFecharDetalhes = new System.Windows.Forms.Label();
            this.btnExcluirUsuario = new System.Windows.Forms.PictureBox();
            this.btnEditarUsuario = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarUsuario)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFecharDetalhes);
            this.groupBox1.Controls.Add(this.btnExcluirUsuario);
            this.groupBox1.Controls.Add(this.btnEditarUsuario);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalhes do Login";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtConfirmarSenhaUsuario);
            this.groupBox2.Controls.Add(this.txtLoginUsuario);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtSenhaUsuario);
            this.groupBox2.Controls.Add(this.cbxTipoLoginUsuario);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNomeUsuario);
            this.groupBox2.Controls.Add(this.btnCancelar);
            this.groupBox2.Controls.Add(this.btnFinalizar);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnPesquisarLogin);
            this.groupBox2.Controls.Add(this.txtBuscaLogin);
            this.groupBox2.Controls.Add(this.dgvLogin);
            this.groupBox2.Controls.Add(this.lblIdDetalhesLogin);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(10, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(954, 410);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Login";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label4.Location = new System.Drawing.Point(13, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 22);
            this.label4.TabIndex = 7;
            this.label4.Text = "Confirmar Senha:";
            // 
            // txtConfirmarSenhaUsuario
            // 
            this.txtConfirmarSenhaUsuario.Enabled = false;
            this.txtConfirmarSenhaUsuario.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtConfirmarSenhaUsuario.Location = new System.Drawing.Point(183, 224);
            this.txtConfirmarSenhaUsuario.Name = "txtConfirmarSenhaUsuario";
            this.txtConfirmarSenhaUsuario.Size = new System.Drawing.Size(442, 29);
            this.txtConfirmarSenhaUsuario.TabIndex = 8;
            this.txtConfirmarSenhaUsuario.UseSystemPasswordChar = true;
            // 
            // txtLoginUsuario
            // 
            this.txtLoginUsuario.Enabled = false;
            this.txtLoginUsuario.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtLoginUsuario.Location = new System.Drawing.Point(101, 117);
            this.txtLoginUsuario.Name = "txtLoginUsuario";
            this.txtLoginUsuario.Size = new System.Drawing.Size(524, 29);
            this.txtLoginUsuario.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label6.Location = new System.Drawing.Point(13, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 22);
            this.label6.TabIndex = 5;
            this.label6.Text = "Senha:";
            // 
            // txtSenhaUsuario
            // 
            this.txtSenhaUsuario.Enabled = false;
            this.txtSenhaUsuario.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtSenhaUsuario.Location = new System.Drawing.Point(88, 171);
            this.txtSenhaUsuario.Name = "txtSenhaUsuario";
            this.txtSenhaUsuario.Size = new System.Drawing.Size(537, 29);
            this.txtSenhaUsuario.TabIndex = 6;
            this.txtSenhaUsuario.UseSystemPasswordChar = true;
            // 
            // cbxTipoLoginUsuario
            // 
            this.cbxTipoLoginUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoLoginUsuario.Enabled = false;
            this.cbxTipoLoginUsuario.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.cbxTipoLoginUsuario.FormattingEnabled = true;
            this.cbxTipoLoginUsuario.Location = new System.Drawing.Point(183, 276);
            this.cbxTipoLoginUsuario.Name = "cbxTipoLoginUsuario";
            this.cbxTipoLoginUsuario.Size = new System.Drawing.Size(282, 30);
            this.cbxTipoLoginUsuario.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label5.Location = new System.Drawing.Point(13, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 22);
            this.label5.TabIndex = 9;
            this.label5.Text = "Tipo de Usuário:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label3.Location = new System.Drawing.Point(13, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "Usuário:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label2.Location = new System.Drawing.Point(13, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome:";
            // 
            // txtNomeUsuario
            // 
            this.txtNomeUsuario.Enabled = false;
            this.txtNomeUsuario.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtNomeUsuario.Location = new System.Drawing.Point(88, 65);
            this.txtNomeUsuario.Name = "txtNomeUsuario";
            this.txtNomeUsuario.Size = new System.Drawing.Size(537, 29);
            this.txtNomeUsuario.TabIndex = 2;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(380, 343);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(122, 44);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizar.Location = new System.Drawing.Point(137, 343);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(122, 44);
            this.btnFinalizar.TabIndex = 11;
            this.btnFinalizar.Text = "Finalizar";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Visible = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(642, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 26);
            this.label1.TabIndex = 13;
            this.label1.Text = "Pesquisar:";
            // 
            // btnPesquisarLogin
            // 
            this.btnPesquisarLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisarLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisarLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarLogin.Image")));
            this.btnPesquisarLogin.Location = new System.Drawing.Point(920, 20);
            this.btnPesquisarLogin.Name = "btnPesquisarLogin";
            this.btnPesquisarLogin.Size = new System.Drawing.Size(28, 29);
            this.btnPesquisarLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPesquisarLogin.TabIndex = 28;
            this.btnPesquisarLogin.TabStop = false;
            this.btnPesquisarLogin.Click += new System.EventHandler(this.btnPesquisarLogin_Click);
            // 
            // txtBuscaLogin
            // 
            this.txtBuscaLogin.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscaLogin.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscaLogin.Location = new System.Drawing.Point(752, 20);
            this.txtBuscaLogin.Name = "txtBuscaLogin";
            this.txtBuscaLogin.Size = new System.Drawing.Size(162, 29);
            this.txtBuscaLogin.TabIndex = 14;
            // 
            // dgvLogin
            // 
            this.dgvLogin.AllowUserToAddRows = false;
            this.dgvLogin.AllowUserToDeleteRows = false;
            this.dgvLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLogin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvLogin.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLogin.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLogin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLogin.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLogin.Location = new System.Drawing.Point(647, 55);
            this.dgvLogin.Name = "dgvLogin";
            this.dgvLogin.ReadOnly = true;
            this.dgvLogin.Size = new System.Drawing.Size(301, 349);
            this.dgvLogin.TabIndex = 15;
            this.dgvLogin.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLogin_CellClick);
            // 
            // lblIdDetalhesLogin
            // 
            this.lblIdDetalhesLogin.AutoSize = true;
            this.lblIdDetalhesLogin.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.lblIdDetalhesLogin.Location = new System.Drawing.Point(6, 29);
            this.lblIdDetalhesLogin.Name = "lblIdDetalhesLogin";
            this.lblIdDetalhesLogin.Size = new System.Drawing.Size(24, 22);
            this.lblIdDetalhesLogin.TabIndex = 0;
            this.lblIdDetalhesLogin.Text = "Id";
            this.lblIdDetalhesLogin.Visible = false;
            // 
            // btnFecharDetalhes
            // 
            this.btnFecharDetalhes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFecharDetalhes.AutoSize = true;
            this.btnFecharDetalhes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnFecharDetalhes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFecharDetalhes.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFecharDetalhes.Location = new System.Drawing.Point(946, -7);
            this.btnFecharDetalhes.Name = "btnFecharDetalhes";
            this.btnFecharDetalhes.Size = new System.Drawing.Size(25, 26);
            this.btnFecharDetalhes.TabIndex = 0;
            this.btnFecharDetalhes.Text = "X";
            this.btnFecharDetalhes.Click += new System.EventHandler(this.btnFecharDetalhes_Click);
            // 
            // btnExcluirUsuario
            // 
            this.btnExcluirUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluirUsuario.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirUsuario.Image")));
            this.btnExcluirUsuario.Location = new System.Drawing.Point(523, 466);
            this.btnExcluirUsuario.Name = "btnExcluirUsuario";
            this.btnExcluirUsuario.Size = new System.Drawing.Size(69, 59);
            this.btnExcluirUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExcluirUsuario.TabIndex = 27;
            this.btnExcluirUsuario.TabStop = false;
            this.btnExcluirUsuario.Click += new System.EventHandler(this.btnExcluirUsuario_Click);
            // 
            // btnEditarUsuario
            // 
            this.btnEditarUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarUsuario.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarUsuario.Image")));
            this.btnEditarUsuario.Location = new System.Drawing.Point(402, 466);
            this.btnEditarUsuario.Name = "btnEditarUsuario";
            this.btnEditarUsuario.Size = new System.Drawing.Size(69, 59);
            this.btnEditarUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditarUsuario.TabIndex = 26;
            this.btnEditarUsuario.TabStop = false;
            this.btnEditarUsuario.Click += new System.EventHandler(this.btnEditarUsuario_Click);
            // 
            // FormDetalhesLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormDetalhesLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormDetalhesLogin";
            this.Load += new System.EventHandler(this.FormDetalhesLogin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarUsuario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btnPesquisarLogin;
        private System.Windows.Forms.TextBox txtBuscaLogin;
        private System.Windows.Forms.DataGridView dgvLogin;
        private System.Windows.Forms.Label lblIdDetalhesLogin;
        private System.Windows.Forms.Label btnFecharDetalhes;
        private System.Windows.Forms.PictureBox btnExcluirUsuario;
        private System.Windows.Forms.PictureBox btnEditarUsuario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtConfirmarSenhaUsuario;
        private System.Windows.Forms.TextBox txtLoginUsuario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSenhaUsuario;
        private System.Windows.Forms.ComboBox cbxTipoLoginUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomeUsuario;
    }
}