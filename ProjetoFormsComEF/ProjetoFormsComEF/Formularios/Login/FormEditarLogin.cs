﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoIntegrador4.Formularios.Login
{
    public partial class FormEditarLogin : Form
    {
        public FormEditarLogin()
        {
            InitializeComponent();
        }

        private void btnFecharEditar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
