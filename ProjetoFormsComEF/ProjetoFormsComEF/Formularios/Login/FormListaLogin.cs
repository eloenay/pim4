﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Login
{
    public partial class FormListaLogin : Form
    {
        private FormNovoLogin formNovoLogin;

        public FormListaLogin()
        {
            InitializeComponent();
        }

        private void btnVoltaUsuario_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNovoUsuario_Click(object sender, EventArgs e)
        {
            formNovoLogin = new FormNovoLogin();
            formNovoLogin.Show();
        }

        private void btnPesquisarUsuario_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            dgvUsuarios.DataSource = usuario.BuscarPorNomes(txtBuscaUsuario.Text);
        }

        private void FormListaLogin_Load(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            dgvUsuarios.DataSource = usuario.BuscarPorNomes(txtBuscaUsuario.Text);
        }

        public void AtualizarLista()
        {
            Usuario usuario = new Usuario();
            dgvUsuarios.DataSource = usuario.BuscarPorNomes(txtBuscaUsuario.Text);
        }

        private void btnDetalhesLogin_Click(object sender, EventArgs e)
        {
            FormDetalhesLogin formDetalhesLogin = new FormDetalhesLogin();
            formDetalhesLogin.Show();
        }
    }
}
