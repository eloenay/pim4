﻿namespace ProjetoIntegrador4.Formularios.Login
{
    partial class FormListaLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaLogin));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDetalhesLogin = new System.Windows.Forms.PictureBox();
            this.btnNovoUsuario = new System.Windows.Forms.PictureBox();
            this.btnVoltaUsuario = new System.Windows.Forms.PictureBox();
            this.btnPesquisarUsuario = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBuscaUsuario = new System.Windows.Forms.TextBox();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.btnDetalhesLogin);
            this.groupBox1.Controls.Add(this.btnNovoUsuario);
            this.groupBox1.Controls.Add(this.btnVoltaUsuario);
            this.groupBox1.Controls.Add(this.btnPesquisarUsuario);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBuscaUsuario);
            this.groupBox1.Controls.Add(this.dgvUsuarios);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Usuários";
            // 
            // btnDetalhesLogin
            // 
            this.btnDetalhesLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalhesLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalhesLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnDetalhesLogin.Image")));
            this.btnDetalhesLogin.Location = new System.Drawing.Point(447, 466);
            this.btnDetalhesLogin.Name = "btnDetalhesLogin";
            this.btnDetalhesLogin.Size = new System.Drawing.Size(73, 59);
            this.btnDetalhesLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetalhesLogin.TabIndex = 27;
            this.btnDetalhesLogin.TabStop = false;
            this.toolTip1.SetToolTip(this.btnDetalhesLogin, "Detalhes da Clinica");
            this.btnDetalhesLogin.Click += new System.EventHandler(this.btnDetalhesLogin_Click);
            // 
            // btnNovoUsuario
            // 
            this.btnNovoUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovoUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovoUsuario.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoUsuario.Image")));
            this.btnNovoUsuario.Location = new System.Drawing.Point(919, 62);
            this.btnNovoUsuario.Name = "btnNovoUsuario";
            this.btnNovoUsuario.Size = new System.Drawing.Size(49, 45);
            this.btnNovoUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovoUsuario.TabIndex = 16;
            this.btnNovoUsuario.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovoUsuario, "Novo Usuário");
            this.btnNovoUsuario.Click += new System.EventHandler(this.btnNovoUsuario_Click);
            // 
            // btnVoltaUsuario
            // 
            this.btnVoltaUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltaUsuario.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltaUsuario.Image")));
            this.btnVoltaUsuario.Location = new System.Drawing.Point(11, 71);
            this.btnVoltaUsuario.Name = "btnVoltaUsuario";
            this.btnVoltaUsuario.Size = new System.Drawing.Size(48, 36);
            this.btnVoltaUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltaUsuario.TabIndex = 17;
            this.btnVoltaUsuario.TabStop = false;
            this.toolTip1.SetToolTip(this.btnVoltaUsuario, "Voltar");
            this.btnVoltaUsuario.Click += new System.EventHandler(this.btnVoltaUsuario_Click);
            // 
            // btnPesquisarUsuario
            // 
            this.btnPesquisarUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisarUsuario.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisarUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarUsuario.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarUsuario.Image")));
            this.btnPesquisarUsuario.Location = new System.Drawing.Point(919, 154);
            this.btnPesquisarUsuario.Name = "btnPesquisarUsuario";
            this.btnPesquisarUsuario.Size = new System.Drawing.Size(50, 48);
            this.btnPesquisarUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPesquisarUsuario.TabIndex = 24;
            this.btnPesquisarUsuario.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPesquisarUsuario, "Pesquisar Usuário");
            this.btnPesquisarUsuario.Click += new System.EventHandler(this.btnPesquisarUsuario_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 26);
            this.label2.TabIndex = 23;
            this.label2.Text = "Pesquisar:";
            // 
            // txtBuscaUsuario
            // 
            this.txtBuscaUsuario.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscaUsuario.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscaUsuario.Location = new System.Drawing.Point(112, 169);
            this.txtBuscaUsuario.Name = "txtBuscaUsuario";
            this.txtBuscaUsuario.Size = new System.Drawing.Size(781, 29);
            this.txtBuscaUsuario.TabIndex = 22;
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvUsuarios.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUsuarios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvUsuarios.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvUsuarios.Location = new System.Drawing.Point(6, 204);
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.Size = new System.Drawing.Size(962, 256);
            this.dgvUsuarios.TabIndex = 21;
            // 
            // FormListaLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(205, 90);
            this.Name = "FormListaLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormListaLogin";
            this.Load += new System.EventHandler(this.FormListaLogin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox btnNovoUsuario;
        private System.Windows.Forms.PictureBox btnVoltaUsuario;
        private System.Windows.Forms.PictureBox btnPesquisarUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBuscaUsuario;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox btnDetalhesLogin;
    }
}