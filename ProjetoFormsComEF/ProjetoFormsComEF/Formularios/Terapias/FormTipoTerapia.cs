﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Terapias
{
    public partial class FormTipoTerapia : Form
    {
        public FormTipoTerapia()
        {
            InitializeComponent();
        }

        private void btnFinalizarTipoTerapia_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDescricaoTipoTerapia.Text == String.Empty)
                {
                    MessageBox.Show("O campo descrição não pode estar vazio", "Informação");
                }
                else
                {
                    TipoTerapia tipoTerapia = new TipoTerapia();
                    tipoTerapia.Descricao = txtDescricaoTipoTerapia.Text;

                    tipoTerapia.Salvar(tipoTerapia);

                    MessageBox.Show("Tipo de terapia salvo com sucesso", "Informação");
                    Close();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
