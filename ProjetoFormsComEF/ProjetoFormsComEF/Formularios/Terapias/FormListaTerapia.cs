﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Terapias
{
    public partial class FormListaAtendimento : Form
    {
        private FormNovaTerapia formNovoAtendimento;
        private FormDetalhesAtendimento formDetalhesAtendimento;

        public FormListaAtendimento()
        {
            InitializeComponent();
            CarregarDataGrid();
        }

        public void CarregarDataGrid()
        {
            Terapia terapia = new Terapia();
            dgvTerapia.DataSource = terapia.BuscarTodos(txtBuscaTerapia.Text);
        }

        private void btnVoltaAtendimento_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDetalhesAtendimento_Click(object sender, EventArgs e)
        {
            formDetalhesAtendimento = new FormDetalhesAtendimento();
            formDetalhesAtendimento.Show();
        }

        private void btnNovoAtendimento_Click(object sender, EventArgs e)
        {
            formNovoAtendimento = new FormNovaTerapia();
            formNovoAtendimento.Show();
        }

        private void btnPesquisarTerapia_Click(object sender, EventArgs e)
        {
            CarregarDataGrid();
        }
    }
}
