﻿namespace ProjetoIntegrador4.Formularios.Terapias
{
    partial class FormDetalhesAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalhesAtendimento));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPequisarTerapia = new System.Windows.Forms.PictureBox();
            this.txtBuscarTerapia = new System.Windows.Forms.TextBox();
            this.dgvBuscarTerapia = new System.Windows.Forms.DataGridView();
            this.Consulta = new System.Windows.Forms.GroupBox();
            this.lblIdTerapia = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnFinalizarTerapia = new System.Windows.Forms.Button();
            this.cbxTerapiaHorario = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDataTerapia = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxTipoTerapia = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTerapiaValor = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblIdConsulta = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNumeroCrpTerapia = new System.Windows.Forms.TextBox();
            this.txtNomePsicologoTerapia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTerapiaDataNascimento = new System.Windows.Forms.TextBox();
            this.txtTerapiaNomePaciente = new System.Windows.Forms.TextBox();
            this.btnFecharDetalhesPaciente = new System.Windows.Forms.Label();
            this.btnExcluirTerapia = new System.Windows.Forms.PictureBox();
            this.btnEditarTerapia = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBuscarTerapia)).BeginInit();
            this.Consulta.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarTerapia)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPequisarTerapia);
            this.groupBox1.Controls.Add(this.txtBuscarTerapia);
            this.groupBox1.Controls.Add(this.dgvBuscarTerapia);
            this.groupBox1.Controls.Add(this.Consulta);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFecharDetalhesPaciente);
            this.groupBox1.Controls.Add(this.btnExcluirTerapia);
            this.groupBox1.Controls.Add(this.btnEditarTerapia);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalhes do Atendimento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(625, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 22);
            this.label1.TabIndex = 40;
            this.label1.Text = "Pesquisar:";
            // 
            // btnPequisarTerapia
            // 
            this.btnPequisarTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPequisarTerapia.BackColor = System.Drawing.Color.Transparent;
            this.btnPequisarTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPequisarTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnPequisarTerapia.Image")));
            this.btnPequisarTerapia.Location = new System.Drawing.Point(927, 59);
            this.btnPequisarTerapia.Name = "btnPequisarTerapia";
            this.btnPequisarTerapia.Size = new System.Drawing.Size(37, 30);
            this.btnPequisarTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPequisarTerapia.TabIndex = 43;
            this.btnPequisarTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPequisarTerapia, "Pesquisar Psicólogo");
            this.btnPequisarTerapia.Click += new System.EventHandler(this.btnPequisarPsicologo_Click);
            // 
            // txtBuscarTerapia
            // 
            this.txtBuscarTerapia.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscarTerapia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscarTerapia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarTerapia.Location = new System.Drawing.Point(709, 59);
            this.txtBuscarTerapia.Name = "txtBuscarTerapia";
            this.txtBuscarTerapia.Size = new System.Drawing.Size(212, 29);
            this.txtBuscarTerapia.TabIndex = 42;
            // 
            // dgvBuscarTerapia
            // 
            this.dgvBuscarTerapia.AllowUserToAddRows = false;
            this.dgvBuscarTerapia.AllowUserToDeleteRows = false;
            this.dgvBuscarTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBuscarTerapia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvBuscarTerapia.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBuscarTerapia.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvBuscarTerapia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBuscarTerapia.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvBuscarTerapia.Location = new System.Drawing.Point(629, 92);
            this.dgvBuscarTerapia.Name = "dgvBuscarTerapia";
            this.dgvBuscarTerapia.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBuscarTerapia.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvBuscarTerapia.Size = new System.Drawing.Size(335, 366);
            this.dgvBuscarTerapia.TabIndex = 41;
            this.dgvBuscarTerapia.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBuscarTerapia_CellClick);
            // 
            // Consulta
            // 
            this.Consulta.Controls.Add(this.lblIdTerapia);
            this.Consulta.Controls.Add(this.btnCancelar);
            this.Consulta.Controls.Add(this.btnFinalizarTerapia);
            this.Consulta.Controls.Add(this.cbxTerapiaHorario);
            this.Consulta.Controls.Add(this.label7);
            this.Consulta.Controls.Add(this.txtDataTerapia);
            this.Consulta.Controls.Add(this.label11);
            this.Consulta.Controls.Add(this.cbxTipoTerapia);
            this.Consulta.Controls.Add(this.label8);
            this.Consulta.Controls.Add(this.label9);
            this.Consulta.Controls.Add(this.txtTerapiaValor);
            this.Consulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consulta.Location = new System.Drawing.Point(6, 205);
            this.Consulta.Name = "Consulta";
            this.Consulta.Size = new System.Drawing.Size(614, 253);
            this.Consulta.TabIndex = 39;
            this.Consulta.TabStop = false;
            this.Consulta.Text = "Terapia";
            this.toolTip1.SetToolTip(this.Consulta, "Finalizar Atendimento");
            // 
            // lblIdTerapia
            // 
            this.lblIdTerapia.AutoSize = true;
            this.lblIdTerapia.Location = new System.Drawing.Point(270, 26);
            this.lblIdTerapia.Name = "lblIdTerapia";
            this.lblIdTerapia.Size = new System.Drawing.Size(74, 22);
            this.lblIdTerapia.TabIndex = 39;
            this.lblIdTerapia.Text = "IdTerapia";
            this.lblIdTerapia.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(349, 171);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(140, 51);
            this.btnCancelar.TabIndex = 45;
            this.btnCancelar.Text = "Cancelar";
            this.toolTip1.SetToolTip(this.btnCancelar, "Cancelar");
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnFinalizarTerapia
            // 
            this.btnFinalizarTerapia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarTerapia.Location = new System.Drawing.Point(131, 171);
            this.btnFinalizarTerapia.Name = "btnFinalizarTerapia";
            this.btnFinalizarTerapia.Size = new System.Drawing.Size(136, 51);
            this.btnFinalizarTerapia.TabIndex = 43;
            this.btnFinalizarTerapia.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarTerapia, "Finalizar Terapia");
            this.btnFinalizarTerapia.UseVisualStyleBackColor = true;
            this.btnFinalizarTerapia.Visible = false;
            this.btnFinalizarTerapia.Click += new System.EventHandler(this.btnFinalizarTerapia_Click);
            // 
            // cbxTerapiaHorario
            // 
            this.cbxTerapiaHorario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTerapiaHorario.Enabled = false;
            this.cbxTerapiaHorario.FormattingEnabled = true;
            this.cbxTerapiaHorario.Location = new System.Drawing.Point(488, 103);
            this.cbxTerapiaHorario.Name = "cbxTerapiaHorario";
            this.cbxTerapiaHorario.Size = new System.Drawing.Size(114, 30);
            this.cbxTerapiaHorario.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(422, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 22);
            this.label7.TabIndex = 6;
            this.label7.Text = "Horário:";
            // 
            // txtDataTerapia
            // 
            this.txtDataTerapia.Enabled = false;
            this.txtDataTerapia.Location = new System.Drawing.Point(132, 104);
            this.txtDataTerapia.Name = "txtDataTerapia";
            this.txtDataTerapia.Size = new System.Drawing.Size(286, 29);
            this.txtDataTerapia.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 22);
            this.label11.TabIndex = 4;
            this.label11.Text = "Data da Terapia:";
            // 
            // cbxTipoTerapia
            // 
            this.cbxTipoTerapia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoTerapia.Enabled = false;
            this.cbxTipoTerapia.FormattingEnabled = true;
            this.cbxTipoTerapia.Location = new System.Drawing.Point(56, 57);
            this.cbxTipoTerapia.Name = "cbxTipoTerapia";
            this.cbxTipoTerapia.Size = new System.Drawing.Size(362, 30);
            this.cbxTipoTerapia.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(422, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 22);
            this.label8.TabIndex = 2;
            this.label8.Text = "Valor:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 22);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tipo:";
            // 
            // txtTerapiaValor
            // 
            this.txtTerapiaValor.Enabled = false;
            this.txtTerapiaValor.Location = new System.Drawing.Point(488, 58);
            this.txtTerapiaValor.Name = "txtTerapiaValor";
            this.txtTerapiaValor.Size = new System.Drawing.Size(114, 29);
            this.txtTerapiaValor.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblIdConsulta);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtNumeroCrpTerapia);
            this.groupBox2.Controls.Add(this.txtNomePsicologoTerapia);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTerapiaDataNascimento);
            this.groupBox2.Controls.Add(this.txtTerapiaNomePaciente);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(614, 142);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da consulta";
            // 
            // lblIdConsulta
            // 
            this.lblIdConsulta.AutoSize = true;
            this.lblIdConsulta.Location = new System.Drawing.Point(263, 112);
            this.lblIdConsulta.Name = "lblIdConsulta";
            this.lblIdConsulta.Size = new System.Drawing.Size(84, 22);
            this.lblIdConsulta.TabIndex = 38;
            this.lblIdConsulta.Text = "IdConsulta";
            this.lblIdConsulta.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(350, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 22);
            this.label4.TabIndex = 6;
            this.label4.Text = "Número Crp:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "Psicólogo:";
            // 
            // txtNumeroCrpTerapia
            // 
            this.txtNumeroCrpTerapia.Enabled = false;
            this.txtNumeroCrpTerapia.Location = new System.Drawing.Point(481, 80);
            this.txtNumeroCrpTerapia.Name = "txtNumeroCrpTerapia";
            this.txtNumeroCrpTerapia.Size = new System.Drawing.Size(121, 29);
            this.txtNumeroCrpTerapia.TabIndex = 7;
            // 
            // txtNomePsicologoTerapia
            // 
            this.txtNomePsicologoTerapia.Enabled = false;
            this.txtNomePsicologoTerapia.Location = new System.Drawing.Point(89, 79);
            this.txtNomePsicologoTerapia.Name = "txtNomePsicologoTerapia";
            this.txtNomePsicologoTerapia.Size = new System.Drawing.Size(255, 29);
            this.txtNomePsicologoTerapia.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(349, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Paciente:";
            // 
            // txtTerapiaDataNascimento
            // 
            this.txtTerapiaDataNascimento.Enabled = false;
            this.txtTerapiaDataNascimento.Location = new System.Drawing.Point(481, 36);
            this.txtTerapiaDataNascimento.Name = "txtTerapiaDataNascimento";
            this.txtTerapiaDataNascimento.Size = new System.Drawing.Size(121, 29);
            this.txtTerapiaDataNascimento.TabIndex = 3;
            // 
            // txtTerapiaNomePaciente
            // 
            this.txtTerapiaNomePaciente.Enabled = false;
            this.txtTerapiaNomePaciente.Location = new System.Drawing.Point(89, 35);
            this.txtTerapiaNomePaciente.Name = "txtTerapiaNomePaciente";
            this.txtTerapiaNomePaciente.Size = new System.Drawing.Size(255, 29);
            this.txtTerapiaNomePaciente.TabIndex = 1;
            // 
            // btnFecharDetalhesPaciente
            // 
            this.btnFecharDetalhesPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFecharDetalhesPaciente.AutoSize = true;
            this.btnFecharDetalhesPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFecharDetalhesPaciente.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFecharDetalhesPaciente.Location = new System.Drawing.Point(946, -7);
            this.btnFecharDetalhesPaciente.Name = "btnFecharDetalhesPaciente";
            this.btnFecharDetalhesPaciente.Size = new System.Drawing.Size(25, 26);
            this.btnFecharDetalhesPaciente.TabIndex = 28;
            this.btnFecharDetalhesPaciente.Text = "X";
            this.toolTip1.SetToolTip(this.btnFecharDetalhesPaciente, "Fechar");
            this.btnFecharDetalhesPaciente.Click += new System.EventHandler(this.btnFecharDetalhesPaciente_Click);
            // 
            // btnExcluirTerapia
            // 
            this.btnExcluirTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluirTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirTerapia.Image")));
            this.btnExcluirTerapia.Location = new System.Drawing.Point(530, 464);
            this.btnExcluirTerapia.Name = "btnExcluirTerapia";
            this.btnExcluirTerapia.Size = new System.Drawing.Size(69, 59);
            this.btnExcluirTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExcluirTerapia.TabIndex = 27;
            this.btnExcluirTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnExcluirTerapia, "Excluir Terapia");
            // 
            // btnEditarTerapia
            // 
            this.btnEditarTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarTerapia.Image")));
            this.btnEditarTerapia.Location = new System.Drawing.Point(409, 464);
            this.btnEditarTerapia.Name = "btnEditarTerapia";
            this.btnEditarTerapia.Size = new System.Drawing.Size(69, 59);
            this.btnEditarTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditarTerapia.TabIndex = 26;
            this.btnEditarTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnEditarTerapia, "Editar Terapia");
            this.btnEditarTerapia.Click += new System.EventHandler(this.btnEditarTerapia_Click);
            // 
            // FormDetalhesAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormDetalhesAtendimento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormDetalhesAtendimento";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBuscarTerapia)).EndInit();
            this.Consulta.ResumeLayout(false);
            this.Consulta.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarTerapia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label btnFecharDetalhesPaciente;
        private System.Windows.Forms.PictureBox btnExcluirTerapia;
        private System.Windows.Forms.PictureBox btnEditarTerapia;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox Consulta;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnFinalizarTerapia;
        private System.Windows.Forms.ComboBox cbxTerapiaHorario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker txtDataTerapia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxTipoTerapia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTerapiaValor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblIdConsulta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNumeroCrpTerapia;
        private System.Windows.Forms.TextBox txtNomePsicologoTerapia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTerapiaDataNascimento;
        private System.Windows.Forms.TextBox txtTerapiaNomePaciente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btnPequisarTerapia;
        private System.Windows.Forms.TextBox txtBuscarTerapia;
        private System.Windows.Forms.DataGridView dgvBuscarTerapia;
        private System.Windows.Forms.Label lblIdTerapia;
    }
}