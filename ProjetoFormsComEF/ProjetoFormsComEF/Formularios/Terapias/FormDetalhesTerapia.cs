﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Terapias
{
    public partial class FormDetalhesAtendimento : Form
    {
        public FormDetalhesAtendimento()
        {
            InitializeComponent();
            PreencherTerapiaHorario();
            PreencherTipoTerapia();
        }

        private void btnFecharDetalhesPaciente_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPequisarPsicologo_Click(object sender, EventArgs e)
        {
            Terapia terapia = new Terapia();
            dgvBuscarTerapia.DataSource = terapia.BuscarTodos(txtBuscarTerapia.Text);
        }

        private void dgvBuscarTerapia_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int) dgvBuscarTerapia.CurrentRow.Cells["Id"].Value;
            Terapia terapia = new Terapia();
            terapia = terapia.BuscarPorId(id);

            lblIdTerapia.Text = terapia.TerapiaId.ToString();
            lblIdConsulta.Text = terapia.ConsultaId.ToString();
            txtTerapiaNomePaciente.Text = terapia.Consulta.Paciente.Nome;
            txtTerapiaDataNascimento.Text = terapia.Consulta.Paciente.DataNascimento.Date.ToString();
            txtNomePsicologoTerapia.Text = terapia.Consulta.Psicologo.Nome;
            txtNumeroCrpTerapia.Text = terapia.Consulta.Psicologo.NumeroCrp.ToString();
            cbxTipoTerapia.Text = terapia.TipoTerapia.Descricao;
            cbxTerapiaHorario.Text = terapia.Horario.Descricao;
            txtDataTerapia.Text = terapia.DataTerapia.ToString();
            txtTerapiaValor.Text = terapia.Valor.ToString();
        }

        public void LiparCampos()
        {
            lblIdConsulta.Text = String.Empty;
            txtTerapiaNomePaciente.Text = String.Empty;
            txtTerapiaDataNascimento.Text = String.Empty;
            txtNomePsicologoTerapia.Text = String.Empty;
            txtNumeroCrpTerapia.Text = String.Empty;
            cbxTipoTerapia.Text = String.Empty;
            cbxTerapiaHorario.Text = String.Empty;
            txtDataTerapia.Text = String.Empty;
            txtTerapiaValor.Text = String.Empty;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            cbxTipoTerapia.Enabled = false;
            cbxTerapiaHorario.Enabled = false;
            txtTerapiaValor.Enabled = false;
            txtDataTerapia.Enabled = false;
            btnCancelar.Visible = false;
            btnFinalizarTerapia.Visible = false;
            btnEditarTerapia.Visible = true;
            btnExcluirTerapia.Visible = true;

            LiparCampos();
        }

        private void btnEditarTerapia_Click(object sender, EventArgs e)
        {
            cbxTipoTerapia.Enabled = true;
            cbxTerapiaHorario.Enabled = true;
            txtTerapiaValor.Enabled = true;
            txtDataTerapia.Enabled = true;
            btnCancelar.Visible = true;
            btnFinalizarTerapia.Visible = true;
            btnEditarTerapia.Visible = false;
            btnExcluirTerapia.Visible = false;
        }

        private void btnFinalizarTerapia_Click(object sender, EventArgs e)
        {
            try
            {
                Terapia terapia = new Terapia();
                if (txtTerapiaNomePaciente.Text == String.Empty || txtTerapiaValor.Text == String.Empty || txtDataTerapia.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios...", "Informação");
                }
                else
                {
                    terapia.DataTerapia = Convert.ToDateTime(txtDataTerapia.Text);

                    if (terapia.DataTerapia < DateTime.Now.Date)
                    {
                        MessageBox.Show("Não é possivel agendar para dias anteriores a nao ser que vc seje o Dr. Estranho", "Informação");
                    }
                    else
                    {
                        terapia.TerapiaId = Convert.ToInt32(lblIdTerapia.Text);
                        terapia.ConsultaId = Convert.ToInt32(lblIdConsulta.Text);
                        terapia.TipoTerapiaId = Convert.ToInt32(cbxTipoTerapia.SelectedValue);
                        terapia.HorarioId = Convert.ToInt32(cbxTerapiaHorario.SelectedValue);
                        terapia.Valor = Convert.ToDecimal(txtTerapiaValor.Text);

                        terapia.Editar(terapia);
                        MessageBox.Show("Terapia Aualizada com sucesso...", "Informação");
                        dgvBuscarTerapia.DataSource = terapia.BuscarTodos(txtBuscarTerapia.Text);
                    }
                }

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        public void PreencherTipoTerapia()
        {
            ConexaoBd cnBd = new ConexaoBd();
            var tipoTerapia = from tipoTerapias in cnBd.TipoTerapias
                select tipoTerapias;
            cbxTipoTerapia.DataSource = tipoTerapia.ToList();
            cbxTipoTerapia.ValueMember = "TipoTerapiaId";
            cbxTipoTerapia.DisplayMember = "Descricao";
        }

        public void PreencherTerapiaHorario()
        {
            ConexaoBd cnBd = new ConexaoBd();
            var horario = from horarios in cnBd.Horarios
                select horarios;
            cbxTerapiaHorario.DataSource = horario.ToList();
            cbxTerapiaHorario.ValueMember = "HorarioId";
            cbxTerapiaHorario.DisplayMember = "Descricao";
        }
    }
}
