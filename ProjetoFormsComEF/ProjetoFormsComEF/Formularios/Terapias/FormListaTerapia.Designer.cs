﻿namespace ProjetoIntegrador4.Formularios.Terapias
{
    partial class FormListaAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaAtendimento));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDetalhesTerapia = new System.Windows.Forms.PictureBox();
            this.btnNovaTerapia = new System.Windows.Forms.PictureBox();
            this.btnVoltaTerapia = new System.Windows.Forms.PictureBox();
            this.btnPesquisarTerapia = new System.Windows.Forms.PictureBox();
            this.txtBuscaTerapia = new System.Windows.Forms.TextBox();
            this.dgvTerapia = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerapia)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDetalhesTerapia);
            this.groupBox1.Controls.Add(this.btnNovaTerapia);
            this.groupBox1.Controls.Add(this.btnVoltaTerapia);
            this.groupBox1.Controls.Add(this.btnPesquisarTerapia);
            this.groupBox1.Controls.Add(this.txtBuscaTerapia);
            this.groupBox1.Controls.Add(this.dgvTerapia);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Terapias";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 26);
            this.label1.TabIndex = 27;
            this.label1.Text = "Pesquisar:";
            // 
            // btnDetalhesTerapia
            // 
            this.btnDetalhesTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalhesTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalhesTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnDetalhesTerapia.Image")));
            this.btnDetalhesTerapia.Location = new System.Drawing.Point(473, 466);
            this.btnDetalhesTerapia.Name = "btnDetalhesTerapia";
            this.btnDetalhesTerapia.Size = new System.Drawing.Size(73, 59);
            this.btnDetalhesTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetalhesTerapia.TabIndex = 26;
            this.btnDetalhesTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnDetalhesTerapia, "Detalhes da Terapia");
            this.btnDetalhesTerapia.Click += new System.EventHandler(this.btnDetalhesAtendimento_Click);
            // 
            // btnNovaTerapia
            // 
            this.btnNovaTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovaTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovaTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnNovaTerapia.Image")));
            this.btnNovaTerapia.Location = new System.Drawing.Point(919, 62);
            this.btnNovaTerapia.Name = "btnNovaTerapia";
            this.btnNovaTerapia.Size = new System.Drawing.Size(49, 45);
            this.btnNovaTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovaTerapia.TabIndex = 16;
            this.btnNovaTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovaTerapia, "Nova Terapia");
            this.btnNovaTerapia.Click += new System.EventHandler(this.btnNovoAtendimento_Click);
            // 
            // btnVoltaTerapia
            // 
            this.btnVoltaTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltaTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltaTerapia.Image")));
            this.btnVoltaTerapia.Location = new System.Drawing.Point(11, 71);
            this.btnVoltaTerapia.Name = "btnVoltaTerapia";
            this.btnVoltaTerapia.Size = new System.Drawing.Size(48, 36);
            this.btnVoltaTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltaTerapia.TabIndex = 17;
            this.btnVoltaTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnVoltaTerapia, "Voltar");
            this.btnVoltaTerapia.Click += new System.EventHandler(this.btnVoltaAtendimento_Click);
            // 
            // btnPesquisarTerapia
            // 
            this.btnPesquisarTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisarTerapia.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisarTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarTerapia.Image")));
            this.btnPesquisarTerapia.Location = new System.Drawing.Point(919, 154);
            this.btnPesquisarTerapia.Name = "btnPesquisarTerapia";
            this.btnPesquisarTerapia.Size = new System.Drawing.Size(50, 48);
            this.btnPesquisarTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPesquisarTerapia.TabIndex = 24;
            this.btnPesquisarTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPesquisarTerapia, "Pesquisar Terapia");
            this.btnPesquisarTerapia.Click += new System.EventHandler(this.btnPesquisarTerapia_Click);
            // 
            // txtBuscaTerapia
            // 
            this.txtBuscaTerapia.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscaTerapia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscaTerapia.Location = new System.Drawing.Point(112, 169);
            this.txtBuscaTerapia.Name = "txtBuscaTerapia";
            this.txtBuscaTerapia.Size = new System.Drawing.Size(781, 29);
            this.txtBuscaTerapia.TabIndex = 22;
            // 
            // dgvTerapia
            // 
            this.dgvTerapia.AllowUserToAddRows = false;
            this.dgvTerapia.AllowUserToDeleteRows = false;
            this.dgvTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTerapia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTerapia.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTerapia.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTerapia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTerapia.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTerapia.Location = new System.Drawing.Point(6, 204);
            this.dgvTerapia.Name = "dgvTerapia";
            this.dgvTerapia.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTerapia.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvTerapia.Size = new System.Drawing.Size(962, 256);
            this.dgvTerapia.TabIndex = 21;
            // 
            // FormListaAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(205, 90);
            this.Name = "FormListaAtendimento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormAtendimentoLista";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerapia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox btnDetalhesTerapia;
        private System.Windows.Forms.PictureBox btnNovaTerapia;
        private System.Windows.Forms.PictureBox btnVoltaTerapia;
        private System.Windows.Forms.PictureBox btnPesquisarTerapia;
        private System.Windows.Forms.TextBox txtBuscaTerapia;
        private System.Windows.Forms.DataGridView dgvTerapia;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
    }
}