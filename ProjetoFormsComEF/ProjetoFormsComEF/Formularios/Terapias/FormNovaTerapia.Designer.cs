﻿namespace ProjetoIntegrador4.Formularios.Terapias
{
    partial class FormNovaTerapia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNovaTerapia));
            this.btnConsultaBuscarPsicologo = new System.Windows.Forms.GroupBox();
            this.dgvAgendarTerapia = new System.Windows.Forms.DataGridView();
            this.btnAgendarTerapia = new System.Windows.Forms.Button();
            this.btnFinalizarTerapia = new System.Windows.Forms.Button();
            this.Consulta = new System.Windows.Forms.GroupBox();
            this.cbxTerapiaHorario = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDataTerapia = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxTipoTerapia = new System.Windows.Forms.ComboBox();
            this.btnNovoTipoTerapia = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTerapiaValor = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblIdConsulta = new System.Windows.Forms.Label();
            this.btnBuscarPacienteEmConsulta = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNumeroCrpTerapia = new System.Windows.Forms.TextBox();
            this.txtNomePsicologoTerapia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTerapiaDataNascimento = new System.Windows.Forms.TextBox();
            this.txtTerapiaNomePaciente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnConsultaBuscarPsicologo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgendarTerapia)).BeginInit();
            this.Consulta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoTipoTerapia)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConsultaBuscarPsicologo
            // 
            this.btnConsultaBuscarPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultaBuscarPsicologo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnConsultaBuscarPsicologo.Controls.Add(this.dgvAgendarTerapia);
            this.btnConsultaBuscarPsicologo.Controls.Add(this.btnAgendarTerapia);
            this.btnConsultaBuscarPsicologo.Controls.Add(this.btnFinalizarTerapia);
            this.btnConsultaBuscarPsicologo.Controls.Add(this.Consulta);
            this.btnConsultaBuscarPsicologo.Controls.Add(this.groupBox2);
            this.btnConsultaBuscarPsicologo.Controls.Add(this.label1);
            this.btnConsultaBuscarPsicologo.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaBuscarPsicologo.Location = new System.Drawing.Point(2, 2);
            this.btnConsultaBuscarPsicologo.Name = "btnConsultaBuscarPsicologo";
            this.btnConsultaBuscarPsicologo.Size = new System.Drawing.Size(974, 531);
            this.btnConsultaBuscarPsicologo.TabIndex = 0;
            this.btnConsultaBuscarPsicologo.TabStop = false;
            this.btnConsultaBuscarPsicologo.Text = "Cadastro de Terapia";
            // 
            // dgvAgendarTerapia
            // 
            this.dgvAgendarTerapia.AllowUserToAddRows = false;
            this.dgvAgendarTerapia.AllowUserToDeleteRows = false;
            this.dgvAgendarTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAgendarTerapia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAgendarTerapia.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAgendarTerapia.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAgendarTerapia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgendarTerapia.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAgendarTerapia.Location = new System.Drawing.Point(67, 315);
            this.dgvAgendarTerapia.Name = "dgvAgendarTerapia";
            this.dgvAgendarTerapia.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAgendarTerapia.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAgendarTerapia.Size = new System.Drawing.Size(845, 154);
            this.dgvAgendarTerapia.TabIndex = 36;
            // 
            // btnAgendarTerapia
            // 
            this.btnAgendarTerapia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgendarTerapia.Location = new System.Drawing.Point(357, 475);
            this.btnAgendarTerapia.Name = "btnAgendarTerapia";
            this.btnAgendarTerapia.Size = new System.Drawing.Size(237, 46);
            this.btnAgendarTerapia.TabIndex = 0;
            this.btnAgendarTerapia.Text = "Agendar";
            this.toolTip1.SetToolTip(this.btnAgendarTerapia, "Agendar Terapia");
            this.btnAgendarTerapia.UseVisualStyleBackColor = true;
            this.btnAgendarTerapia.Click += new System.EventHandler(this.btnAgendarTerapia_Click);
            // 
            // btnFinalizarTerapia
            // 
            this.btnFinalizarTerapia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarTerapia.Location = new System.Drawing.Point(815, 475);
            this.btnFinalizarTerapia.Name = "btnFinalizarTerapia";
            this.btnFinalizarTerapia.Size = new System.Drawing.Size(97, 46);
            this.btnFinalizarTerapia.TabIndex = 5;
            this.btnFinalizarTerapia.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarTerapia, "Finalizar Terapia");
            this.btnFinalizarTerapia.UseVisualStyleBackColor = true;
            this.btnFinalizarTerapia.Click += new System.EventHandler(this.btnFinalizarTerapia_Click);
            // 
            // Consulta
            // 
            this.Consulta.Controls.Add(this.cbxTerapiaHorario);
            this.Consulta.Controls.Add(this.label7);
            this.Consulta.Controls.Add(this.txtDataTerapia);
            this.Consulta.Controls.Add(this.label11);
            this.Consulta.Controls.Add(this.cbxTipoTerapia);
            this.Consulta.Controls.Add(this.btnNovoTipoTerapia);
            this.Consulta.Controls.Add(this.label8);
            this.Consulta.Controls.Add(this.label9);
            this.Consulta.Controls.Add(this.txtTerapiaValor);
            this.Consulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consulta.Location = new System.Drawing.Point(67, 201);
            this.Consulta.Name = "Consulta";
            this.Consulta.Size = new System.Drawing.Size(845, 108);
            this.Consulta.TabIndex = 4;
            this.Consulta.TabStop = false;
            this.Consulta.Text = "Terapia";
            this.toolTip1.SetToolTip(this.Consulta, "Finalizar Atendimento");
            // 
            // cbxTerapiaHorario
            // 
            this.cbxTerapiaHorario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTerapiaHorario.FormattingEnabled = true;
            this.cbxTerapiaHorario.Location = new System.Drawing.Point(601, 68);
            this.cbxTerapiaHorario.Name = "cbxTerapiaHorario";
            this.cbxTerapiaHorario.Size = new System.Drawing.Size(168, 30);
            this.cbxTerapiaHorario.TabIndex = 7;
            this.cbxTerapiaHorario.Click += new System.EventHandler(this.cbxTerapiaHorario_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(535, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 22);
            this.label7.TabIndex = 6;
            this.label7.Text = "Horário:";
            // 
            // txtDataTerapia
            // 
            this.txtDataTerapia.Location = new System.Drawing.Point(178, 69);
            this.txtDataTerapia.Name = "txtDataTerapia";
            this.txtDataTerapia.Size = new System.Drawing.Size(349, 29);
            this.txtDataTerapia.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(49, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 22);
            this.label11.TabIndex = 4;
            this.label11.Text = "Data da Terapia:";
            // 
            // cbxTipoTerapia
            // 
            this.cbxTipoTerapia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoTerapia.FormattingEnabled = true;
            this.cbxTipoTerapia.Location = new System.Drawing.Point(124, 22);
            this.cbxTipoTerapia.Name = "cbxTipoTerapia";
            this.cbxTipoTerapia.Size = new System.Drawing.Size(350, 30);
            this.cbxTipoTerapia.TabIndex = 1;
            this.cbxTipoTerapia.Click += new System.EventHandler(this.cbxTipoTerapia_Click);
            // 
            // btnNovoTipoTerapia
            // 
            this.btnNovoTipoTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovoTipoTerapia.BackColor = System.Drawing.Color.Transparent;
            this.btnNovoTipoTerapia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovoTipoTerapia.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoTipoTerapia.Image")));
            this.btnNovoTipoTerapia.Location = new System.Drawing.Point(491, 22);
            this.btnNovoTipoTerapia.Name = "btnNovoTipoTerapia";
            this.btnNovoTipoTerapia.Size = new System.Drawing.Size(31, 30);
            this.btnNovoTipoTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovoTipoTerapia.TabIndex = 25;
            this.btnNovoTipoTerapia.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovoTipoTerapia, "Cadastrar Nova Terapia");
            this.btnNovoTipoTerapia.Click += new System.EventHandler(this.btnNovoTipoTerapia_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(535, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 22);
            this.label8.TabIndex = 2;
            this.label8.Text = "Valor:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(49, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 22);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tipo:";
            // 
            // txtTerapiaValor
            // 
            this.txtTerapiaValor.Location = new System.Drawing.Point(601, 23);
            this.txtTerapiaValor.Name = "txtTerapiaValor";
            this.txtTerapiaValor.Size = new System.Drawing.Size(168, 29);
            this.txtTerapiaValor.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblIdConsulta);
            this.groupBox2.Controls.Add(this.btnBuscarPacienteEmConsulta);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtNumeroCrpTerapia);
            this.groupBox2.Controls.Add(this.txtNomePsicologoTerapia);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTerapiaDataNascimento);
            this.groupBox2.Controls.Add(this.txtTerapiaNomePaciente);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(67, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(845, 145);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da consulta";
            // 
            // lblIdConsulta
            // 
            this.lblIdConsulta.AutoSize = true;
            this.lblIdConsulta.Location = new System.Drawing.Point(252, 113);
            this.lblIdConsulta.Name = "lblIdConsulta";
            this.lblIdConsulta.Size = new System.Drawing.Size(84, 22);
            this.lblIdConsulta.TabIndex = 38;
            this.lblIdConsulta.Text = "IdConsulta";
            this.lblIdConsulta.Visible = false;
            // 
            // btnBuscarPacienteEmConsulta
            // 
            this.btnBuscarPacienteEmConsulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarPacienteEmConsulta.Location = new System.Drawing.Point(372, 109);
            this.btnBuscarPacienteEmConsulta.Name = "btnBuscarPacienteEmConsulta";
            this.btnBuscarPacienteEmConsulta.Size = new System.Drawing.Size(142, 30);
            this.btnBuscarPacienteEmConsulta.TabIndex = 37;
            this.btnBuscarPacienteEmConsulta.Text = "Buscar paciente";
            this.toolTip1.SetToolTip(this.btnBuscarPacienteEmConsulta, "Finalizar Terapia");
            this.btnBuscarPacienteEmConsulta.UseVisualStyleBackColor = true;
            this.btnBuscarPacienteEmConsulta.Click += new System.EventHandler(this.btnBuscarPacienteEmConsulta_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(461, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 22);
            this.label4.TabIndex = 6;
            this.label4.Text = "Número Crp:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "Psicólogo:";
            // 
            // txtNumeroCrpTerapia
            // 
            this.txtNumeroCrpTerapia.Enabled = false;
            this.txtNumeroCrpTerapia.Location = new System.Drawing.Point(572, 72);
            this.txtNumeroCrpTerapia.Name = "txtNumeroCrpTerapia";
            this.txtNumeroCrpTerapia.Size = new System.Drawing.Size(196, 29);
            this.txtNumeroCrpTerapia.TabIndex = 7;
            // 
            // txtNomePsicologoTerapia
            // 
            this.txtNomePsicologoTerapia.Enabled = false;
            this.txtNomePsicologoTerapia.Location = new System.Drawing.Point(124, 74);
            this.txtNomePsicologoTerapia.Name = "txtNomePsicologoTerapia";
            this.txtNomePsicologoTerapia.Size = new System.Drawing.Size(295, 29);
            this.txtNomePsicologoTerapia.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(434, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Paciente:";
            // 
            // txtTerapiaDataNascimento
            // 
            this.txtTerapiaDataNascimento.Enabled = false;
            this.txtTerapiaDataNascimento.Location = new System.Drawing.Point(572, 23);
            this.txtTerapiaDataNascimento.Name = "txtTerapiaDataNascimento";
            this.txtTerapiaDataNascimento.Size = new System.Drawing.Size(196, 29);
            this.txtTerapiaDataNascimento.TabIndex = 3;
            // 
            // txtTerapiaNomePaciente
            // 
            this.txtTerapiaNomePaciente.Enabled = false;
            this.txtTerapiaNomePaciente.Location = new System.Drawing.Point(124, 25);
            this.txtTerapiaNomePaciente.Name = "txtTerapiaNomePaciente";
            this.txtTerapiaNomePaciente.Size = new System.Drawing.Size(295, 29);
            this.txtTerapiaNomePaciente.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(946, -7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.toolTip1.SetToolTip(this.label1, "Fechar");
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // FormNovaTerapia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.btnConsultaBuscarPsicologo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormNovaTerapia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormNovoAtendimento";
            this.btnConsultaBuscarPsicologo.ResumeLayout(false);
            this.btnConsultaBuscarPsicologo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgendarTerapia)).EndInit();
            this.Consulta.ResumeLayout(false);
            this.Consulta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovoTipoTerapia)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox btnConsultaBuscarPsicologo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTerapiaDataNascimento;
        private System.Windows.Forms.TextBox txtTerapiaNomePaciente;
        private System.Windows.Forms.Button btnFinalizarTerapia;
        private System.Windows.Forms.GroupBox Consulta;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTerapiaValor;
        private System.Windows.Forms.ComboBox cbxTerapiaHorario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker txtDataTerapia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxTipoTerapia;
        private System.Windows.Forms.PictureBox btnNovoTipoTerapia;
        private System.Windows.Forms.Button btnAgendarTerapia;
        private System.Windows.Forms.DataGridView dgvAgendarTerapia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNumeroCrpTerapia;
        private System.Windows.Forms.TextBox txtNomePsicologoTerapia;
        private System.Windows.Forms.Label lblIdConsulta;
        private System.Windows.Forms.Button btnBuscarPacienteEmConsulta;
    }
}