﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.Consultas;
using ProjetoIntegrador4.Formularios.FormsAdicionais;

namespace ProjetoIntegrador4.Formularios.Terapias
{
    public partial class FormNovaTerapia : Form
    {
       public Terapia terapia = new Terapia();
        public FormNovaTerapia()
        {
            InitializeComponent();
        }

        private void cbxTerapiaHorario_Click(object sender, EventArgs e)
        {
            ConexaoBd cnBd = new ConexaoBd();
            var horario = from horarios in cnBd.Horarios select horarios;
            cbxTerapiaHorario.DataSource = horario.ToList();
            cbxTerapiaHorario.ValueMember = "HorarioId";
            cbxTerapiaHorario.DisplayMember = "Descricao";
        }

        private void cbxTipoTerapia_Click(object sender, EventArgs e)
        {
            ConexaoBd cnBd = new ConexaoBd();
            var tipoTerapia = from tipoTerapias in cnBd.TipoTerapias
                select tipoTerapias;
            cbxTipoTerapia.DataSource = tipoTerapia.ToList();
            cbxTipoTerapia.ValueMember = "TipoTerapiaId";
            cbxTipoTerapia.DisplayMember = "Descricao";
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnBuscarPacienteEmConsulta_Click(object sender, EventArgs e)
        {
            FormBuscarConsulta formBuscarConsulta = new FormBuscarConsulta(this);
            formBuscarConsulta.Show();
        }

        public void BuscarPacienteConsulta()
        {
            try
            {
                lblIdConsulta.Text = Convert.ToString(terapia.Consulta.ConsultaId);
                txtTerapiaNomePaciente.Text = terapia.Consulta.Paciente.Nome;
                txtTerapiaDataNascimento.Text = Convert.ToString(terapia.Consulta.Paciente.DataNascimento.Date);
                txtNomePsicologoTerapia.Text = terapia.Consulta.Psicologo.Nome;
                txtNumeroCrpTerapia.Text = Convert.ToString(terapia.Consulta.Psicologo.NumeroCrp);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void btnAgendarTerapia_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTerapiaNomePaciente.Text == String.Empty || txtTerapiaValor.Text == String.Empty || txtDataTerapia.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios...", "Informação");
                }
                else
                {
                    terapia.DataTerapia = Convert.ToDateTime(txtDataTerapia.Text);
                    if (terapia.DataTerapia < DateTime.Now.Date )
                    {
                        MessageBox.Show("Não é possivel agendar para dias anteriores a nao ser que vc seje o Dr. Estranho", "Informação");
                    }
                    else
                    {
                        terapia.ConsultaId = Convert.ToInt32(lblIdConsulta.Text);

                        terapia.TipoTerapiaId = Convert.ToInt32(cbxTipoTerapia.SelectedValue);
                        terapia.HorarioId = Convert.ToInt32(cbxTerapiaHorario.SelectedValue);
                        terapia.Valor = Convert.ToDecimal(txtTerapiaValor.Text);

                        terapia.Salvar(terapia);
                        MessageBox.Show("Terapia agendada com sucesso...", "Informação");
                        int consulta = Convert.ToInt32(lblIdConsulta.Text);
                        dgvAgendarTerapia.DataSource = terapia.BuscarPorPaciente(consulta);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnNovoTipoTerapia_Click(object sender, EventArgs e)
        {
            FormTipoTerapia formTipoTerapia = new FormTipoTerapia();
            formTipoTerapia.Show();
        }

        private void btnFinalizarTerapia_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
