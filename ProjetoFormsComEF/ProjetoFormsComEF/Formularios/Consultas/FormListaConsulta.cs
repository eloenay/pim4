﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.Consultas;

namespace ProjetoIntegrador4.Formularios.Exame
{
    public partial class FormListaConsulta : Form
    {
        private FormNovaConsulta formNovaConsulta;
        private FormDetalhesConsulta formEditarConsulta;

        public FormListaConsulta()
        {
            InitializeComponent();
            CarregarLista();
        }

        public void CarregarLista()
        {
            Consulta consulta = new Consulta();
            dgvConsultas.DataSource = consulta.BuscarTodos();
        }

        private void btnVoltaConsultas_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNovaConsulta_Click(object sender, EventArgs e)
        {
            formNovaConsulta = new FormNovaConsulta();
            formNovaConsulta.Show();
        }

        private void btnDetalhesClinica_Click(object sender, EventArgs e)
        {
            formEditarConsulta = new FormDetalhesConsulta();
            formEditarConsulta.Show();
        }

        private void btnPesquisarConsulta_Click(object sender, EventArgs e)
        {
            Consulta consulta = new Consulta();
            if (rbPsicologo.Checked)
            {
                dgvConsultas.DataSource = consulta.BuscarPorPsicologo(txtBuscarConsulta.Text);
            }
            else
            if (rbPaciente.Checked)
            {
                dgvConsultas.DataSource = consulta.BuscarPorPaciente(txtBuscarConsulta.Text);
            }
            else
            {
                dgvConsultas.DataSource = consulta.BuscarTodos();
            }
        }
    }
}
