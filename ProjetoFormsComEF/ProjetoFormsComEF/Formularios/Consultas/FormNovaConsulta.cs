﻿using System;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Formularios.FormsAdicionais;

namespace ProjetoIntegrador4.Formularios.Consultas
{
    public partial class FormNovaConsulta : Form
    {

        public Consulta consulta = new Consulta();
        public FormNovaConsulta()
        {
            InitializeComponent();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConsultaBuscarPaciente_Click(object sender, EventArgs e)
        {
            FormBuscarPaciente formBuscarPacienteConsulta = new FormBuscarPaciente(this);
            formBuscarPacienteConsulta.Show();
        }

        private void btnConsultaBuscarPsicologo_Click(object sender, EventArgs e)
        {
            FormBuscarPsicologoConsulta formBuscarPsicologoConsulta = new FormBuscarPsicologoConsulta(this);
            formBuscarPsicologoConsulta.Show();
        }

        public void BuscarPaciente()
        {
            try
            {
                lblIdPaciente.Text = Convert.ToString(consulta.Paciente.PacienteId);
                txtConsultaNomePaciente.Text = consulta.Paciente.Nome;
                txtConsultaDataNascimento.Text = Convert.ToString(consulta.Paciente.DataNascimento.Date);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void BuscarPsicologo()
        {
            try
            {
                lblPsicologo.Text = Convert.ToString(consulta.Psicologo.PsicologoId);
                txtPsicologoConsulta.Text = consulta.Psicologo.Nome;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void btnFinalizarTerapia_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtConsultaNomePaciente.Text == String.Empty || txtPsicologoConsulta.Text == String.Empty || txtConsultaValor.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios...", "Informação");
                }
                else
                {
                    consulta.PacienteId = Convert.ToInt32(lblIdPaciente.Text);
                    consulta.PsicologoId = Convert.ToInt32(lblPsicologo.Text);
                    consulta.Valor = Convert.ToDecimal(txtConsultaValor.Text);
                    consulta.HorarioConsulta = TimeSpan.Parse(txtConsultaHorario.Text);
                    consulta.DataConsulta = Convert.ToDateTime(txtDataConsulta.Text);

                    if (consulta.DataConsulta >= DateTime.Now.Date)
                    {
                        consulta.Salvar(consulta);
                        MessageBox.Show("Consulta Cadastrada com sucesso...", "Informação");
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Não é possivel cadastrar para dias anteriores.", "Informação");
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }


        }
    }
}
