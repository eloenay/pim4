﻿namespace ProjetoIntegrador4.Formularios.Consultas
{
    partial class FormNovaConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNovaConsulta));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFinalizarTerapia = new System.Windows.Forms.Button();
            this.Consulta = new System.Windows.Forms.GroupBox();
            this.txtConsultaValor = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDataConsulta = new System.Windows.Forms.DateTimePicker();
            this.txtConsultaHorario = new System.Windows.Forms.MaskedTextBox();
            this.lblPsicologo = new System.Windows.Forms.Label();
            this.btnConsultaBuscarPsicologo = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPsicologoConsulta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtConsultaDataNascimento = new System.Windows.Forms.DateTimePicker();
            this.lblIdPaciente = new System.Windows.Forms.Label();
            this.btnConsultaBuscarPaciente = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtConsultaNomePaciente = new System.Windows.Forms.TextBox();
            this.btnFechar = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.Consulta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultaBuscarPsicologo)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultaBuscarPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.btnFinalizarTerapia);
            this.groupBox1.Controls.Add(this.Consulta);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFechar);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cadastro de Consultas";
            // 
            // btnFinalizarTerapia
            // 
            this.btnFinalizarTerapia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarTerapia.Location = new System.Drawing.Point(416, 452);
            this.btnFinalizarTerapia.Name = "btnFinalizarTerapia";
            this.btnFinalizarTerapia.Size = new System.Drawing.Size(142, 64);
            this.btnFinalizarTerapia.TabIndex = 3;
            this.btnFinalizarTerapia.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarTerapia, "Finalizar Consulta");
            this.btnFinalizarTerapia.UseVisualStyleBackColor = true;
            this.btnFinalizarTerapia.Click += new System.EventHandler(this.btnFinalizarTerapia_Click);
            // 
            // Consulta
            // 
            this.Consulta.Controls.Add(this.txtConsultaValor);
            this.Consulta.Controls.Add(this.label1);
            this.Consulta.Controls.Add(this.txtDataConsulta);
            this.Consulta.Controls.Add(this.txtConsultaHorario);
            this.Consulta.Controls.Add(this.lblPsicologo);
            this.Consulta.Controls.Add(this.btnConsultaBuscarPsicologo);
            this.Consulta.Controls.Add(this.label6);
            this.Consulta.Controls.Add(this.txtPsicologoConsulta);
            this.Consulta.Controls.Add(this.label7);
            this.Consulta.Controls.Add(this.label8);
            this.Consulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consulta.Location = new System.Drawing.Point(67, 233);
            this.Consulta.Name = "Consulta";
            this.Consulta.Size = new System.Drawing.Size(845, 157);
            this.Consulta.TabIndex = 2;
            this.Consulta.TabStop = false;
            this.Consulta.Text = "Consultas";
            // 
            // txtConsultaValor
            // 
            this.txtConsultaValor.Location = new System.Drawing.Point(227, 96);
            this.txtConsultaValor.Name = "txtConsultaValor";
            this.txtConsultaValor.Size = new System.Drawing.Size(101, 29);
            this.txtConsultaValor.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(334, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 22);
            this.label1.TabIndex = 5;
            this.label1.Text = "Dia da consulta:";
            // 
            // txtDataConsulta
            // 
            this.txtDataConsulta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDataConsulta.Location = new System.Drawing.Point(455, 96);
            this.txtDataConsulta.Name = "txtDataConsulta";
            this.txtDataConsulta.Size = new System.Drawing.Size(102, 29);
            this.txtDataConsulta.TabIndex = 6;
            // 
            // txtConsultaHorario
            // 
            this.txtConsultaHorario.Location = new System.Drawing.Point(630, 96);
            this.txtConsultaHorario.Mask = "00:00";
            this.txtConsultaHorario.Name = "txtConsultaHorario";
            this.txtConsultaHorario.Size = new System.Drawing.Size(60, 29);
            this.txtConsultaHorario.TabIndex = 8;
            this.txtConsultaHorario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtConsultaHorario.ValidatingType = typeof(System.DateTime);
            // 
            // lblPsicologo
            // 
            this.lblPsicologo.AutoSize = true;
            this.lblPsicologo.Location = new System.Drawing.Point(223, 18);
            this.lblPsicologo.Name = "lblPsicologo";
            this.lblPsicologo.Size = new System.Drawing.Size(88, 22);
            this.lblPsicologo.TabIndex = 0;
            this.lblPsicologo.Text = "IdPsicologo";
            this.lblPsicologo.Visible = false;
            // 
            // btnConsultaBuscarPsicologo
            // 
            this.btnConsultaBuscarPsicologo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultaBuscarPsicologo.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultaBuscarPsicologo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultaBuscarPsicologo.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultaBuscarPsicologo.Image")));
            this.btnConsultaBuscarPsicologo.Location = new System.Drawing.Point(720, 59);
            this.btnConsultaBuscarPsicologo.Name = "btnConsultaBuscarPsicologo";
            this.btnConsultaBuscarPsicologo.Size = new System.Drawing.Size(50, 48);
            this.btnConsultaBuscarPsicologo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnConsultaBuscarPsicologo.TabIndex = 26;
            this.btnConsultaBuscarPsicologo.TabStop = false;
            this.toolTip1.SetToolTip(this.btnConsultaBuscarPsicologo, "Buscar Psicólogo");
            this.btnConsultaBuscarPsicologo.Click += new System.EventHandler(this.btnConsultaBuscarPsicologo_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(142, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 22);
            this.label6.TabIndex = 1;
            this.label6.Text = "Psicólogo:";
            // 
            // txtPsicologoConsulta
            // 
            this.txtPsicologoConsulta.Enabled = false;
            this.txtPsicologoConsulta.Location = new System.Drawing.Point(227, 46);
            this.txtPsicologoConsulta.Name = "txtPsicologoConsulta";
            this.txtPsicologoConsulta.Size = new System.Drawing.Size(463, 29);
            this.txtPsicologoConsulta.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(560, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 22);
            this.label7.TabIndex = 7;
            this.label7.Text = "Horário:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(142, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 22);
            this.label8.TabIndex = 3;
            this.label8.Text = "Valor:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtConsultaDataNascimento);
            this.groupBox2.Controls.Add(this.lblIdPaciente);
            this.groupBox2.Controls.Add(this.btnConsultaBuscarPaciente);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtConsultaNomePaciente);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(67, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(845, 115);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Paciente";
            // 
            // txtConsultaDataNascimento
            // 
            this.txtConsultaDataNascimento.Enabled = false;
            this.txtConsultaDataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtConsultaDataNascimento.Location = new System.Drawing.Point(580, 47);
            this.txtConsultaDataNascimento.Name = "txtConsultaDataNascimento";
            this.txtConsultaDataNascimento.Size = new System.Drawing.Size(110, 29);
            this.txtConsultaDataNascimento.TabIndex = 4;
            // 
            // lblIdPaciente
            // 
            this.lblIdPaciente.AutoSize = true;
            this.lblIdPaciente.Location = new System.Drawing.Point(89, 20);
            this.lblIdPaciente.Name = "lblIdPaciente";
            this.lblIdPaciente.Size = new System.Drawing.Size(80, 22);
            this.lblIdPaciente.TabIndex = 0;
            this.lblIdPaciente.Text = "IdPaciente";
            this.lblIdPaciente.Visible = false;
            // 
            // btnConsultaBuscarPaciente
            // 
            this.btnConsultaBuscarPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultaBuscarPaciente.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultaBuscarPaciente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultaBuscarPaciente.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultaBuscarPaciente.Image")));
            this.btnConsultaBuscarPaciente.Location = new System.Drawing.Point(720, 36);
            this.btnConsultaBuscarPaciente.Name = "btnConsultaBuscarPaciente";
            this.btnConsultaBuscarPaciente.Size = new System.Drawing.Size(50, 48);
            this.btnConsultaBuscarPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnConsultaBuscarPaciente.TabIndex = 25;
            this.btnConsultaBuscarPaciente.TabStop = false;
            this.toolTip1.SetToolTip(this.btnConsultaBuscarPaciente, "Buscar Paciente");
            this.btnConsultaBuscarPaciente.Click += new System.EventHandler(this.btnConsultaBuscarPaciente_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(444, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome:";
            // 
            // txtConsultaNomePaciente
            // 
            this.txtConsultaNomePaciente.Enabled = false;
            this.txtConsultaNomePaciente.Location = new System.Drawing.Point(80, 49);
            this.txtConsultaNomePaciente.Name = "txtConsultaNomePaciente";
            this.txtConsultaNomePaciente.Size = new System.Drawing.Size(358, 29);
            this.txtConsultaNomePaciente.TabIndex = 2;
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.AutoSize = true;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(946, -7);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(25, 26);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.Text = "X";
            this.toolTip1.SetToolTip(this.btnFechar, "Fechar");
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // FormNovaConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormNovaConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormNovaConsulta";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Consulta.ResumeLayout(false);
            this.Consulta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultaBuscarPsicologo)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultaBuscarPaciente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFinalizarTerapia;
        private System.Windows.Forms.GroupBox Consulta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox btnConsultaBuscarPaciente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtConsultaNomePaciente;
        private System.Windows.Forms.Label btnFechar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox btnConsultaBuscarPsicologo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPsicologoConsulta;
        private System.Windows.Forms.Label lblIdPaciente;
        private System.Windows.Forms.Label lblPsicologo;
        private System.Windows.Forms.DateTimePicker txtConsultaDataNascimento;
        private System.Windows.Forms.MaskedTextBox txtConsultaHorario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker txtDataConsulta;
        private System.Windows.Forms.MaskedTextBox txtConsultaValor;
    }
}