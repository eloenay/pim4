﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Consultas
{
    public partial class FormDetalhesConsulta : Form
    {
        public FormDetalhesConsulta()
        {
            InitializeComponent();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPequisarConsulta_Click(object sender, EventArgs e)
        {
            Consulta consulta = new Consulta();
            dgvConsulta.DataSource = consulta.BuscarPorPaciente(txtPesquisarConsulta.Text);
        }

        public void LimparTela()
        {
            lblPsicologo.Text = String.Empty;
            lblIdPaciente.Text = String.Empty;
            txtConsultaNomePaciente.Text = String.Empty;
            txtConsultaDataNascimento.Text = String.Empty;
            lblPsicologo.Text = String.Empty;
            txtPsicologoConsulta.Text = String.Empty;
            txtConsultaValor.Text = String.Empty;
            txtDataConsulta.Text = String.Empty;
            txtConsultaHorario.Text = String.Empty;
        }

        private void dgvConsulta_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Consulta consulta = new Consulta();
            int id = (int) dgvConsulta.CurrentRow.Cells["Id"].Value;
            consulta = consulta.BuscarPorId(id);

            lblIdConsulta.Text = consulta.ConsultaId.ToString();
            lblIdPaciente.Text = consulta.PacienteId.ToString();
            txtConsultaNomePaciente.Text = consulta.Paciente.Nome;
            txtConsultaDataNascimento.Text = consulta.Paciente.DataNascimento.ToString();
            lblPsicologo.Text = consulta.PsicologoId.ToString();
            txtPsicologoConsulta.Text = consulta.Psicologo.Nome;
            txtConsultaValor.Text = consulta.Valor.ToString();
            txtDataConsulta.Text = consulta.DataConsulta.ToString();
            txtConsultaHorario.Text = consulta.HorarioConsulta.ToString();
        }

        public void EnableForms()
        {
            //txtPsicologoConsulta.Enabled = false;
            txtConsultaValor.Enabled = false;
            txtDataConsulta.Enabled = false;
            txtConsultaHorario.Enabled = false;
            btnCancelar.Visible = false;
            btnFinalizarConsulta.Visible = false;
            btnExcluirConsulta.Visible = true;
            btnEditarConsulta.Visible = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //txtPsicologoConsulta.Enabled = false;
            txtConsultaValor.Enabled = false;
            txtDataConsulta.Enabled = false;
            txtConsultaHorario.Enabled = false;
            btnCancelar.Visible = false;
            btnFinalizarConsulta.Visible = false;
            btnExcluirConsulta.Visible = true;
            btnEditarConsulta.Visible = true;

            LimparTela();
        }

        private void btnEditarConsulta_Click(object sender, EventArgs e)
        {
            //txtPsicologoConsulta.Enabled = true;
            txtConsultaValor.Enabled = true;
            txtDataConsulta.Enabled = true;
            txtConsultaHorario.Enabled = true;
            btnCancelar.Visible = true;
            btnFinalizarConsulta.Visible = true;
            btnExcluirConsulta.Visible = false;
            btnEditarConsulta.Visible = false;
        }

        private void btnFinalizarConsulta_Click(object sender, EventArgs e)
        {
            try
            {
                Consulta consulta = new Consulta();
                if (txtConsultaNomePaciente.Text == String.Empty || txtPsicologoConsulta.Text == String.Empty ||
                    txtConsultaValor.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios...", "Informação");
                }
                else
                {
                    consulta.ConsultaId = Convert.ToInt32(lblIdConsulta.Text);
                    consulta.PacienteId = Convert.ToInt32(lblIdPaciente.Text);
                    consulta.PsicologoId = Convert.ToInt32(lblPsicologo.Text);
                    consulta.Valor = Convert.ToDecimal(txtConsultaValor.Text);
                    consulta.HorarioConsulta = TimeSpan.Parse(txtConsultaHorario.Text);
                    consulta.DataConsulta = Convert.ToDateTime(txtDataConsulta.Text);

                    if (consulta.DataConsulta >= DateTime.Now.Date)
                    {
                        consulta.Editar(consulta);
                        MessageBox.Show("Consulta Atualizada com sucesso...", "Informação");
                        dgvConsulta.DataSource = consulta.BuscarPorPaciente(txtPesquisarConsulta.Text);
                        EnableForms();                     
                    }
                    else
                    {
                        MessageBox.Show(
                            "Não é possivel cadastrar para dias anteriores a não ser que vc seje o Doutor Estranho.",
                            "Informação");
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }
    }
}
