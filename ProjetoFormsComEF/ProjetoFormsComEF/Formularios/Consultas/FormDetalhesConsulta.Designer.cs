﻿namespace ProjetoIntegrador4.Formularios.Consultas
{
    partial class FormDetalhesConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalhesConsulta));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnFinalizarConsulta = new System.Windows.Forms.Button();
            this.btnExcluirConsulta = new System.Windows.Forms.PictureBox();
            this.btnEditarConsulta = new System.Windows.Forms.PictureBox();
            this.btnPequisarConsulta = new System.Windows.Forms.PictureBox();
            this.btnFechar = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPesquisarConsulta = new System.Windows.Forms.TextBox();
            this.dgvConsulta = new System.Windows.Forms.DataGridView();
            this.Consulta = new System.Windows.Forms.GroupBox();
            this.txtConsultaValor = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDataConsulta = new System.Windows.Forms.DateTimePicker();
            this.txtConsultaHorario = new System.Windows.Forms.MaskedTextBox();
            this.lblPsicologo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPsicologoConsulta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblIdConsulta = new System.Windows.Forms.Label();
            this.txtConsultaDataNascimento = new System.Windows.Forms.DateTimePicker();
            this.lblIdPaciente = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtConsultaNomePaciente = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarConsulta)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsulta)).BeginInit();
            this.Consulta.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(374, 188);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 46);
            this.btnCancelar.TabIndex = 28;
            this.btnCancelar.Text = "Cancelar";
            this.toolTip1.SetToolTip(this.btnCancelar, "Cancelar");
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnFinalizarConsulta
            // 
            this.btnFinalizarConsulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarConsulta.Location = new System.Drawing.Point(142, 188);
            this.btnFinalizarConsulta.Name = "btnFinalizarConsulta";
            this.btnFinalizarConsulta.Size = new System.Drawing.Size(120, 46);
            this.btnFinalizarConsulta.TabIndex = 27;
            this.btnFinalizarConsulta.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarConsulta, "Finalizar editar consulta");
            this.btnFinalizarConsulta.UseVisualStyleBackColor = true;
            this.btnFinalizarConsulta.Visible = false;
            this.btnFinalizarConsulta.Click += new System.EventHandler(this.btnFinalizarConsulta_Click);
            // 
            // btnExcluirConsulta
            // 
            this.btnExcluirConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirConsulta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluirConsulta.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirConsulta.Image")));
            this.btnExcluirConsulta.Location = new System.Drawing.Point(539, 465);
            this.btnExcluirConsulta.Name = "btnExcluirConsulta";
            this.btnExcluirConsulta.Size = new System.Drawing.Size(69, 59);
            this.btnExcluirConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExcluirConsulta.TabIndex = 29;
            this.btnExcluirConsulta.TabStop = false;
            this.toolTip1.SetToolTip(this.btnExcluirConsulta, "Excluir Consulta");
            // 
            // btnEditarConsulta
            // 
            this.btnEditarConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarConsulta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarConsulta.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarConsulta.Image")));
            this.btnEditarConsulta.Location = new System.Drawing.Point(418, 465);
            this.btnEditarConsulta.Name = "btnEditarConsulta";
            this.btnEditarConsulta.Size = new System.Drawing.Size(69, 59);
            this.btnEditarConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditarConsulta.TabIndex = 28;
            this.btnEditarConsulta.TabStop = false;
            this.toolTip1.SetToolTip(this.btnEditarConsulta, "Editar Consulta");
            this.btnEditarConsulta.Click += new System.EventHandler(this.btnEditarConsulta_Click);
            // 
            // btnPequisarConsulta
            // 
            this.btnPequisarConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPequisarConsulta.BackColor = System.Drawing.Color.Transparent;
            this.btnPequisarConsulta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPequisarConsulta.Image = ((System.Drawing.Image)(resources.GetObject("btnPequisarConsulta.Image")));
            this.btnPequisarConsulta.Location = new System.Drawing.Point(938, 54);
            this.btnPequisarConsulta.Name = "btnPequisarConsulta";
            this.btnPequisarConsulta.Size = new System.Drawing.Size(25, 30);
            this.btnPequisarConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPequisarConsulta.TabIndex = 37;
            this.btnPequisarConsulta.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPequisarConsulta, "Pesquisar Psicólogo");
            this.btnPequisarConsulta.Click += new System.EventHandler(this.btnPequisarConsulta_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.AutoSize = true;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(946, -7);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(25, 26);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.Text = "X";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnPequisarConsulta);
            this.groupBox1.Controls.Add(this.txtPesquisarConsulta);
            this.groupBox1.Controls.Add(this.dgvConsulta);
            this.groupBox1.Controls.Add(this.btnExcluirConsulta);
            this.groupBox1.Controls.Add(this.btnEditarConsulta);
            this.groupBox1.Controls.Add(this.Consulta);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFechar);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalhes Consultas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(596, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 22);
            this.label4.TabIndex = 34;
            this.label4.Text = "Pesquisar:";
            // 
            // txtPesquisarConsulta
            // 
            this.txtPesquisarConsulta.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisarConsulta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPesquisarConsulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisarConsulta.Location = new System.Drawing.Point(680, 55);
            this.txtPesquisarConsulta.Name = "txtPesquisarConsulta";
            this.txtPesquisarConsulta.Size = new System.Drawing.Size(252, 29);
            this.txtPesquisarConsulta.TabIndex = 36;
            // 
            // dgvConsulta
            // 
            this.dgvConsulta.AllowUserToAddRows = false;
            this.dgvConsulta.AllowUserToDeleteRows = false;
            this.dgvConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConsulta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvConsulta.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConsulta.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConsulta.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvConsulta.Location = new System.Drawing.Point(599, 90);
            this.dgvConsulta.Name = "dgvConsulta";
            this.dgvConsulta.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConsulta.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvConsulta.Size = new System.Drawing.Size(365, 369);
            this.dgvConsulta.TabIndex = 35;
            this.dgvConsulta.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConsulta_CellClick);
            // 
            // Consulta
            // 
            this.Consulta.Controls.Add(this.btnCancelar);
            this.Consulta.Controls.Add(this.btnFinalizarConsulta);
            this.Consulta.Controls.Add(this.txtConsultaValor);
            this.Consulta.Controls.Add(this.label1);
            this.Consulta.Controls.Add(this.txtDataConsulta);
            this.Consulta.Controls.Add(this.txtConsultaHorario);
            this.Consulta.Controls.Add(this.lblPsicologo);
            this.Consulta.Controls.Add(this.label6);
            this.Consulta.Controls.Add(this.txtPsicologoConsulta);
            this.Consulta.Controls.Add(this.label7);
            this.Consulta.Controls.Add(this.label8);
            this.Consulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consulta.Location = new System.Drawing.Point(11, 206);
            this.Consulta.Name = "Consulta";
            this.Consulta.Size = new System.Drawing.Size(582, 253);
            this.Consulta.TabIndex = 6;
            this.Consulta.TabStop = false;
            this.Consulta.Text = "Consultas";
            // 
            // txtConsultaValor
            // 
            this.txtConsultaValor.Enabled = false;
            this.txtConsultaValor.Location = new System.Drawing.Point(94, 119);
            this.txtConsultaValor.Name = "txtConsultaValor";
            this.txtConsultaValor.Size = new System.Drawing.Size(101, 29);
            this.txtConsultaValor.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(201, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 22);
            this.label1.TabIndex = 5;
            this.label1.Text = "Dia da consulta:";
            // 
            // txtDataConsulta
            // 
            this.txtDataConsulta.Enabled = false;
            this.txtDataConsulta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDataConsulta.Location = new System.Drawing.Point(322, 119);
            this.txtDataConsulta.Name = "txtDataConsulta";
            this.txtDataConsulta.Size = new System.Drawing.Size(99, 29);
            this.txtDataConsulta.TabIndex = 6;
            // 
            // txtConsultaHorario
            // 
            this.txtConsultaHorario.Enabled = false;
            this.txtConsultaHorario.Location = new System.Drawing.Point(497, 119);
            this.txtConsultaHorario.Mask = "00:00";
            this.txtConsultaHorario.Name = "txtConsultaHorario";
            this.txtConsultaHorario.Size = new System.Drawing.Size(68, 29);
            this.txtConsultaHorario.TabIndex = 8;
            this.txtConsultaHorario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtConsultaHorario.ValidatingType = typeof(System.DateTime);
            // 
            // lblPsicologo
            // 
            this.lblPsicologo.AutoSize = true;
            this.lblPsicologo.Location = new System.Drawing.Point(90, 42);
            this.lblPsicologo.Name = "lblPsicologo";
            this.lblPsicologo.Size = new System.Drawing.Size(88, 22);
            this.lblPsicologo.TabIndex = 0;
            this.lblPsicologo.Text = "IdPsicologo";
            this.lblPsicologo.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 22);
            this.label6.TabIndex = 1;
            this.label6.Text = "Psicólogo:";
            // 
            // txtPsicologoConsulta
            // 
            this.txtPsicologoConsulta.Enabled = false;
            this.txtPsicologoConsulta.Location = new System.Drawing.Point(94, 70);
            this.txtPsicologoConsulta.Name = "txtPsicologoConsulta";
            this.txtPsicologoConsulta.Size = new System.Drawing.Size(471, 29);
            this.txtPsicologoConsulta.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(427, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 22);
            this.label7.TabIndex = 7;
            this.label7.Text = "Horário:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 22);
            this.label8.TabIndex = 3;
            this.label8.Text = "Valor:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblIdConsulta);
            this.groupBox2.Controls.Add(this.txtConsultaDataNascimento);
            this.groupBox2.Controls.Add(this.lblIdPaciente);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtConsultaNomePaciente);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(11, 51);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(582, 149);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Paciente";
            // 
            // lblIdConsulta
            // 
            this.lblIdConsulta.AutoSize = true;
            this.lblIdConsulta.Location = new System.Drawing.Point(485, 114);
            this.lblIdConsulta.Name = "lblIdConsulta";
            this.lblIdConsulta.Size = new System.Drawing.Size(84, 22);
            this.lblIdConsulta.TabIndex = 26;
            this.lblIdConsulta.Text = "IdConsulta";
            this.lblIdConsulta.Visible = false;
            // 
            // txtConsultaDataNascimento
            // 
            this.txtConsultaDataNascimento.Enabled = false;
            this.txtConsultaDataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtConsultaDataNascimento.Location = new System.Drawing.Point(154, 98);
            this.txtConsultaDataNascimento.Name = "txtConsultaDataNascimento";
            this.txtConsultaDataNascimento.Size = new System.Drawing.Size(110, 29);
            this.txtConsultaDataNascimento.TabIndex = 4;
            // 
            // lblIdPaciente
            // 
            this.lblIdPaciente.AutoSize = true;
            this.lblIdPaciente.Location = new System.Drawing.Point(89, 20);
            this.lblIdPaciente.Name = "lblIdPaciente";
            this.lblIdPaciente.Size = new System.Drawing.Size(80, 22);
            this.lblIdPaciente.TabIndex = 0;
            this.lblIdPaciente.Text = "IdPaciente";
            this.lblIdPaciente.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "Data Nascimento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome:";
            // 
            // txtConsultaNomePaciente
            // 
            this.txtConsultaNomePaciente.Enabled = false;
            this.txtConsultaNomePaciente.Location = new System.Drawing.Point(80, 49);
            this.txtConsultaNomePaciente.Name = "txtConsultaNomePaciente";
            this.txtConsultaNomePaciente.Size = new System.Drawing.Size(485, 29);
            this.txtConsultaNomePaciente.TabIndex = 2;
            // 
            // FormDetalhesConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormDetalhesConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormEditarConsulta";
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPequisarConsulta)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsulta)).EndInit();
            this.Consulta.ResumeLayout(false);
            this.Consulta.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label btnFechar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox Consulta;
        private System.Windows.Forms.MaskedTextBox txtConsultaValor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker txtDataConsulta;
        private System.Windows.Forms.MaskedTextBox txtConsultaHorario;
        private System.Windows.Forms.Label lblPsicologo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPsicologoConsulta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker txtConsultaDataNascimento;
        private System.Windows.Forms.Label lblIdPaciente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtConsultaNomePaciente;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnFinalizarConsulta;
        private System.Windows.Forms.PictureBox btnExcluirConsulta;
        private System.Windows.Forms.PictureBox btnEditarConsulta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox btnPequisarConsulta;
        private System.Windows.Forms.TextBox txtPesquisarConsulta;
        private System.Windows.Forms.DataGridView dgvConsulta;
        private System.Windows.Forms.Label lblIdConsulta;
    }
}