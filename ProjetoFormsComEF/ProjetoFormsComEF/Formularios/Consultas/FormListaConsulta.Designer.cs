﻿namespace ProjetoIntegrador4.Formularios.Exame
{
    partial class FormListaConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaConsulta));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDetalhesClinica = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNovaConsulta = new System.Windows.Forms.PictureBox();
            this.btnVoltaConsultas = new System.Windows.Forms.PictureBox();
            this.btnPesquisarConsulta = new System.Windows.Forms.PictureBox();
            this.txtBuscarConsulta = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.rbPaciente = new System.Windows.Forms.RadioButton();
            this.rbPsicologo = new System.Windows.Forms.RadioButton();
            this.dgvConsultas = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaConsultas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.dgvConsultas);
            this.groupBox1.Controls.Add(this.rbPsicologo);
            this.groupBox1.Controls.Add(this.rbPaciente);
            this.groupBox1.Controls.Add(this.btnDetalhesClinica);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnNovaConsulta);
            this.groupBox1.Controls.Add(this.btnVoltaConsultas);
            this.groupBox1.Controls.Add(this.btnPesquisarConsulta);
            this.groupBox1.Controls.Add(this.txtBuscarConsulta);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consultas";
            // 
            // btnDetalhesClinica
            // 
            this.btnDetalhesClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalhesClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalhesClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnDetalhesClinica.Image")));
            this.btnDetalhesClinica.Location = new System.Drawing.Point(473, 466);
            this.btnDetalhesClinica.Name = "btnDetalhesClinica";
            this.btnDetalhesClinica.Size = new System.Drawing.Size(73, 59);
            this.btnDetalhesClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetalhesClinica.TabIndex = 27;
            this.btnDetalhesClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnDetalhesClinica, "Detalhes da Clinica");
            this.btnDetalhesClinica.Click += new System.EventHandler(this.btnDetalhesClinica_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 26);
            this.label1.TabIndex = 24;
            this.label1.Text = "Pesquisar:";
            // 
            // btnNovaConsulta
            // 
            this.btnNovaConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovaConsulta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovaConsulta.Image = ((System.Drawing.Image)(resources.GetObject("btnNovaConsulta.Image")));
            this.btnNovaConsulta.Location = new System.Drawing.Point(919, 62);
            this.btnNovaConsulta.Name = "btnNovaConsulta";
            this.btnNovaConsulta.Size = new System.Drawing.Size(49, 45);
            this.btnNovaConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovaConsulta.TabIndex = 16;
            this.btnNovaConsulta.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovaConsulta, "Nova Consulta");
            this.btnNovaConsulta.Click += new System.EventHandler(this.btnNovaConsulta_Click);
            // 
            // btnVoltaConsultas
            // 
            this.btnVoltaConsultas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltaConsultas.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltaConsultas.Image")));
            this.btnVoltaConsultas.Location = new System.Drawing.Point(11, 71);
            this.btnVoltaConsultas.Name = "btnVoltaConsultas";
            this.btnVoltaConsultas.Size = new System.Drawing.Size(48, 36);
            this.btnVoltaConsultas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltaConsultas.TabIndex = 17;
            this.btnVoltaConsultas.TabStop = false;
            this.toolTip1.SetToolTip(this.btnVoltaConsultas, "Voltar");
            this.btnVoltaConsultas.Click += new System.EventHandler(this.btnVoltaConsultas_Click);
            // 
            // btnPesquisarConsulta
            // 
            this.btnPesquisarConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisarConsulta.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisarConsulta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarConsulta.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarConsulta.Image")));
            this.btnPesquisarConsulta.Location = new System.Drawing.Point(919, 154);
            this.btnPesquisarConsulta.Name = "btnPesquisarConsulta";
            this.btnPesquisarConsulta.Size = new System.Drawing.Size(50, 48);
            this.btnPesquisarConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPesquisarConsulta.TabIndex = 24;
            this.btnPesquisarConsulta.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPesquisarConsulta, "Pesquisar Consulta");
            this.btnPesquisarConsulta.Click += new System.EventHandler(this.btnPesquisarConsulta_Click);
            // 
            // txtBuscarConsulta
            // 
            this.txtBuscarConsulta.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscarConsulta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarConsulta.Location = new System.Drawing.Point(112, 169);
            this.txtBuscarConsulta.Name = "txtBuscarConsulta";
            this.txtBuscarConsulta.Size = new System.Drawing.Size(567, 29);
            this.txtBuscarConsulta.TabIndex = 22;
            // 
            // rbPaciente
            // 
            this.rbPaciente.AutoSize = true;
            this.rbPaciente.Font = new System.Drawing.Font("Palatino Linotype", 14.25F);
            this.rbPaciente.Location = new System.Drawing.Point(699, 170);
            this.rbPaciente.Name = "rbPaciente";
            this.rbPaciente.Size = new System.Drawing.Size(98, 30);
            this.rbPaciente.TabIndex = 28;
            this.rbPaciente.TabStop = true;
            this.rbPaciente.Text = "Paciente";
            this.rbPaciente.UseVisualStyleBackColor = true;
            // 
            // rbPsicologo
            // 
            this.rbPsicologo.AutoSize = true;
            this.rbPsicologo.Font = new System.Drawing.Font("Palatino Linotype", 14.25F);
            this.rbPsicologo.Location = new System.Drawing.Point(803, 170);
            this.rbPsicologo.Name = "rbPsicologo";
            this.rbPsicologo.Size = new System.Drawing.Size(108, 30);
            this.rbPsicologo.TabIndex = 29;
            this.rbPsicologo.TabStop = true;
            this.rbPsicologo.Text = "Psicólogo";
            this.rbPsicologo.UseVisualStyleBackColor = true;
            // 
            // dgvConsultas
            // 
            this.dgvConsultas.AllowUserToAddRows = false;
            this.dgvConsultas.AllowUserToDeleteRows = false;
            this.dgvConsultas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConsultas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvConsultas.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConsultas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConsultas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConsultas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvConsultas.Location = new System.Drawing.Point(6, 204);
            this.dgvConsultas.Name = "dgvConsultas";
            this.dgvConsultas.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConsultas.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvConsultas.Size = new System.Drawing.Size(962, 256);
            this.dgvConsultas.TabIndex = 30;
            // 
            // FormListaConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(205, 90);
            this.Name = "FormListaConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormExameLista";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltaConsultas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox btnNovaConsulta;
        private System.Windows.Forms.PictureBox btnVoltaConsultas;
        private System.Windows.Forms.PictureBox btnPesquisarConsulta;
        private System.Windows.Forms.TextBox txtBuscarConsulta;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btnDetalhesClinica;
        private System.Windows.Forms.RadioButton rbPsicologo;
        private System.Windows.Forms.RadioButton rbPaciente;
        private System.Windows.Forms.DataGridView dgvConsultas;
    }
}