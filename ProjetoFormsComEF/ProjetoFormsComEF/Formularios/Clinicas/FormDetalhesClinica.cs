﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Formularios.Clinicas
{
    public partial class FormDetalhesClinica : Form
    {
        public FormDetalhesClinica()
        {
            InitializeComponent();
        }


        #region Funcionalidades

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            VisibleTxt();
            LimparCampos();
        }

        private void btnFecharDetalhes_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void VisibleTxt()
        {
            txtDetalhesNomeClinica.Enabled = false;
            txtDetalhesEnderecoClinica.Enabled = false;
            txtDetalhesNumeroClinica.Enabled = false;
            txtDetalhesBairroClinica.Enabled = false;
            txtDetalhesCepClinica.Enabled = false;
            txtDetalhesCidadeClinica.Enabled = false;
            txtDetalhesEmailClinica.Enabled = false;
            cbxEstadoClinica.Enabled = false;
            btnFinalizar.Visible = false;
            btnCancelar.Visible = false;
            btnEditarClinica.Visible = true;
            btnExcluirClinica.Visible = true;
        }

        public void LimparCampos()
        {
            lblIdDetalhesClinica.Text = String.Empty;
            txtDetalhesNomeClinica.Text = String.Empty;
            txtDetalhesBairroClinica.Text = String.Empty;
            txtDetalhesCepClinica.Text = String.Empty;
            txtDetalhesCidadeClinica.Text = String.Empty;
            txtDetalhesEmailClinica.Text = String.Empty;
            txtDetalhesEnderecoClinica.Text = String.Empty;
            txtDetalhesNumeroClinica.Text = String.Empty;
        } 

        private void btnEditarClinica_Click(object sender, EventArgs e)
        {
            txtDetalhesNomeClinica.Enabled = true;
            txtDetalhesEnderecoClinica.Enabled = true;
            txtDetalhesNumeroClinica.Enabled = true;
            txtDetalhesBairroClinica.Enabled = true;
            txtDetalhesCepClinica.Enabled = true;
            txtDetalhesCidadeClinica.Enabled = true;
            txtDetalhesEmailClinica.Enabled = true;
            cbxEstadoClinica.Enabled = true;
            btnFinalizar.Visible = true;
            btnCancelar.Visible = true;
            btnEditarClinica.Visible = false;
            btnExcluirClinica.Visible = false;
        }

        private void btnPesquisarClinica_Click(object sender, EventArgs e)
        {
            Clinica clinica = new Clinica();
            dgvClinicas.DataSource = clinica.BuscarPorNome(txtBuscaClinica.Text);

            if (dgvClinicas == null)
            {
                MessageBox.Show("Não a clinicas salvas");
            }
        }

        #endregion

        #region Crud

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                Clinica clinica = new Clinica();
                if (txtDetalhesNomeClinica.Text == String.Empty || txtDetalhesEnderecoClinica.Text == String.Empty || txtDetalhesNumeroClinica.Text == String.Empty || txtDetalhesEmailClinica.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    clinica.ClinicaId = Convert.ToInt32(lblIdDetalhesClinica.Text);
                    clinica.Nome = txtDetalhesNomeClinica.Text;
                    clinica.Endereco = txtDetalhesEnderecoClinica.Text;
                    clinica.Numero = Convert.ToInt32(txtDetalhesNumeroClinica.Text);
                    clinica.Bairro = txtDetalhesBairroClinica.Text;
                    clinica.Cep = txtDetalhesCepClinica.Text;
                    clinica.Estado = (Estado)cbxEstadoClinica.SelectedValue;
                    clinica.Cidade = txtDetalhesCidadeClinica.Text;
                    clinica.Email = txtDetalhesEmailClinica.Text;
                    if (txtDetalhesCepClinica.Text.Length < 9)
                    {
                        MessageBox.Show("Preencha corretamente o cep", "Informação");
                    }
                    else
                    {
                        if (txtDetalhesEmailClinica.Text.Contains("@"))
                        {
                            clinica.Salvar(clinica);
                            MessageBox.Show("Clinica atualizada com sucesso!!!");

                            dgvClinicas.DataSource = clinica.BuscarPorNome(txtBuscaClinica.Text);
                            VisibleTxt();
                            LimparCampos();
                        }
                        else
                        {
                            MessageBox.Show("Tipo incorreto de email", "Informação");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnExcluirClinica_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDetalhesNomeClinica.Text == String.Empty)
                {
                    MessageBox.Show("Selecione uma clinica para esse procedimento", "Informação");
                }
                else
                {
                    Clinica clinica = new Clinica();
                    clinica.ClinicaId = Convert.ToInt32(lblIdDetalhesClinica.Text);
                    DialogResult dr = MessageBox.Show("Deseja mesmo excluir a clinica", "Excluir", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        clinica.Deletar(clinica);
                        MessageBox.Show("Clinica excluida com sucesso...", "Informação");
                        LimparCampos();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvClinicas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Clinica clinica = new Clinica();
            int id = (int)dgvClinicas.CurrentRow.Cells["Id"].Value;
            clinica = clinica.BuscarPorId(id);

            lblIdDetalhesClinica.Text = clinica.ClinicaId.ToString();
            txtDetalhesNomeClinica.Text = clinica.Nome;
            txtDetalhesEnderecoClinica.Text = clinica.Endereco;
            txtDetalhesNumeroClinica.Text = clinica.Numero.ToString();
            txtDetalhesBairroClinica.Text = clinica.Bairro;
            txtDetalhesCepClinica.Text = clinica.Cep;
            cbxEstadoClinica.Text = clinica.Estado.ToString();
            txtDetalhesCidadeClinica.Text = clinica.Cidade;
            txtDetalhesEmailClinica.Text = clinica.Email;
        }

        #endregion

        #region Preencher combobox

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void FormDetalhesClinica_Load(object sender, EventArgs e)
        {
            cbxEstadoClinica.DataSource = Listar(typeof(Estado));
            cbxEstadoClinica.DisplayMember = "Value";
            cbxEstadoClinica.ValueMember = "Key";
        }



        #endregion
    }
}
