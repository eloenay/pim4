﻿namespace ProjetoIntegrador4.Formularios.Clinicas
{
    partial class FormListaClinica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaClinica));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDetalhesClinica = new System.Windows.Forms.PictureBox();
            this.btnNovaClinica = new System.Windows.Forms.PictureBox();
            this.btnVoltarClinicas = new System.Windows.Forms.PictureBox();
            this.btnPesquisarClinica = new System.Windows.Forms.PictureBox();
            this.txtBuscaClinica = new System.Windows.Forms.TextBox();
            this.dgvClinicas = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltarClinicas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClinicas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDetalhesClinica);
            this.groupBox1.Controls.Add(this.btnNovaClinica);
            this.groupBox1.Controls.Add(this.btnVoltarClinicas);
            this.groupBox1.Controls.Add(this.btnPesquisarClinica);
            this.groupBox1.Controls.Add(this.txtBuscaClinica);
            this.groupBox1.Controls.Add(this.dgvClinicas);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Clínicas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 26);
            this.label1.TabIndex = 24;
            this.label1.Text = "Pesquisar:";
            // 
            // btnDetalhesClinica
            // 
            this.btnDetalhesClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalhesClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalhesClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnDetalhesClinica.Image")));
            this.btnDetalhesClinica.Location = new System.Drawing.Point(473, 466);
            this.btnDetalhesClinica.Name = "btnDetalhesClinica";
            this.btnDetalhesClinica.Size = new System.Drawing.Size(73, 59);
            this.btnDetalhesClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetalhesClinica.TabIndex = 26;
            this.btnDetalhesClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnDetalhesClinica, "Detalhes da Clinica");
            this.btnDetalhesClinica.Click += new System.EventHandler(this.btnDetalhesClinica_Click);
            // 
            // btnNovaClinica
            // 
            this.btnNovaClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovaClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNovaClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnNovaClinica.Image")));
            this.btnNovaClinica.Location = new System.Drawing.Point(919, 62);
            this.btnNovaClinica.Name = "btnNovaClinica";
            this.btnNovaClinica.Size = new System.Drawing.Size(49, 45);
            this.btnNovaClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnNovaClinica.TabIndex = 16;
            this.btnNovaClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNovaClinica, "Nova Clinica");
            this.btnNovaClinica.Click += new System.EventHandler(this.btnNovaClinica_Click);
            // 
            // btnVoltarClinicas
            // 
            this.btnVoltarClinicas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltarClinicas.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltarClinicas.Image")));
            this.btnVoltarClinicas.Location = new System.Drawing.Point(11, 71);
            this.btnVoltarClinicas.Name = "btnVoltarClinicas";
            this.btnVoltarClinicas.Size = new System.Drawing.Size(48, 36);
            this.btnVoltarClinicas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltarClinicas.TabIndex = 17;
            this.btnVoltarClinicas.TabStop = false;
            this.toolTip1.SetToolTip(this.btnVoltarClinicas, "Voltar");
            this.btnVoltarClinicas.Click += new System.EventHandler(this.btnVoltarClinicas_Click);
            // 
            // btnPesquisarClinica
            // 
            this.btnPesquisarClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisarClinica.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisarClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarClinica.Image")));
            this.btnPesquisarClinica.Location = new System.Drawing.Point(919, 154);
            this.btnPesquisarClinica.Name = "btnPesquisarClinica";
            this.btnPesquisarClinica.Size = new System.Drawing.Size(50, 48);
            this.btnPesquisarClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPesquisarClinica.TabIndex = 24;
            this.btnPesquisarClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPesquisarClinica, "Pesquisar Clinica");
            this.btnPesquisarClinica.Click += new System.EventHandler(this.btnPesquisarClinica_Click);
            // 
            // txtBuscaClinica
            // 
            this.txtBuscaClinica.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscaClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscaClinica.Location = new System.Drawing.Point(112, 169);
            this.txtBuscaClinica.Name = "txtBuscaClinica";
            this.txtBuscaClinica.Size = new System.Drawing.Size(781, 29);
            this.txtBuscaClinica.TabIndex = 22;
            // 
            // dgvClinicas
            // 
            this.dgvClinicas.AllowUserToAddRows = false;
            this.dgvClinicas.AllowUserToDeleteRows = false;
            this.dgvClinicas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvClinicas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvClinicas.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvClinicas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvClinicas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvClinicas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvClinicas.Location = new System.Drawing.Point(6, 204);
            this.dgvClinicas.Name = "dgvClinicas";
            this.dgvClinicas.ReadOnly = true;
            this.dgvClinicas.Size = new System.Drawing.Size(962, 256);
            this.dgvClinicas.TabIndex = 21;
            // 
            // FormListaClinica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(205, 90);
            this.Name = "FormListaClinica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "formClinicaLista";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetalhesClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNovaClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltarClinicas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClinicas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox btnDetalhesClinica;
        private System.Windows.Forms.PictureBox btnNovaClinica;
        private System.Windows.Forms.PictureBox btnVoltarClinicas;
        private System.Windows.Forms.PictureBox btnPesquisarClinica;
        private System.Windows.Forms.TextBox txtBuscaClinica;
        private System.Windows.Forms.DataGridView dgvClinicas;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
    }
}