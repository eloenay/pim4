﻿namespace ProjetoIntegrador4.Formularios.Clinicas
{
    partial class FormDetalhesClinica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalhesClinica));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDetalhesCepClinica = new System.Windows.Forms.MaskedTextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.cbxEstadoClinica = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPesquisarClinica = new System.Windows.Forms.PictureBox();
            this.txtBuscaClinica = new System.Windows.Forms.TextBox();
            this.dgvClinicas = new System.Windows.Forms.DataGridView();
            this.lblIdDetalhesClinica = new System.Windows.Forms.Label();
            this.txtDetalhesEmailClinica = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDetalhesCidadeClinica = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDetalhesNumeroClinica = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDetalhesEnderecoClinica = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDetalhesBairroClinica = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDetalhesNomeClinica = new System.Windows.Forms.TextBox();
            this.btnFecharDetalhes = new System.Windows.Forms.Label();
            this.btnExcluirClinica = new System.Windows.Forms.PictureBox();
            this.btnEditarClinica = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClinicas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirClinica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarClinica)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnFecharDetalhes);
            this.groupBox1.Controls.Add(this.btnExcluirClinica);
            this.groupBox1.Controls.Add(this.btnEditarClinica);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalhes da Clínica";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDetalhesCepClinica);
            this.groupBox2.Controls.Add(this.btnCancelar);
            this.groupBox2.Controls.Add(this.btnFinalizar);
            this.groupBox2.Controls.Add(this.cbxEstadoClinica);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnPesquisarClinica);
            this.groupBox2.Controls.Add(this.txtBuscaClinica);
            this.groupBox2.Controls.Add(this.dgvClinicas);
            this.groupBox2.Controls.Add(this.lblIdDetalhesClinica);
            this.groupBox2.Controls.Add(this.txtDetalhesEmailClinica);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtDetalhesCidadeClinica);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtDetalhesNumeroClinica);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtDetalhesEnderecoClinica);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtDetalhesBairroClinica);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtDetalhesNomeClinica);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(10, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(954, 410);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Clínica";
            // 
            // txtDetalhesCepClinica
            // 
            this.txtDetalhesCepClinica.Enabled = false;
            this.txtDetalhesCepClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesCepClinica.Location = new System.Drawing.Point(465, 164);
            this.txtDetalhesCepClinica.Mask = "00000-000";
            this.txtDetalhesCepClinica.Name = "txtDetalhesCepClinica";
            this.txtDetalhesCepClinica.Size = new System.Drawing.Size(154, 29);
            this.txtDetalhesCepClinica.TabIndex = 33;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(367, 341);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(122, 44);
            this.btnCancelar.TabIndex = 32;
            this.btnCancelar.Text = "Cancelar";
            this.toolTip1.SetToolTip(this.btnCancelar, "Cancelar");
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizar.Location = new System.Drawing.Point(124, 341);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(122, 44);
            this.btnFinalizar.TabIndex = 31;
            this.btnFinalizar.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizar, "Finalizar Editar Clínica");
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Visible = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // cbxEstadoClinica
            // 
            this.cbxEstadoClinica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxEstadoClinica.Enabled = false;
            this.cbxEstadoClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.cbxEstadoClinica.FormattingEnabled = true;
            this.cbxEstadoClinica.Location = new System.Drawing.Point(74, 222);
            this.cbxEstadoClinica.Name = "cbxEstadoClinica";
            this.cbxEstadoClinica.Size = new System.Drawing.Size(117, 30);
            this.cbxEstadoClinica.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(642, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 26);
            this.label1.TabIndex = 27;
            this.label1.Text = "Pesquisar:";
            // 
            // btnPesquisarClinica
            // 
            this.btnPesquisarClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisarClinica.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisarClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarClinica.Image")));
            this.btnPesquisarClinica.Location = new System.Drawing.Point(920, 20);
            this.btnPesquisarClinica.Name = "btnPesquisarClinica";
            this.btnPesquisarClinica.Size = new System.Drawing.Size(28, 29);
            this.btnPesquisarClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPesquisarClinica.TabIndex = 28;
            this.btnPesquisarClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnPesquisarClinica, "Pesquisar Clinica");
            this.btnPesquisarClinica.Click += new System.EventHandler(this.btnPesquisarClinica_Click);
            // 
            // txtBuscaClinica
            // 
            this.txtBuscaClinica.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBuscaClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscaClinica.Location = new System.Drawing.Point(752, 20);
            this.txtBuscaClinica.Name = "txtBuscaClinica";
            this.txtBuscaClinica.Size = new System.Drawing.Size(162, 29);
            this.txtBuscaClinica.TabIndex = 26;
            // 
            // dgvClinicas
            // 
            this.dgvClinicas.AllowUserToAddRows = false;
            this.dgvClinicas.AllowUserToDeleteRows = false;
            this.dgvClinicas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvClinicas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvClinicas.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvClinicas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvClinicas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvClinicas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvClinicas.Location = new System.Drawing.Point(647, 55);
            this.dgvClinicas.Name = "dgvClinicas";
            this.dgvClinicas.ReadOnly = true;
            this.dgvClinicas.Size = new System.Drawing.Size(301, 349);
            this.dgvClinicas.TabIndex = 25;
            this.dgvClinicas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClinicas_CellClick);
            // 
            // lblIdDetalhesClinica
            // 
            this.lblIdDetalhesClinica.AutoSize = true;
            this.lblIdDetalhesClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.lblIdDetalhesClinica.Location = new System.Drawing.Point(6, 29);
            this.lblIdDetalhesClinica.Name = "lblIdDetalhesClinica";
            this.lblIdDetalhesClinica.Size = new System.Drawing.Size(24, 22);
            this.lblIdDetalhesClinica.TabIndex = 23;
            this.lblIdDetalhesClinica.Text = "Id";
            this.lblIdDetalhesClinica.Visible = false;
            // 
            // txtDetalhesEmailClinica
            // 
            this.txtDetalhesEmailClinica.Enabled = false;
            this.txtDetalhesEmailClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesEmailClinica.Location = new System.Drawing.Point(83, 276);
            this.txtDetalhesEmailClinica.Name = "txtDetalhesEmailClinica";
            this.txtDetalhesEmailClinica.Size = new System.Drawing.Size(488, 29);
            this.txtDetalhesEmailClinica.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label9.Location = new System.Drawing.Point(8, 276);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 22);
            this.label9.TabIndex = 21;
            this.label9.Text = "Email:";
            // 
            // txtDetalhesCidadeClinica
            // 
            this.txtDetalhesCidadeClinica.Enabled = false;
            this.txtDetalhesCidadeClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesCidadeClinica.Location = new System.Drawing.Point(277, 225);
            this.txtDetalhesCidadeClinica.Name = "txtDetalhesCidadeClinica";
            this.txtDetalhesCidadeClinica.Size = new System.Drawing.Size(294, 29);
            this.txtDetalhesCidadeClinica.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label8.Location = new System.Drawing.Point(197, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 22);
            this.label8.TabIndex = 18;
            this.label8.Text = "Cidade:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label7.Location = new System.Drawing.Point(8, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 22);
            this.label7.TabIndex = 16;
            this.label7.Text = "Estado:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label5.Location = new System.Drawing.Point(418, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 22);
            this.label5.TabIndex = 14;
            this.label5.Text = "Cep:";
            // 
            // txtDetalhesNumeroClinica
            // 
            this.txtDetalhesNumeroClinica.Enabled = false;
            this.txtDetalhesNumeroClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesNumeroClinica.Location = new System.Drawing.Point(511, 116);
            this.txtDetalhesNumeroClinica.Name = "txtDetalhesNumeroClinica";
            this.txtDetalhesNumeroClinica.Size = new System.Drawing.Size(108, 29);
            this.txtDetalhesNumeroClinica.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label4.Location = new System.Drawing.Point(418, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 22);
            this.label4.TabIndex = 12;
            this.label4.Text = "Número:";
            // 
            // txtDetalhesEnderecoClinica
            // 
            this.txtDetalhesEnderecoClinica.Enabled = false;
            this.txtDetalhesEnderecoClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesEnderecoClinica.Location = new System.Drawing.Point(108, 109);
            this.txtDetalhesEnderecoClinica.Name = "txtDetalhesEnderecoClinica";
            this.txtDetalhesEnderecoClinica.Size = new System.Drawing.Size(295, 29);
            this.txtDetalhesEnderecoClinica.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label6.Location = new System.Drawing.Point(8, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 22);
            this.label6.TabIndex = 9;
            this.label6.Text = "Bairro:";
            // 
            // txtDetalhesBairroClinica
            // 
            this.txtDetalhesBairroClinica.Enabled = false;
            this.txtDetalhesBairroClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesBairroClinica.Location = new System.Drawing.Point(83, 167);
            this.txtDetalhesBairroClinica.Name = "txtDetalhesBairroClinica";
            this.txtDetalhesBairroClinica.Size = new System.Drawing.Size(320, 29);
            this.txtDetalhesBairroClinica.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label3.Location = new System.Drawing.Point(8, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Endereço:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.label2.Location = new System.Drawing.Point(8, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome:";
            // 
            // txtDetalhesNomeClinica
            // 
            this.txtDetalhesNomeClinica.Enabled = false;
            this.txtDetalhesNomeClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.txtDetalhesNomeClinica.Location = new System.Drawing.Point(83, 54);
            this.txtDetalhesNomeClinica.Name = "txtDetalhesNomeClinica";
            this.txtDetalhesNomeClinica.Size = new System.Drawing.Size(536, 29);
            this.txtDetalhesNomeClinica.TabIndex = 1;
            // 
            // btnFecharDetalhes
            // 
            this.btnFecharDetalhes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFecharDetalhes.AutoSize = true;
            this.btnFecharDetalhes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnFecharDetalhes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFecharDetalhes.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFecharDetalhes.Location = new System.Drawing.Point(946, -7);
            this.btnFecharDetalhes.Name = "btnFecharDetalhes";
            this.btnFecharDetalhes.Size = new System.Drawing.Size(25, 26);
            this.btnFecharDetalhes.TabIndex = 28;
            this.btnFecharDetalhes.Text = "X";
            this.toolTip1.SetToolTip(this.btnFecharDetalhes, "Fechar");
            this.btnFecharDetalhes.Click += new System.EventHandler(this.btnFecharDetalhes_Click);
            // 
            // btnExcluirClinica
            // 
            this.btnExcluirClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluirClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirClinica.Image")));
            this.btnExcluirClinica.Location = new System.Drawing.Point(523, 466);
            this.btnExcluirClinica.Name = "btnExcluirClinica";
            this.btnExcluirClinica.Size = new System.Drawing.Size(69, 59);
            this.btnExcluirClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExcluirClinica.TabIndex = 27;
            this.btnExcluirClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnExcluirClinica, "Excluir Clínica");
            this.btnExcluirClinica.Click += new System.EventHandler(this.btnExcluirClinica_Click);
            // 
            // btnEditarClinica
            // 
            this.btnEditarClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarClinica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarClinica.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarClinica.Image")));
            this.btnEditarClinica.Location = new System.Drawing.Point(402, 466);
            this.btnEditarClinica.Name = "btnEditarClinica";
            this.btnEditarClinica.Size = new System.Drawing.Size(69, 59);
            this.btnEditarClinica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditarClinica.TabIndex = 26;
            this.btnEditarClinica.TabStop = false;
            this.toolTip1.SetToolTip(this.btnEditarClinica, "Editar Clínica");
            this.btnEditarClinica.Click += new System.EventHandler(this.btnEditarClinica_Click);
            // 
            // FormDetalhesClinica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormDetalhesClinica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormDetalhesClinica";
            this.Load += new System.EventHandler(this.FormDetalhesClinica_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPesquisarClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClinicas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcluirClinica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditarClinica)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label btnFecharDetalhes;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox btnExcluirClinica;
        private System.Windows.Forms.PictureBox btnEditarClinica;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDetalhesEmailClinica;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDetalhesCidadeClinica;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDetalhesNumeroClinica;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDetalhesEnderecoClinica;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDetalhesBairroClinica;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDetalhesNomeClinica;
        private System.Windows.Forms.Label lblIdDetalhesClinica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btnPesquisarClinica;
        private System.Windows.Forms.TextBox txtBuscaClinica;
        private System.Windows.Forms.DataGridView dgvClinicas;
        private System.Windows.Forms.ComboBox cbxEstadoClinica;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.MaskedTextBox txtDetalhesCepClinica;
    }
}