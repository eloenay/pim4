﻿using System;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;

namespace ProjetoIntegrador4.Formularios.Clinicas
{
    public partial class FormListaClinica : Form
    {
        private FormNovaClinica formNovaClinica;
        private FormDetalhesClinica formDetalhesClinica;

        public FormListaClinica()
        {
            InitializeComponent();
            CarregarDados();
        }

        private void btnVoltarClinicas_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNovaClinica_Click(object sender, EventArgs e)
        {
            formNovaClinica = new FormNovaClinica();
            formNovaClinica.Show();
        }

        private void btnDetalhesClinica_Click(object sender, EventArgs e)
        {
            formDetalhesClinica = new FormDetalhesClinica();
            formDetalhesClinica.Show();
        }

        public void CarregarDados()
        {
            Clinica clinica = new Clinica();
            dgvClinicas.DataSource = clinica.BuscarPorNome(txtBuscaClinica.Text);
        }

        private void btnPesquisarClinica_Click(object sender, EventArgs e)
        {
            Clinica clinica = new Clinica();
            dgvClinicas.DataSource = clinica.BuscarPorNome(txtBuscaClinica.Text);

            if (dgvClinicas == null)
            {
                MessageBox.Show("Não a clinicas salvas");
            }
        }
    }
}
