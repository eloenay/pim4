﻿namespace ProjetoIntegrador4.Formularios.Clinicas
{
    partial class FormNovaClinica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFinalizarClinica = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCepClinica = new System.Windows.Forms.MaskedTextBox();
            this.txtEmailClinica = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbxEstadoClinica = new System.Windows.Forms.ComboBox();
            this.txtCidadeClinica = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNumeroClinica = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEnderecoClinica = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBairroClinica = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomeClinica = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.btnFinalizarClinica);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 531);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cadastro de Clínicas";
            // 
            // btnFinalizarClinica
            // 
            this.btnFinalizarClinica.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarClinica.Location = new System.Drawing.Point(412, 455);
            this.btnFinalizarClinica.Name = "btnFinalizarClinica";
            this.btnFinalizarClinica.Size = new System.Drawing.Size(142, 64);
            this.btnFinalizarClinica.TabIndex = 2;
            this.btnFinalizarClinica.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFinalizarClinica, "Finalizar Nova Clínica");
            this.btnFinalizarClinica.UseVisualStyleBackColor = true;
            this.btnFinalizarClinica.Click += new System.EventHandler(this.btnFinalizarClinica_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCepClinica);
            this.groupBox2.Controls.Add(this.txtEmailClinica);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.cbxEstadoClinica);
            this.groupBox2.Controls.Add(this.txtCidadeClinica);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtNumeroClinica);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtEnderecoClinica);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtBairroClinica);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNomeClinica);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(74, 79);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(822, 355);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Clínica";
            // 
            // txtCepClinica
            // 
            this.txtCepClinica.Location = new System.Drawing.Point(589, 161);
            this.txtCepClinica.Mask = "00000-000";
            this.txtCepClinica.Name = "txtCepClinica";
            this.txtCepClinica.Size = new System.Drawing.Size(146, 33);
            this.txtCepClinica.TabIndex = 16;
            // 
            // txtEmailClinica
            // 
            this.txtEmailClinica.Location = new System.Drawing.Point(117, 276);
            this.txtEmailClinica.Name = "txtEmailClinica";
            this.txtEmailClinica.Size = new System.Drawing.Size(618, 33);
            this.txtEmailClinica.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(42, 276);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 26);
            this.label9.TabIndex = 14;
            this.label9.Text = "Email:";
            // 
            // cbxEstadoClinica
            // 
            this.cbxEstadoClinica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxEstadoClinica.FormattingEnabled = true;
            this.cbxEstadoClinica.Location = new System.Drawing.Point(117, 221);
            this.cbxEstadoClinica.Name = "cbxEstadoClinica";
            this.cbxEstadoClinica.Size = new System.Drawing.Size(146, 34);
            this.cbxEstadoClinica.TabIndex = 11;
            this.cbxEstadoClinica.Click += new System.EventHandler(this.cbxEstadoClinica_Click);
            // 
            // txtCidadeClinica
            // 
            this.txtCidadeClinica.Location = new System.Drawing.Point(349, 222);
            this.txtCidadeClinica.Name = "txtCidadeClinica";
            this.txtCidadeClinica.Size = new System.Drawing.Size(386, 33);
            this.txtCidadeClinica.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(269, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 26);
            this.label8.TabIndex = 12;
            this.label8.Text = "Cidade:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(42, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 26);
            this.label7.TabIndex = 10;
            this.label7.Text = "Estado:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(534, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Cep:";
            // 
            // txtNumeroClinica
            // 
            this.txtNumeroClinica.Location = new System.Drawing.Point(627, 116);
            this.txtNumeroClinica.Name = "txtNumeroClinica";
            this.txtNumeroClinica.Size = new System.Drawing.Size(108, 33);
            this.txtNumeroClinica.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(534, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 26);
            this.label4.TabIndex = 4;
            this.label4.Text = "Número:";
            // 
            // txtEnderecoClinica
            // 
            this.txtEnderecoClinica.Location = new System.Drawing.Point(142, 109);
            this.txtEnderecoClinica.Name = "txtEnderecoClinica";
            this.txtEnderecoClinica.Size = new System.Drawing.Size(375, 33);
            this.txtEnderecoClinica.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 26);
            this.label6.TabIndex = 6;
            this.label6.Text = "Bairro:";
            // 
            // txtBairroClinica
            // 
            this.txtBairroClinica.Location = new System.Drawing.Point(117, 167);
            this.txtBairroClinica.Name = "txtBairroClinica";
            this.txtBairroClinica.Size = new System.Drawing.Size(400, 33);
            this.txtBairroClinica.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Endereço:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome:";
            // 
            // txtNomeClinica
            // 
            this.txtNomeClinica.Location = new System.Drawing.Point(117, 54);
            this.txtNomeClinica.Name = "txtNomeClinica";
            this.txtNomeClinica.Size = new System.Drawing.Size(618, 33);
            this.txtNomeClinica.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(946, -7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.toolTip1.SetToolTip(this.label1, "Fechar");
            this.label1.Click += new System.EventHandler(this.lblFecharNovaClinica_Click);
            // 
            // FormNovaClinica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(978, 535);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(207, 92);
            this.Name = "FormNovaClinica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormNovaClinica";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnFinalizarClinica;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBairroClinica;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomeClinica;
        private System.Windows.Forms.TextBox txtEmailClinica;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbxEstadoClinica;
        private System.Windows.Forms.TextBox txtCidadeClinica;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNumeroClinica;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEnderecoClinica;
        private System.Windows.Forms.MaskedTextBox txtCepClinica;
    }
}