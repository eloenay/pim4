﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;
using System.Reflection;

namespace ProjetoIntegrador4.Formularios.Clinicas
{
    public partial class FormNovaClinica : Form
    {
        public FormNovaClinica()
        {
            InitializeComponent();
        }

        private void lblFecharNovaClinica_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnFinalizarClinica_Click(object sender, EventArgs e)
        {
            try
            {
                Clinica clinica = new Clinica();
                if (txtNomeClinica.Text == String.Empty || txtEnderecoClinica.Text == String.Empty || txtNumeroClinica.Text == String.Empty || txtEmailClinica.Text == String.Empty || txtBairroClinica.Text == String.Empty)
                {
                    MessageBox.Show("Não pode conter campos vazios");
                }
                else
                {
                    clinica.Nome = txtNomeClinica.Text;
                    clinica.Endereco = txtEnderecoClinica.Text;
                    clinica.Numero = Convert.ToInt32(txtNumeroClinica.Text);
                    clinica.Bairro = txtBairroClinica.Text;
                    clinica.Cep = txtCepClinica.Text;
                    clinica.Estado = (Estado)cbxEstadoClinica.SelectedValue;
                    clinica.Cidade = txtCidadeClinica.Text;
                    clinica.Email = txtEmailClinica.Text;

                    if (txtCepClinica.Text.Length < 9)
                    {
                        MessageBox.Show("Preencha corretamente o cep", "Informação");
                    }
                    else 
                    {
                        if (txtEmailClinica.Text.Contains("@"))
                        {
                            clinica.Salvar(clinica);
                            MessageBox.Show("Clinica salva com sucesso!!!");
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Tipo incorreto de email", "Informação");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);             
            }
        }

        #region Preencher combobox

        public static string ObterDescricao(Enum valor)
        {
            FieldInfo fieldInfo = valor.GetType().GetField(valor.ToString());
            DescriptionAttribute[] atributos =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return atributos.Length > 0 ? atributos[0].Description ?? "Nulo" : valor.ToString();
        }

        public static IList Listar(Type tipo)
        {
            ArrayList lista = new ArrayList();
            if (tipo != null)
            {
                Array enumValores = Enum.GetValues(tipo);
                foreach (Enum valor in enumValores)
                {
                    lista.Add(new KeyValuePair<Enum, string>(valor, ObterDescricao(valor)));
                }
            }

            return lista;
        }

        private void cbxEstadoClinica_Click(object sender, EventArgs e)
        {
            cbxEstadoClinica.DataSource = Listar(typeof(Estado));
            cbxEstadoClinica.DisplayMember = "Value";
            cbxEstadoClinica.ValueMember = "Key";
        }
        #endregion
    }
}
