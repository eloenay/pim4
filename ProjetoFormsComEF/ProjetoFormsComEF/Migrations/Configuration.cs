using ProjetoIntegrador4.Classes;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjetoIntegrador4.Classes.ConexaoBd>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ProjetoIntegrador4.Classes.ConexaoBd context)
        {
            context.Usuarios.AddOrUpdate(u => u.UsuarioId,
                new Usuario()
                {
                    UsuarioId = 1,
                    Nome = "Usuario Master",
                    Login = "admin",
                    Senha = "123456",
                    ConfirmarSenha = "123456",
                    TipoUsuario = TipoUsuario.Administrador
                },
                new Usuario()
                {
                    UsuarioId = 2,
                    Nome = "Eloenay Pereira",
                    Login = "eloenay",
                    Senha = "123456",
                    ConfirmarSenha = "123456",
                    TipoUsuario = TipoUsuario.Administrador
                }
            );

            context.Clinicas.AddOrUpdate(c => c.ClinicaId,
                new Clinica()
                {
                    ClinicaId = 1,
                    Nome = "PsicoClinic",
                    Bairro = "S�o Francisco",
                    Cep = "76820-000",
                    Cidade = "Porto Velho",
                    Email = "psicoClinic@gmail.com",
                    Endereco = "Rua Tambaqui",
                    Estado = Estado.RO,
                    UsuarioId = null,
                    Numero = 8765
                }
            );

            context.Pacientes.AddOrUpdate(c => c.PacienteId,
                new Paciente()
                {
                    PacienteId = 1,
                    Nome = "Maria Felix",
                    DataNascimento = Convert.ToDateTime("12/05/1980"),
                    Email = "mariafelix@gmail.com",
                    Sexo = Sexo.Femenino,
                    UsuarioId = null,
                }
            );

            context.Salas.AddOrUpdate(c => c.SalaId,
                new Sala()
                {
                    SalaId = 1,
                    Descricao = "SALA 1",
                    ClinicaId = 1
                },
                new Sala()
                {
                    SalaId = 2,
                    Descricao = "SALA 2",
                    ClinicaId = 1
                },
                new Sala()
                {
                    SalaId = 3,
                    Descricao = "SALA 3",
                    ClinicaId = 1
                }
            );

            context.TipoTerapias.AddOrUpdate(c => c.TipoTerapiaId,
                new TipoTerapia()
                {
                    TipoTerapiaId = 1,
                    Descricao = "Psican�lise"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 2,
                    Descricao = "Jungiana"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 3,
                    Descricao = "Lacaniana"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 4,
                    Descricao = "Cognitivo-Construtivista"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 5,
                    Descricao = "Anal�tico-Comportamental"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 6,
                    Descricao = "Cognitivo-Comportamental"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 7,
                    Descricao = "Cognitivo-Comportamental"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 8,
                    Descricao = "Cognitivo-Comportamental"
                },
                new TipoTerapia()
                {
                    TipoTerapiaId = 9,
                    Descricao = "EMDR"
                }
            );

            context.Horarios.AddOrUpdate(c => c.HorarioId,
                new Horario()
                {
                    HorarioId = 1,
                    Descricao = "8:00"
                },
                new Horario()
                {
                    HorarioId = 2,
                    Descricao = "9:00"
                },
                new Horario()
                {
                    HorarioId = 3,
                    Descricao = "10:00"
                },
                new Horario()
                {
                    HorarioId = 4,
                    Descricao = "11:00"
                },
                new Horario()
                {
                    HorarioId = 5,
                    Descricao = "14:00"
                },
                new Horario()
                {
                    HorarioId = 6,
                    Descricao = "15:00"
                },
                new Horario()
                {
                    HorarioId = 7,
                    Descricao = "16:00"
                },
                new Horario()
                {
                    HorarioId = 8,
                    Descricao = "17:00"
                }
            );

            context.Psicologos.AddOrUpdate(c => c.PsicologoId,
                new Psicologo()
                {
                    PsicologoId = 1,
                    Cpf = "111.111.111-11",
                    DataNascimento = Convert.ToDateTime("12/05/1949"),
                    Nome = "Ozama Binladem",
                    NumeroCrp = 11111,
                    RegiaoCrp = Regiao.Regiao_MS,
                    SalaId = 1,
                    UsuarioId = null
                },
                new Psicologo()
                {
                    PsicologoId = 2,
                    Cpf = "222.222.222-22",
                    DataNascimento = Convert.ToDateTime("12/05/1949"),
                    Nome = "Fernandinho Beiramar",
                    NumeroCrp = 23212,
                    RegiaoCrp = Regiao.Regiao_RS,
                    SalaId = 2,
                    UsuarioId = null
                },
                new Psicologo()
                {
                    PsicologoId = 3,
                    Cpf = "333.333.333-33",
                    DataNascimento = Convert.ToDateTime("12/05/1949"),
                    Nome = "Adolf Hitler",
                    NumeroCrp = 12145,
                    RegiaoCrp = Regiao.Regiao_AL,
                    SalaId = 3,
                    UsuarioId = null
                }
            );
        }
    }
}
