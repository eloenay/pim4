using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using ProjetoIntegrador4.DAO;

namespace ProjetoIntegrador4.Classes
{
    [Table("Salas")]
    public class Sala
    {
        public int SalaId { get; set; }
        public string Descricao { get; set; }
        public int? ClinicaId { get; set; }
        public Clinica Clinica { get; set; }

        public void Salvar(Sala sala)
        {
            DaoSala daoSala = new DaoSala();
            daoSala.Save(sala);
        }

        public Sala VerificarSala(Sala sala)
        {
            DaoSala daoSala = new DaoSala();
            return daoSala.VerificarSala(sala);
        }
    }
}
