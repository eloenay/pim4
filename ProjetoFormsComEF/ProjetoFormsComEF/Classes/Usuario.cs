using System.Data;
using System.Windows.Forms;
using ProjetoIntegrador4.DAO;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Classes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Usuarios")]
    public class Usuario
    {
        public int UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public bool Ativo { get; set; } = false;
        public TipoUsuario TipoUsuario { get; set; }

        public void Salvar(Usuario usuario)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            daoUsuario.Save(usuario);
        }

        public void Deletar(Usuario id)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            daoUsuario.Deletar(id);
        }

        public Usuario BuscarPorId(int id)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            return daoUsuario.BuscarPorId(id);
        }

        public Usuario BuscarLoginSenha(Usuario usuario)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            return daoUsuario.BuscarLoginSenha(usuario);
        }

        public DataTable BuscarTodos(string nome)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            IEnumerable<Usuario> usuarios = daoUsuario.BuscarPorNome(nome);
            DataTable dt = new DataTable();

            if (usuarios != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Nome", typeof(string));
                dt.Columns.Add("Login", typeof(string));
                dt.Columns.Add("Senha", typeof(string));
                dt.Columns.Add("Senha Confirmada", typeof(string));
                dt.Columns.Add("Tipo de Usuario", typeof(string));

                foreach (var u in usuarios)
                {
                    dt.Rows.Add(u.UsuarioId, u.Nome, u.Login, u.Senha, u.ConfirmarSenha, u.TipoUsuario);
                }

            }
            else
            {
                dt = null;
            }

            return dt;
        }

        public DataTable BuscarPorNomes(string nome)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            IEnumerable<Usuario> usuarios = daoUsuario.BuscarPorNome(nome);
            DataTable dt = new DataTable();

            if (usuarios != null)
            {
                dt.Columns.Add("Nome", typeof(string));
                dt.Columns.Add("Login", typeof(string));
                dt.Columns.Add("Tipo de Usuario", typeof(string));

                foreach (var u in usuarios)
                {
                    dt.Rows.Add(u.Nome, u.Login, u.TipoUsuario);
                }

            }
            else
            {
                dt = null;
            }

            return dt;
        }

        public Usuario BuscarLoginExistente(Usuario registrado)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            return daoUsuario.BuscarLoginExistente(registrado);
        }
    }
}
