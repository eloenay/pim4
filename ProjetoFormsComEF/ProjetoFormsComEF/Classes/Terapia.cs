using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using ProjetoIntegrador4.DAO;
using System.Data;

namespace ProjetoIntegrador4.Classes
{
    [Table("Terapias")]
    public class Terapia
    {
        public int TerapiaId { get; set; }
        public DateTime DataTerapia { get; set; }
        public decimal Valor { get; set; }
        public int? HorarioId { get; set; }
        public Horario Horario { get; set; }

        public int? ConsultaId { get; set; }
        public Consulta Consulta { get; set; }

        public int? TipoTerapiaId { get; set; }
        public TipoTerapia TipoTerapia { get; set; }

        public int? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }


        public void Salvar(Terapia terapia)
        {
            DaoTerapia daoTerapia = new DaoTerapia();
            daoTerapia.Salvar(terapia);
        }

        public void Editar(Terapia terapia)
        {
            DaoTerapia daoTerapia = new DaoTerapia();
            daoTerapia.Editar(terapia);
        }

        public Terapia BuscarPorId(int? id)
        {
            DaoTerapia daoTerapia = new DaoTerapia();
            return daoTerapia.BuscarPorId(id);
        }

        public DataTable BuscarPorPaciente(int consulta)
        {
            DaoTerapia daoTerapia = new DaoTerapia();
            IEnumerable<Terapia> terapias = daoTerapia.BuscarPorPaciente(consulta);
            DataTable dt = new DataTable();
            if (terapias != null)
            {
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Hr.Terapia", typeof(string));
                dt.Columns.Add("Dt.Terapia", typeof(DateTime));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Tipo Terapia", typeof(string));

                foreach (var i in terapias)
                {
                    dt.Rows.Add(i.Consulta.Paciente.Nome, i.Horario.Descricao, i.DataTerapia, i.Valor, i.TipoTerapia.Descricao);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

        public DataTable BuscarTodos(string nome)
        {
            DaoTerapia daoTerapia = new DaoTerapia();
            IEnumerable<Terapia> terapias = daoTerapia.BuscarTodos(nome);
            DataTable dt = new DataTable();
            if (terapias != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Hr.Terapia", typeof(string));
                dt.Columns.Add("Dt.Terapia", typeof(DateTime));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Tipo Terapia", typeof(string));

                foreach (var i in terapias)
                {
                    dt.Rows.Add(i.TerapiaId, i.Consulta.Paciente.Nome, i.Horario.Descricao, i.DataTerapia, i.Valor, i.TipoTerapia.Descricao);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }
    }
}
