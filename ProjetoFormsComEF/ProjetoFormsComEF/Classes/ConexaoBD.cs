using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using ProjetoIntegrador4.EntityConfig;

namespace ProjetoIntegrador4.Classes
{
    public  class ConexaoBd : DbContext
    {
        public ConexaoBd()
            : base("name=BancoPsicologo")
        {
        }

        public virtual DbSet<Consulta> Consultas { get; set; }
        public virtual DbSet<Clinica> Clinicas { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Paciente> Pacientes { get; set; }
        public virtual DbSet<Psicologo> Psicologos { get; set; }
        public virtual DbSet<Sala> Salas { get; set; }
        public virtual DbSet<Terapia> Terapias { get; set; }
        public virtual DbSet<TipoTerapia> TipoTerapias { get; set; }
        public virtual DbSet<Horario> Horarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>()
                .Configure(x => x.HasColumnType("varchar"));

            modelBuilder.Configurations.Add(new PacienteConfiguration());
            modelBuilder.Configurations.Add(new PsicologoConfiguration());
            modelBuilder.Configurations.Add(new ConsultaConfiguration());
            modelBuilder.Configurations.Add(new ClinicaConfiguration());
            modelBuilder.Configurations.Add(new UsuarioConfiguration());
            modelBuilder.Configurations.Add(new SalaConfiguration());
            modelBuilder.Configurations.Add(new TerapiaConfiguration());
            modelBuilder.Configurations.Add(new HorarioConfiguration());
            modelBuilder.Configurations.Add(new TipoTerapiaConfiguration());


            base.OnModelCreating(modelBuilder);
        }

    }
}
