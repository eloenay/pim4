﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoIntegrador4.DAO;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Classes
{
    [Table("Clinicas")]
   public class Clinica
    {
        public int ClinicaId { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public int Numero { get; set; }
        public string Cep { get; set; }
        public string Bairro { get; set; }
        public Estado Estado { get; set; }
        public string Cidade { get; set; }
        public string Email { get; set; }
        public int? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        public void Salvar(Clinica clinica)
        {
            DaoClinica daoClinica = new DaoClinica();
            daoClinica.Save(clinica);
        }

        public void Deletar(Clinica clinica)
        {
            DaoClinica daoClinica = new DaoClinica();
            daoClinica.Deletar(clinica);
        }

        public Clinica BuscarPorId(int id)
        {
            DaoClinica daoClinica = new DaoClinica();
            return daoClinica.BuscarPorId(id);
        }

        public IEnumerable<Clinica> BuscarTodos()
        {
            DaoClinica daoClinica = new DaoClinica();
            return daoClinica.BuscarTodos();
        }

        public DataTable BuscarPorNome(string nome)
        {
            DaoClinica daoClinica = new DaoClinica();
            IEnumerable<Clinica> clinicas = daoClinica.BuscarPorNome(nome);
            DataTable dt = new DataTable();

            if (clinicas != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Nome", typeof(string));
                dt.Columns.Add("Endereco", typeof(string));
                dt.Columns.Add("Numero", typeof(string));
                dt.Columns.Add("Bairro", typeof(string));
                dt.Columns.Add("Cidade", typeof(string));
                dt.Columns.Add("Estado", typeof(string));

                foreach (var c in clinicas)
                {
                    dt.Rows.Add(c.ClinicaId, c.Nome, c.Endereco, c.Numero, c.Bairro, c.Cidade, c.Estado);
                }
            }
            else
            {
                dt = null;
            }

            return dt;
        }
    }
}
