using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Data;
using System.Runtime.Serialization;
using ProjetoIntegrador4.DAO;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Classes
{
    [Table("Pacientes")]
    public class Paciente
    {
        public int PacienteId { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public Sexo Sexo { get; set; }
        public string Email { get; set; }
        public int? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        public void Salvar(Paciente paciente)
        {
            DaoPaciente daoPaciente = new DaoPaciente();
            daoPaciente.Save(paciente);
        }

        public void Deletar(Paciente paciente)
        {
            DaoPaciente daoPaciente = new DaoPaciente();
            daoPaciente.Deletar(paciente);
        }

        public Paciente BuscarPorId(int id)
        {
            Paciente paciente = new Paciente();
            DaoPaciente daoPaciente = new DaoPaciente();
            paciente = daoPaciente.BuscarPorId(id);
            return paciente;
        }

        public DataTable BuscarPorNome(string nome)
        {
            DaoPaciente daoPaciente = new DaoPaciente();
            ICollection<Paciente> paciente = daoPaciente.BuscarPorNome(nome);
            DataTable dt = new DataTable();
            if (paciente != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Nome", typeof(string));
                dt.Columns.Add("Dt.Nascimento", typeof(DateTime));
                dt.Columns.Add("Sexo", typeof(string));
                dt.Columns.Add("E-mail", typeof(string));


                foreach (var i in paciente)
                {
                    dt.Rows.Add(i.PacienteId, i.Nome, i.DataNascimento, i.Sexo, i.Email);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }
    }
}
