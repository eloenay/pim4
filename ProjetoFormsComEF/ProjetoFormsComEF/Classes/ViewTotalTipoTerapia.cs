namespace ProjetoIntegrador4.Classes
{
    using ProjetoIntegrador4.DAO;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Entity.Spatial;

    [Table("ViewTotalTipoTerapia")]
    public partial class ViewTotalTipoTerapia
    {
        [Key]
        [StringLength(50)]
        public string Descricao { get; set; }

        public int? QntTerapia { get; set; }

        public DataTable BuscarTodos()
        {
            DaoViewTotalTipoTerapia daoViewTotalTipoTerapia = new DaoViewTotalTipoTerapia();
            IEnumerable<ViewTotalTipoTerapia> viewTotalTipoTerapia = daoViewTotalTipoTerapia.Buscartodos();
            DataTable dt = new DataTable();
            if (viewTotalTipoTerapia != null)
            {
                dt.Columns.Add("Tipo de Terapia", typeof(string));
                dt.Columns.Add("Terapias Mais Solicitadas", typeof(string));

                foreach (var i in viewTotalTipoTerapia)
                {
                    dt.Rows.Add(i.Descricao, i.QntTerapia);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }
    }
}
