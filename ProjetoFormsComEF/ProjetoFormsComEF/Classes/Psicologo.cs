using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity.Spatial;
using System.Runtime.Serialization;
using ProjetoIntegrador4.DAO;
using ProjetoIntegrador4.Enuns;

namespace ProjetoIntegrador4.Classes
{
    [Table("Psicologos")]
    public class Psicologo
    {
        public int PsicologoId { get; set; }
        public string Nome { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Cpf { get; set; }
        public int NumeroCrp { get; set; }
        public Regiao RegiaoCrp { get; set; }

        public int? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        public int? SalaId{ get; set; }
        public Sala Sala { get; set; }

        #region Crud

        public void Salvar(Psicologo psicologo)
        {
            DaoPsicologo daoPsicologo = new DaoPsicologo();
            daoPsicologo.Save(psicologo);
        }

        public void Deletar(Psicologo psicologo)
        {
            DaoPsicologo daoPsicologo = new DaoPsicologo();
            daoPsicologo.Deletar(psicologo);
        }

        public string BuscarSala()
        {
            DaoPsicologo daoPsicologo = new DaoPsicologo();
            Sala sala = new Sala();
            if (SalaId == Sala.SalaId)
            {
                daoPsicologo.BuscarSala(sala);
                return sala.Descricao;
            }
            else
            {
                return null;
            }
        }

        #endregion

        public DataTable BuscarPorNome(string nome)
        {
            DaoPsicologo daoPsicologo = new DaoPsicologo();
            IEnumerable<Psicologo> psicologo = daoPsicologo.BuscarPorNome(nome);
            DataTable dt = new DataTable();
            if (psicologo != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Nome", typeof(string));
                dt.Columns.Add("Dt.Nascimento", typeof(DateTime));
                dt.Columns.Add("Cpf", typeof(string));
                dt.Columns.Add("Sala", typeof(string));
                dt.Columns.Add("N�mero Crp", typeof(string));
                dt.Columns.Add("Regi�o", typeof(string));

                foreach (var i in psicologo)
                {
                    dt.Rows.Add(i.PsicologoId, i.Nome, i.DataNascimento, i.Cpf, i.Sala.Descricao, i.NumeroCrp, i.RegiaoCrp);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

        public Psicologo BuscarPorId(int id)
        {
            DaoPsicologo daoPsicologo = new DaoPsicologo();
            return daoPsicologo.BuscarPorId(id);
        }

        public Psicologo VerificarCpf(Psicologo psicologo)
        {
            DaoPsicologo daoPsicologo = new DaoPsicologo();
            return daoPsicologo.VerificarCpf(psicologo);
        }
    }
}
