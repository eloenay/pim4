using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Windows.Forms;
using ProjetoIntegrador4.DAO;
using System.Data;

namespace ProjetoIntegrador4.Classes
{
    [Table("Consultas")]
    public class Consulta
    {

        public int ConsultaId { get; set; }

        public decimal Valor { get; set; }

        public TimeSpan HorarioConsulta { get; set; }

        public DateTime DataConsulta { get; set; }

        public int PacienteId { get; set; }
        public Paciente Paciente { get; set; }

        public int PsicologoId { get; set; }
        public Psicologo Psicologo { get; set; }

        public int? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        public void Salvar(Consulta consulta)
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            daoConsulta.Salvar(consulta);
        }

        public void Editar(Consulta consulta)
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            daoConsulta.Editar(consulta);
        }

        public Consulta BuscarPorId(int? id)
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            return daoConsulta.BuscarPorId(id);
        }

        public int ConsultaParaHoje()
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            return daoConsulta.ConsultaParaHoje();
        }

        public int ConsultaParaMes()
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            return daoConsulta.ConsultaParaMes();
        }

        public int BuscarPorPacienteConsuta()
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            return daoConsulta.BuscarPacientesComMaisConsulta();
        }

        public DataTable BuscarConsultaParaHoje()
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            IEnumerable<Consulta> consulta = daoConsulta.BuscarConsultaParaHoje();
            DataTable dt = new DataTable();
            if (consulta != null)
            {
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Dt.Consulta", typeof(DateTime));
                dt.Columns.Add("Hr.Consulta", typeof(TimeSpan));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Psicologo", typeof(string));

                foreach (var i in consulta)
                {
                    dt.Rows.Add(i.Paciente.Nome, i.DataConsulta, i.HorarioConsulta, i.Valor, i.Psicologo.Nome);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

        public DataTable BuscarConsultaParaMes()
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            IEnumerable<Consulta> consulta = daoConsulta.BuscarConsultaParaMes();
            DataTable dt = new DataTable();
            if (consulta != null)
            {
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Dt.Consulta", typeof(DateTime));
                dt.Columns.Add("Hr.Consulta", typeof(TimeSpan));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Psicologo", typeof(string));

                foreach (var i in consulta)
                {
                    dt.Rows.Add(i.Paciente.Nome, i.DataConsulta, i.HorarioConsulta, i.Valor, i.Psicologo.Nome);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

        public DataTable BuscarTodos()
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            IEnumerable<Consulta> consulta = daoConsulta.Buscartodos();
            DataTable dt = new DataTable();
            if (consulta != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Dt.Consulta", typeof(DateTime));
                dt.Columns.Add("Hr.Consulta", typeof(TimeSpan));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Psicologo", typeof(string));

                foreach (var i in consulta)
                {
                    dt.Rows.Add(i.ConsultaId, i.Paciente.Nome, i.DataConsulta, i.HorarioConsulta, i.Valor, i.Psicologo.Nome);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

        public DataTable BuscarPorPsicologo(string nome)
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            IEnumerable<Consulta> consulta = daoConsulta.BuscarPorPsicologo(nome);
            DataTable dt = new DataTable();
            if (consulta != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Dt.Consulta", typeof(DateTime));
                dt.Columns.Add("Hr.Consulta", typeof(TimeSpan));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Psicologo", typeof(string));

                foreach (var i in consulta)
                {
                    dt.Rows.Add(i.ConsultaId, i.Paciente.Nome, i.DataConsulta, i.HorarioConsulta, i.Valor, i.Psicologo.Nome);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

        public DataTable BuscarPorPaciente(string nome)
        {
            DaoConsulta daoConsulta = new DaoConsulta();
            IEnumerable<Consulta> consulta = daoConsulta.BuscarPorPaciente(nome);
            DataTable dt = new DataTable();
            if (consulta != null)
            {
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Dt.Consulta", typeof(DateTime));
                dt.Columns.Add("Hr.Consulta", typeof(TimeSpan));
                dt.Columns.Add("Valor", typeof(decimal));
                dt.Columns.Add("Psicologo", typeof(string));

                foreach (var i in consulta)
                {
                    dt.Rows.Add(i.ConsultaId, i.Paciente.Nome, i.DataConsulta, i.HorarioConsulta, i.Valor, i.Psicologo.Nome);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }

    }
}
