﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoIntegrador4.Classes
{
    [Table("Horarios")]
    public class Horario
    {
        public int HorarioId { get; set; }
        public string Descricao { get; set; }
    }
}
