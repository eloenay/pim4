using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using ProjetoIntegrador4.DAO;

namespace ProjetoIntegrador4.Classes
{
    [Table("TipoTerapias")]
    public class TipoTerapia
    {
        public int TipoTerapiaId { get; set; }
        public string Descricao { get; set; }

        public void Salvar(TipoTerapia tipoTerapia)
        {
            DaoTipoTerapia daoTipoTerapia = new DaoTipoTerapia();
            daoTipoTerapia.Save(tipoTerapia);
        }

    }
}
