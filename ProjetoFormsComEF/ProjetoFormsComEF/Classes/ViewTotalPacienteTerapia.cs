namespace ProjetoIntegrador4.Classes
{
    using ProjetoIntegrador4.DAO;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Entity.Spatial;

    [Table("ViewTotalPacienteTerapia")]
    public partial class ViewTotalPacienteTerapia
    {
        [Key]
        [StringLength(100)]
        public string Nome { get; set; }

        public int? Consultas { get; set; }

        public DataTable BuscarTodos()
        {
            DaoViewTotalPacienteTerapia daoViewConsultaPaciente = new DaoViewTotalPacienteTerapia();
            IEnumerable<ViewTotalPacienteTerapia> viewConsultaPaciente = daoViewConsultaPaciente.Buscartodos();
            DataTable dt = new DataTable();
            if (viewConsultaPaciente != null)
            {
                dt.Columns.Add("Paciente", typeof(string));
                dt.Columns.Add("Total de Terapias Realizadas", typeof(string));

                foreach (var i in viewConsultaPaciente)
                {
                    dt.Rows.Add(i.Nome, i.Consultas);
                }

            }
            else
            {
                dt = null;
            }
            return dt;
        }
    }
}
