namespace ProjetoIntegrador4.Classes
{
    using System.Data.Entity;

    public partial class ViewDB : DbContext
    {
        public ViewDB()
            : base("name=BancoPsicologo")
        {
        }

        public virtual DbSet<ViewTotalPacienteTerapia> ViewTotalPacienteTerapia { get; set; }
        public virtual DbSet<ViewTotalTipoTerapia> ViewTotalTipoTerapia { get; set; }

    }
}
